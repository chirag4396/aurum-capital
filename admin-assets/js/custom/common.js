$.ajaxSetup({
	headers: {
		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	}
});

function select(id,type) {
	if($('#'+id).val() == -1){
		Msg.inputText(id,type);
		return true;
	}else{
		Msg.removeInputText(id);		
		return false;
	}
}

function emptyBox(id,type) {
	if($('#'+id).val() == ''){
		Msg.inputText(id,type);
		return true;
	}else{
		Msg.removeInputText(id);
		return false;
	}
}

function mobile(id,type) {	
	var pattern = /^\d{10}$/;
	if (!pattern.test($('#'+id).val())){
		Msg.inputText(id,type);
		return true;
	}else{
		Msg.removeInputText(id);
		return false;
	}
}
function alphaSpace(id,type) {
	var pattern = /^[a-zA-Z\s]+$/;
	
	if (!pattern.test($('#'+id).val())){
		Msg.inputText(id,type);
		return true;
	}
	Msg.removeInputText(id);
	return false;
}

function email(id ,type) {
	var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
	if (!filter.test($('#'+id).val())) {
		Msg.inputText(id,type);
		return true;
	}
	Msg.removeInputText(id);    	
	return false;
}
function same(id, type, other) {
	// console.log(id,type,other);
	var val = $('#'+id).val();	
	var pass = $('#'+other).val();	
	if(val !== pass){
		Msg.inputText(id,'same');
		return true;		
	}
	Msg.removeInputText(id);    	
	return false;		
}
function password(id,type) {
	// console.log(id,type);
	var val = $('#'+id).val();		
	if (val.length < 8) {
		Msg.inputText(id,'minimum');
		return true;		
	} else if (val.length > 50) {			
		Msg.inputText(id,'minimum');
		return true;
	} else if (val.search(/\d/) == -1) {			
		Msg.inputText(id,'oneDigit');
		return true;
	} else if (val.search(/[a-z]/) == -1) {
		Msg.inputText(id,'oneLower');
		return true;
	} else if (val.search(/[A-Z]/) == -1) {
		Msg.inputText(id,'oneUpper');
		return true;
	} else if (val.search(/[\!\@\#\$\%\^\&\*\(\)\_\+\.\,\;\:]/) == -1) {
		Msg.inputText(id,'oneSpecial');
		return true;
	}
	Msg.removeInputText(id);    	
	return false;

}
function validate(type) {

	var chk = [];
	$.each(type,function(k,v){
		switch(k){
			case 'select' :
			chk.push(select(v,k));
			break;
			case 'empty' :
			chk.push(emptyBox(v,k));			
			break;
			case 'mobile' :
			chk.push(mobile(v,k));			
			break;
			case 'email' :
			chk.push(email(v,k));			
			break;
			case 'alphaSpace' :
			chk.push(alphaSpace(v,k));			
			break;
			case 'password' :
			chk.push(password(v,k));			
			break;
			case 'same' :
			chk.push(same(v.id,k,v.other));			
			break;
		}
	});
	return chk;
}

function getSelectValues(main, sub, ph){	
	$('#'+main).on({
		'change' : function(){
			var p = ph+'/'+this.value;
			$.ajax({
				url : p,
				method : 'get',
				dataType : 'json',
				success : function (data) {
					var options = [];
					options.push('<option value = "-1">--Select--</option>');
					if(data.length > 0){
						$.each(data,function(k,v){
							options.push('<option value = "'+v.id+'">'+v.name+'</option>');
						});
						$('#'+sub).html(options).prop('disabled',false);
					}else{
						$('#'+sub).prop('disabled',true);
					}
				}			
			});
		}
	});
}

var handleDataTableButtons = function(id) {
	var i = '#'+id+'Table';
	if ($(i).length) {
		$(i).DataTable({
			dom: "Bfrtip",
			buttons: [
			{
				extend: "copy",
				className: "btn-sm"
			},
			{
				extend: "csv",
				className: "btn-sm"
			},
			{
				extend: "excel",
				className: "btn-sm"
			},
			{
				extend: "pdfHtml5",
				className: "btn-sm"
			},
			{
				extend: "print",
				className: "btn-sm"
			},
			],
			responsive: true
		});
	}
};

Table = function() {    
	return {
		init: function(id) {
			handleDataTableButtons(id);
		}
	};
}();

CRUD = function(){

	var method = ['post', 'get', 'update', 'delete'];

	return{		
		sendFormData : function(options){

			var id = options.id;

			var fd = new FormData($('#'+options.id)[0]);

			if(options.extraVariables){
				$.each(options.extraVariables(),function(k,v){
					fd.append(k,v);
				});
			}

			switch(options.type){
				case 0:
				ur = options.url;
				var t = (options.onLoad) ? options.onLoad : 'add';				
				break;
				case 2:
				ur = options.url+'/'+fd.get('sid');
				fd.append('_method','patch');
				var t = (options.onLoad) ? options.onLoad : 'update';				
				break;
			}
			
			Msg.txt(t,id);

			$.ajax({
				url : ur,
				data : fd,
				type : method[0],
				processData : false,
				contentType : false,
				success : function(data){	
				// console.log(data);				
				if(options.proData){
					options.proData(data);
				}

				Msg.txt(data.msg,id);

			},error : function(xhr,data){				
				if(options.proError){
					options.proError(data);
				}
				
				Msg.txt(data,id);
			}
		});
		},
		formSubmission: function(options) {
			// console.log(options);
			$('#'+options.id).on({
				'submit' : function(e){
					e.preventDefault();
					if(!$.isEmptyObject(options.validate)){
						if($.inArray(true, validate(options.validate)) < 0){
							if(options.noAjax){
								$('#'+options.id)[0].submit();
							}else{
								CRUD.sendFormData(options);
							}
						}
					}else{
						CRUD.sendFormData(options);
					}
				}
			});
		},
		edit : function(options){			
			var extra = '?';
			if(options.extraVariables){
				$.each(options.extraVariables,function(k,v){
					extra+=k+'='+v;
				});
			}
			var editForm = 'edit'+options.id+'Form',			
			u = options.getUrl+'/'+options.fetchId+'/edit'+extra;	


			$('#'+options.id+'Form').attr('id',editForm);
			var t = (options.id == 'enquiry') ? 'View ' : 'Edit '; 
			$('#'+options.id+'ModalLabel').html(t+options.id);			
			$('#'+editForm+' button[type="submit"]').html('UPDATE');

// submit
			$.ajax({
				url : u,
				type : method[1],
				success : function(data){					
					$.each($('#'+editForm)[0],function(k,v){
						if(v.type != 'file'){
							va = (v.name == 'sid') ? 'id' : v.name;							
							if(v.type == 'radio'){
								// console.log(data[va]);
								if($(v).val() == data[va]){
									$(v).prop('checked',true);
								}
							}else{								
								$(v).val(data[va]);
							}
						}
					});
					
					if(options.proData){
						options.proData(data);
					}
					
					$('#'+options.id+'Modal').modal('toggle');

					CRUD.formSubmission({
						url : options.getUrl, 
						type : 2,
						id : editForm,
						proData : function(data){							
							if(data.msg == 'successU'){
								Msg.timer(function(){
									$('#'+options.id+'Modal').modal('toggle');
								});
							}
						}			
					});	
				}
			});
		},
		viewSingle : function(id, getUrl, fetchId, additional){			
			var editForm = 'edit'+id+'Form',			
			u = getUrl+'/'+fetchId+'/edit';	

			// $('#'+id+'Form').attr('id',editForm);
			

			$.ajax({
				url : u,
				type : method[1],
				success : function(data){
					additional(data);
					// $.each($('#'+editForm)[0],function(k,v){
					// 	if(v.type != 'file'){

					// 		va = (v.name == 'sid') ? 'id' : v.name;

					// 		$(v).val(data[va]);
					// 	}
					// });
					// if($('#img')){
					// 	$('#img').attr('src','../'+data.img_thumb_path);

					// 	$('#'+editForm+' button[type="submit"]').attr('type','button').attr('onclick','$(\'#sendQuote\').removeClass(\'hidden\');').html('SEND QUOTE');
					// 	$('#enqProduct').val(data.pro_id);
					// 	$('#enqUser').val(data.enq_id);
					// }else{
					// 	$('#'+editForm+' button[type="submit"]').html('UPDATE');
					// }
					// $('#'+id+'Modal').modal('toggle');

					// CRUD.formSubmission({
					// 	url : getUrl, 
					// 	type : 2,
					// 	id : editForm,
					// 	proData : function(data){
					// 		console.log(data);
					// 		if(data.msg == 'successU'){
					// 			// $('#'+id+'-table').DataTable()
					// 			   // .draw('1');
					// 			// $('#'+id+'-table').DataTable().row().invalidate().render();;
					// 			// Table.init(id+'-table');
					// 			// table=$(id+'-table').dataTable();
					// 			// table.fnPageChange("first",1);
					// 			Msg.timer(function(){
					// 				$('#'+id+'Modal').modal('toggle');
					// 			});

					// 		}
					// 	}			
					// });	
				}
			});
		},
		add : function(id,storeUrl){
			var editForm = '#edit'+id+'Form';
			
			if($(editForm).length){
				$(editForm)[0].reset();
				$(editForm).attr('id',id+'Form');
			}
			$('#'+id+'ModalLabel').html('Add '+id);
			$('#'+id+'Form button[type="submit"]').html('ADD');
			
			$('#'+id+'Modal').modal('toggle');			

			
			CRUD.formSubmission({
				url : storeUrl, 
				type : 0,
				id : id+'Form'
			});		
		},
		delete : function(id,data,name){
			if($('#delName').length){
				$('#delName').html(name);
			}
			$('#'+id+'DModal').modal('toggle');
			$('#yes').attr('onclick','CRUD.confirmDelete("'+id+'",'+data+');');
		},
		confirmDelete : function(id,val){
			$.ajax({
				url : deleteU+'/'+val,
				type : method[3],
				success : function(data){					
					Msg.txt(data.msg,id+'DForm');
					Msg.timer(function(){
						$('#'+id+'DModal').modal('toggle');
					});
					$('#tr-'+val).remove();
				}
			}).error(function (d) {				
				Msg.txt('errorD',id+'DForm');
			});
		}
	}
}();
// CRUD.edit({id:'category', getUrl:'http://brownie-point/admin/category' ,fetchId:'5'});
// CRUD.edit('enquiry', 'http://brownie-point/admin/enquiry' ,'1');
// CRUD.viewSingle('order', 'http://brownie-point/admin/order' ,'1', additional);

Msg = function(){

	var msgDetail = {
		add : {
			msg : 'Adding, Please wait!!', class : 'warning'
		},
		check : {
			msg : 'Checking, Please wait!!', class : 'warning'
		},
		update : {
			msg : 'Updating, Please wait!!', class : 'warning'
		},
		success : {
			msg : 'Successfully Added!!', class : 'success'
		},
		successEnquiry : {
			msg : 'We got you Query, we\'ll get back to you soon!!', class : 'success'
		},
		successU : {
			msg : 'Successfully Updated!!', class : 'success'
		},
		successLogin : {
			msg : 'Successfully Logged in!!', class : 'success'
		},
		successRegister : {
			msg : 'Successfully Registered!!', class : 'success'
		},
		successSend : {
			msg : 'Quote Successfully sent!!', class : 'success'
		},
		error : {
			msg : 'Something went wrong, Please try again!!', class : 'danger'
		},
		errorLogin : {
			msg : 'Invalid credentials, Please try again!!', class : 'danger'
		},
		errorD : {
			msg : 'Something went wrong, try again or these data must be linked with other child data!!', class : 'danger'
		},
		exist : {
			msg : 'Record Exist, Please try something new!!', class : 'warning'		
		},
		same : {
			msg : 'Same as Previous, Please try something new!!', class : 'warning'
		},
		delete : {
			msg : 'Record Deleted Successfully!!', class : 'success'		
		},
        notUser : {
            msg : 'Email or Password is incorrect, please try again!!', class : 'error'
        },
		input : {
			select : 'Please select option',
			empty : 'These field couldn\'t be Empty',
			mobile : 'Invalid Mobile Number',
			email : 'Invalid Email address',
			alphaSpace : 'Name must contain characters and space',
			minimum : 'Should be a length of 8',
			oneDigit : 'Should contain 1 digit',
			oneLower : 'Should contain 1 Lowercase letter',
			oneUpper : 'Should contain 1 Uppercase letter',
			oneSpecial : 'Must contain 1 Special character like @,$,#,etc...',
			same : 'Password d oesn\'t match',
		}
	};

	var m = 2500;
	return {
		timer : function(data){
			window.setTimeout(function(){
				data();
			},m);
		},
		txt : function(type,id){
			var id = '#'+id,
			modal = id.replace('Form','')+'Modal';

			if($('#formAlert').length){
				$('#formAlert').remove();
			}
			$(id).append('<div id = "formAlert" class = "text-center alert alert-'+msgDetail[type].class+' fade in">'+msgDetail[type].msg+'</div>');
			if(type == 'success' || type == 'successEnquiry' || type == 'successU'){
				this.timer(function(){
					$(id)[0].reset();
					if($(modal).length){
						$(modal).modal('toggle');
					}
					$('#formAlert').remove();				
				});			
			}
		},
		removeInputText : function(id) {
			$('#'+id).nextAll('span').remove();
		},
		inputText : function(id,type){
			this.removeInputText(id);
			$('#'+id).after('<span style = "color: #db0c06;">'+msgDetail.input[type]+'</span>');
		}
	};
}();


var height,width;
$(function() {
	$("#banner").on("change", function(){
		var files = !!this.files ? this.files : [];

		if (!files.length || !window.FileReader) return;

		if (/^image/.test( files[0].type)){
			var reader = new FileReader();
			reader.readAsDataURL(files[0]);            
			reader.onloadend = function(upImg){

				$("#imagePreview").css("background-image", "url("+this.result+")");
				urlValue = this.result;

				var image = new Image();
				image.src = upImg.target.result;

				image.onload = function() {
					height = this.height;
					width = this.width;					
				};
			}                
		}
	});
});