var height, width;
function multiSelect(id,link){var self=id;$(id).select2({tags:!0,placeholder:'Start typing slowly',allowClear:!0,createTag:function(params){var term=$.trim(params.term);if(term===''){return null}return{id:term,text:term,newTag:!0}}});$(id).on('select2:select',function(e){var data=e.params.data;var old=data.id;console.log(old);if(data.newTag){$.post(geturl(link),{title:data.text},function(data){$(self).find("option[value='"+old+"']").remove();var newOption=new Option(data.d.title,data.d.id,!0,!0);$(self).append(newOption).trigger('change')})}})}
function imageUpload(id) {

    $('#'+id).on("change", function(){
        var files = !!this.files ? this.files : [];

        if (!files.length || !window.FileReader) return;

        if (/^image/.test( files[0].type)){
            var reader = new FileReader();
            reader.readAsDataURL(files[0]);
            var sizeMB = files[0].size / (1024 * 1024);
            if(sizeMB > 2){
                $('#'+id).parent().parent().parent().find('button[type="submit"]').prop('disabled', true);
                $('#'+id).parent().append('<p style = "color: red;margin-top: 10px;">Please select image of lessthan 2 MBs.</p>');
            }else{
                $('#'+id).parent().find('p').remove();
                $('#'+id).parent().parent().parent().find('button[type="submit"]').prop('disabled', false);             
            }
            reader.onloadend = function(upImg){

                $("#"+id+"Preview").attr("src", this.result);
                urlValue = this.result;
                
                var image = new Image();
                image.src = upImg.target.result;

                image.onload = function() {
                    height = this.height;
                    width = this.width;                 
                };
            }                
        }
    });
}

function nos(e) {
    void 0 !== e.onselectstart ? e.onselectstart = function() {
        return !1
    } : void 0 !== e.style.MozUserSelect ? e.style.MozUserSelect = "none" : (e.onmousedown = function() {
        return !1
    }, e.style.cursor = "default")
}
$.ajaxSetup({
    headers: {
        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
    }
}),
    function(e) {
        e.fn.CRUD = function(t, s) {
            var n = this,
                r = e.extend({}, {
                    url: "",
                    type: "post",
                    method: ["post", "get", "update", "delete"],
                    finalUrl: t.url,
                    validation: !0
                }, t);
            return this.each(function() {
                switch (s) {
                    case "get":
                        e.ajax({
                            url: r.url,
                            type: r.method[1],
                            success: function(e) {
                                t.processResponse && t.processResponse(e)
                            },
                            error: function(e, t) {
                                r.processError ? r.processError(t) : n.MSG({
                                    type: "error"
                                })
                            }
                        });
                        break;
                    case "deleteForm":
                        e("#" + t.id + "DModal").modal("toggle"), e("#delName").html(t.name), e("#" + t.id + "DForm").CRUD({
                            url: t.url + "/" + t.pId,
                            type: 3,
                            processResponse: function(e) {
                                console.log(e)
                            }
                        });
                        break;
                    case "delete":
                        e.ajax({
                            url: r.url,
                            data: {
                                _method: "DELETE"
                            },
                            type: r.method[0],
                            success: function(e) {
                                t.processResponse && t.processResponse(e)
                            },
                            error: function(e, t) {
                                r.processError ? r.processError(t) : n.MSG({
                                    type: "error"
                                })
                            }
                        });
                        break;
                    case "edit":
                        ! function() {
                            var s = "#" + t.id + "Modal",
                                a = "#" + t.id + "Form",
                                o = "?";
                            t.extraVariables && e.each(t.extraVariables, function(e, t) {
                                o += e + "=" + t
                            });
                            var i = r.url + "/" + t.fetchId + "/edit" + o;
                            e(s).modal("toggle"), e(s + "Label").html("Edit " + t.id), e(a + ' button[type="submit"]').html("UPDATE"), e.ajax({
                                url: i,
                                type: r.method[1],
                                success: function(s) {
                                    var n = e("<input/>", {
                                        value: s.id,
                                        name: "id",
                                        type: "hidden",
                                        id: "id"
                                    });
                                    e("#id").remove(), e(a).append(n), s.description && e("#description").length && e("#description .ql-editor").html(s.description), e.each(e(a)[0].elements, function(t, n) {
                                        if ("radio" == n.type) e(n).prop("checked", n.value == s[n.name]);
                                        else if ("file" == n.type)(r = s[n.name]) && e("#" + n.name + "Preview").attr("src", mainUrl + r);
                                        else if ("select-multiple" == n.type) {
                                            var r;
                                            if (r = s[n.name.replace("[]", "")]) {
                                                var a = s[n.name.replace("[]", "")].split(",");
                                                e(n).val(a).trigger("change")
                                            }
                                        } else e(n).val(s[n.name]);
                                        e(n).parent().addClass("is-focused")
                                    }), t.processResponse && t.processResponse(s)
                                },
                                error: function(e, t) {
                                    r.processError ? r.processError(t) : n.MSG({
                                        type: "error"
                                    })
                                }
                            })
                        }();
                        break;
                    case "addModal":
                        a = "#" + t.id + "Form", e(a)[0].reset(), e("#" + t.id + "ModalLabel").html("Add " + t.id.replace("-", "")), e(a + ' button[type="submit"]').html("ADD"), e("#" + t.id + "Modal").modal("toggle"), e(a).CRUD({
                            url: t.url
                        });
                        break;
                    default:
                        e(n).on({
                            submit: function(s) {
                                s.preventDefault();
                                var a = [!1];
                                e(n).find('button[type="submit"]').prop("disabled", !0), e.each(e(n)[0], function(t, s) {
                                    var r = e(s).attr("data-validate");
                                    if (r) {
                                        var o = r.split("|");
                                        a.push(e.inArray(!0, n.VALIDATE({
                                            type: o,
                                            name: e(s)
                                        })) >= 0)
                                    }
                                }), e.inArray(!0, a) < 0 && (r.noAjax ? e(this)[0].submit() : function() {
                                    var s = new FormData(e(n)[0]);
                                    r.extraVariables && e.each(r.extraVariables(), function(t, n) {
                                        e.isArray(n) ? e.each(n, function(e, n) {
                                            s.append(t + "[]", n)
                                        }) : s.append(t, n)
                                    }), r.type = s.get("id") ? 2 : "" == r.type ? 0 : r.type;
                                    var a = "add";
                                    switch (r.type) {
                                        case 2:
                                            r.finalUrl = r.url + "/" + s.get("id"), s.append("_method", "patch"), a = "update";
                                            break;
                                        case 3:
                                            s.append("_method", "DELETE"), a = "deleteS"
                                    }
                                    var o = r.onLoad ? r.onLoad : a;
                                    n.MSG({
                                        type: o
                                    }), e.ajax({
                                        url: r.finalUrl,
                                        data: s,
                                        type: r.method[0],
                                        processData: !1,
                                        contentType: !1,
                                        dataType: "json",
                                        success: function(e) {
                                            t.processResponse && t.processResponse(e), n.MSG({
                                                type: e.msg
                                            })
                                        },
                                        error: function(e, t) {
                                            var s = void 0 === t.msg ? "error" : t.msg;
                                            r.processError ? r.processError(t) : n.MSG({
                                                type: s
                                            })
                                        }
                                    })
                                }()), e(n).find('button[type="submit"]').prop("disabled", !1)
                            }
                        })
                }
                var a
            }), this
        }, e.fn.MSG = function(t) {
            var s = e.extend({}, {
                id: "#formAlert",
                msgDetail: function(e) {
                    return {
                        add: {
                            msg: "Adding, Please wait!!",
                            css: "warning"
                        },
                        deleteS: {
                            msg: "Deleting, Please wait!!",
                            css: "warning"
                        },
                        check: {
                            msg: "Checking, Please wait!!",
                            css: "warning"
                        },
                        update: {
                            msg: "Updating, Please wait!!",
                            css: "warning"
                        },
                        success: {
                            msg: "Successfully Added!!",
                            css: "success"
                        },
                        successEnquiry: {
                            msg: "We got you Query, we'll get back to you soon!!",
                            css: "success"
                        },
                        successU: {
                            msg: "Successfully Updated!!",
                            css: "success"
                        },
                        successLogin: {
                            msg: "Successfully Logged in!!",
                            css: "success"
                        },
                        successRegister: {
                            msg: "Successfully Registered!!",
                            css: "success"
                        },
                        successSend: {
                            msg: "Quote Successfully sent!!",
                            css: "success"
                        },
                        block: {
                            msg: "Your account is blocked, please contact us for queries.",
                            css: "warning"
                        },                        
                        error: {
                            msg: "Something went wrong, Please try again!!",
                            css: "danger"
                        },
                        errorLogin: {
                            msg: "These credentials do not match our records.",
                            css: "danger"
                        },
                        errorD: {
                            msg: "Something went wrong, try again or these data must be linked with other child data!!",
                            css: "danger"
                        },
                        existEmail: {
                            msg: "The user is already present in the system. Please login using your credentials.",
                            css: "warning"
                        },
                        captcha: {
                            msg: "Please fill out Captcha.",
                            css: "warning"
                        },
                        loginSame: {
                            msg: "You are already logged in to other system.",
                            css: "warning"
                        },                        
                        password: {
                            msg: "Password should be of 8 character length",
                            css: "warning"
                        },
                        exist: {
                            msg: "Record Exist, Please try something new!!",
                            css: "warning"
                        },
                        same: {
                            msg: "Same as Previous, Please try something new!!",
                            css: "warning"
                        },
                        delete: {
                            msg: "Record Deleted Successfully!!",
                            css: "success"
                        },
                        image : {
                            msg: "Please Upload Image file",
                            css: "warning"
                        }
                    }[e]
                },
                input: function(e) {
                    return {
                        select: "Please select option",
                        empty: "These field couldn't be Empty",
                        mobile: "Invalid Mobile Number",
                        email: "Invalid Email address",
                        alphaSpace: "Name must contain characters and space",
                        minimum: "Should be a length of 8",
                        oneDigit: "Should contain 1 digit",
                        oneLower: "Should contain 1 Lowercase letter",
                        oneUpper: "Should contain 1 Uppercase letter",
                        oneSpecial: "Must contain 1 Special character like @,$,#,etc...",
                        password: "Password doesn't match",
                        digits: "Please enter digits",
                        date: "Invalid Date"
                    }[e]
                },
                type: "add",
                keep: !1,
                milisecond: 3000,
                for: "form"
            }, t);
            return timer = function(e) {
                window.setTimeout(function() {
                    e()
                }, s.milisecond)
            }, removeInputText = function(t) {
                e("#" + t).remove()
            }, inputText = function(t) {
                var n = e("<p/>", {
                    id: s.id,
                    style: "color: #db0c06;",
                    html: s.input(s.validationType)
                });
                removeInputText(s.id), e(t).after(n)
            }, formText = function(t) {
                e.each(e(t)[0].elements, function(t, s) {
                    e(s).attr("disabled", !0)
                }), e("<div/>", {
                    id: "formAlert",
                    class: "text-center alert alert-" + s.msgDetail(s.type).css,
                    html: s.msgDetail(s.type).msg
                }).appendTo(t);
                var n = ["success", "successU", "delete", "successRegister", "successEnquiry"],
                    r = e(s.id).parent().attr("id").replace("Form", "");
                timer(function() {
                    e.each(e(t)[0].elements, function(t, s) {
                        e(s).attr("disabled", !1)
                    }), e.inArray(s.type, n) > -1 && (e(t)[0].reset(), e("#" + r + "Modal").length && e("#" + r + "Modal").modal("toggle"), e(s.id).remove())
                }), e(t).find('button[type="submit"]').prop("disabled", !0)
            }, this.each(function() {
                switch (e(s.id).length && e(s.id).remove(), s.for) {
                    case "form":
                        formText(this);
                        break;
                    case "input":
                        inputText(this);
                        break;
                    case "remove":
                        removeInputText(s.id)
                }
            })
        }, e.fn.VALIDATE = function(t) {
            var s = e.extend({}, {
                    alphaSpace: /^[a-zA-Z\s]+$/,
                    empty: /^$/,
                    email: /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/,
                    mobile: /^\d{10}$/,
                    digits: /^\d$/,
                    select: /^-1$/,
                    date: /^([0-9][1-2])\/([0-2][0-9]|[3][0-1])\/((19|20)[0-9]{2})$/
                }, t),
                n = [];
            return e.each(s.type, function(t, r) {
                n.push(function(t, n) {
                    if (!t.attr("disabled")) {
                        var r = "empty" == n ? s[n].test(e(t).val()) : !s[n].test(e(t).val());
                        return "select" == n && (r = s[n].test(e(t).val())), "digits" == n && (r = s[n].test(e(t).val())), r ? (e(t).MSG({
                            id: e(t).attr("name") + n,
                            validationType: n,
                            for: "input"
                        }).focus(), !0) : (e(t).MSG({
                            id: e(t).attr("name") + n,
                            validationType: n,
                            for: "remove"
                        }), !1)
                    }
                }(s.name, r))
            }), n
        }
    }(jQuery);