<?php

return [

    'registration_success' => 'Thank you for choosing '.config('app.name').'. Your registration is succesfully done. Please check out our service and plans.',

    'registration_subject' => 'Registration confirmation.',

    'confirmation_success' => 'We have received payment of &#8377;:amount. Thank you. Your payment has been processed successfully. Please complete the KYC and Risk Profile Questionnaire to start our services.',

    'confirmation_subject' => 'Confirmation of Subscription',

    'new_subscription_admin' => ':user (:email) - :mobile paid &#8377;:amount. against '.config('app.name').'.',

    'new_subscription_admin_subject' => 'New Subscription Alert',

    'resume_subscription' => 'Your Subcription of &#8377;:amount is resumed.',

    'resume_subject' => 'Subscription is resumed',

    'cancel_subscription' => 'Your Subcription is cancelled, Please contact '.config('app.name').' admin for more details',

    'cancel_subscription_subject' => 'Cancellation of Subscription',

    'kyc_recieved' => 'Thank you. We have received your KYC details. We will inform you once it gets approved.',

    'kyc_recieved_subject' => 'KYC details received',

    'kyc_approve' => 'Your KYC details are approved. Your subscription is active subject to your completing the Risk Profile Questionnaire. Please, log-in and check. If you need any help please contact us.',

    'kyc_approve_subject' => 'Approval of KYC detail',

    'kyc_reject' => 'Your KYC details, is rejected please check uploaded document and try again.',

    'kyc_reject_subject' => 'Rejection of KYC detail',

    'risk_profile_recieved' => 'Thank you for completing the Risk Profile Questionnaire. Your Risk Profile score is :score. Please check complete details here.',

    'risk_profile_complete_subject' => 'Risk Profile Completion',

    'risk_profile_update_subject' => 'Updation of Risk Profile',
    
    'block_subject' => 'Blocking Subscription',

    'block' => 'We are blocking your subcription, please contact us for more details',

    'unblock_subject' => 'Un-Blocking Subscription',

    'unblock' => 'We are un-blocking your subcription',
];
