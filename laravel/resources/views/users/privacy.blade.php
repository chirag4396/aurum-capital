@extends('users.layouts.master')

@section('content')
<!-- privacy -->
<section class="privacy-main">
    <div class="container">
        <div class="row">
            <h1>Privacy Policy</h1>
            <hr>

            <h3>Personal information:</h3>
            <ol>
                <li><p>You are not required to provide your personal information to visit our website. </p></li>
                <li><p>We collect personal information from our visitors on a voluntary basis. Personal information may include name, title, company, address, phone number, email address, and other relevant data. Questions or comments submitted by visitors may also include personal information. </p>
                </li>
                <li><p>We collect and use personal information for business purposes in order: </p>
                </li>
                <ul>
                    <li><p>that you may download information and take advantage of certain other features of our website.</p> </li>
                    <li><p>to provide information or interactive services through this website, to your e-mail address or, where you - wish it to be sent by post, to your name and postal address.</p> </li>
                    <li><p>to seek your feedback or to contact you</p></li>
                    <li><p>to administer or otherwise carry out our obligations in relation to any agreement you have with us. </p></li>
                    <li><p>to create products or services that may meet your needs.</p></li>
                    <li><p>to process and respond to requests, assess, identify problems and improve system performance, and communicate with visitors about our products, services and businesses.</p></li>
                </ul>
                <li><p>We will not use, trade or share, with a third party, any personal information provided on our website for direct marketing purposes. Because of the nature of the Internet, we may transmit the information to another country, but among Aurum Capital and its affiliates, for purposes other than direct marketing, such as for storage, or for carrying out the processing detailed above, or because of where our servers are located, but we do not provide or use personal information to unrelated businesses for any marketing purposes. </p>
                </li>
                <li><p>To the extent required or permitted by law, we may also collect, use and disclose personal information in connection with security related or law enforcement investigations or in the course of cooperating with authorities or complying with legal requirements.</p> </li>
                <li><p>We may also remove all the personally identifiable information and use the rest for historical, statistical or scientific purposes.</p> </li>
                <li><p>Aurum Capital has the right to edit, remove or add any information at any time at its sole discretion.</p></li>
                <li><p>If you e-mail us, you are voluntarily releasing information to us. Your e-mail address will be used by Aurum Capital to respond to you. We may use the information that can identify you, such as your e-mail address, for direct marketing of our products.</p></li>
                <li><p>In addition, we may have collected similar information from you in the past. By entering this website you are consenting to the terms of our privacy policy and to our continued use of previously collected information. By submitting your personal information to us, you will be treated as having given your permission for the processing of your personal data as set out in this policy.</p></li>
                <li><p>We may collect additional information at other times, including but not limited to, when you provide feedback, change your content or email preferences, respond to a survey, or communicate with us.</p></li>
                <li><p>Registration data: When you register on the website, Application and for the Service, we ask that you provide basic contact Information such as your name, sex, age, address, pin code, contact number, occupation, interests and email address etc. When you register using your other accounts like on Facebook, Twitter, Gmail, LinkedIn etc. we shall retrieve Information from such account to continue to interact with you and to continue providing the Services.</p></li>
                <li><p>Subscription or paid service data:</p></li>
                <ul>
                    <li><p>When you chose any subscription or paid services, we or our payment gateway provider may collect your purchase, address or billing information, including your credit card number and expiration date etc.</p> </li>
                    <li><p>We also collect your KYC documents as per the requirement from the regulator and store in digital and non-digital formats.</p></li>
                </ul>
            </ol>

            <h3>Non personal information: </h3>
            <ol>
                <li><p>At this web site, information sent by your web browser, may be automatically collected. This information typically includes your domain name (the site after the @ in your e-mail address). It may also contain your user name (the name before the @ in your e-mail address). </p>
                </li>
                <li><p>We automatically collect limited information about your computer's connection to the Internet, mobile number, including your IP address, when you visit our site, application or service. Your IP address is a number that lets computers attached to the Internet know where to send you data -- such as the pages you view. We automatically receive and log information from your browser, including your IP address, your computer's name, your operating system, browser type and version, CPU speed, and connection speed. We may also collect log information from your device, including your location, IP address, your device’s name, device’s serial number or unique identification number (e.g. UDiD on your iOS device), your device operating system, browser type and version, CPU speed, and connection speed etc. </p>

                </li>
                <li><p>Other examples of information collected by our server include operating system and platform, the average time spent on our website, pages viewed, information searched for, access times, websites visited before a visitor visits our website, and other relevant statistics. The amount of information sent depends on the settings you have on your web browser; please refer to your browser if you want to learn what information it sends.</p>

                </li>
                <li><p>We may use information collected from the users to avoid the misuse of our services and platform.</p>
                </li>
                <li><p>We may from time to time supply the owners or operators of third party websites from which it is possible to link to our website with information relating to the number of users linking to our website from such third party website. You will not be identified from this information. </p>
                </li>
                <li><p>All such information will be used only to assist us in providing an effective service on this website. We at Aurum Capital can then develop statistics that are helpful to understanding how our visitors use this website. We use this information in the aggregate to the use of our website and to administer and improve our website. This statistical data is interpreted by Aurum Capital in its continuing effort to present the website content that visitors are seeking in a format they find most helpful. </p></li>
            </ol>

            <h3>Cookies: </h3>
            <ol>
                <li><p>We may store some information such as cookies on your computer when you look at our website. Cookies are pieces of information that a website transfers to the hard drive of a visitor's computer for record-keeping purposes. This information facilitates your use of our website and ensures that you do not need to re-enter your details every time you visit it. You can erase or choose to block this information from your computer if you want to; please refer to your browser settings to do so. Erasing or blocking such information may limit the range of features available to the visitor on our website. We also use such information to provide visitors a personalised experience on our website. We may use such information to allow visitors to use the website without logging on upon returning, to auto-populate email forms, to make improvements and to better tailor our website to our visitors' needs. We also use this information to verify that visitors meet the criteria required to process their requests.</p>
                </li>
            </ol>

            <h3>Security: </h3>
            <ol>
                <li><p>We have implemented technology and policies, with the objective of protecting your privacy from unauthorised access and improper use, and periodically review the same. However, our website is on the World Wide Web and internet security is a very dynamic issue. Hence, in cases where there is unauthorised access to or loss of data from aurumcapital.in by hackers and other "cyber vandals", viruses and other technical breakdowns resulting in the website being de-faced, presenting incorrect or inaccurate information, or experiencing downtime, Aurum Capital will not be responsible. </p>
                </li>
            </ol>

            <h3>Foreign Jurisdictions: </h3>
            <ol>
                <li><p>The contents herein – information or views – do not amount to distribution, guidelines, an offer or solicitation of any offer to buy or sell any securities or financial instruments, directly or indirectly, in the United States of America (US), in Canada, in jurisdictions where such distribution or offer is not authorized and in FATF non-compliant jurisdiction and are particularly not for US persons (being persons resident in the US, corporations, partnerships or other entities created or organized in or under the laws of the US or any person falling within the definition of the term “US person” under Regulation S promulgated under the US Securities Act of 1933, as amended) and persons of Canada. Users who are accessing this website from outside the jurisdiction of India shall be solely responsible for the compliance with their local laws and for the consequential liability incurred in the event of violation of such local laws. </p>
                </li>
                <li><p>We consider ourselves and intend to be subject to the jurisdiction only of the Courts in Pune, India. If you don't agree with above please do not read the material on any of our pages.</p></li>
            </ol>

            <h3>Third Parties:  </h3>
            <ol>
                <li><p>For your convenience, this page may contain certain hyperlinks to other pages as well as to websites outside aurumcapital.in. In addition, you may have linked to our website from another website. Aurum Capital is not responsible for the contents, privacy policies and practices of other websites, even if you access them using links from our website. We make no promises or guarantees regarding data collection on the hyper-linked pages and on websites that are not owned by Aurum Capital. We recommend that you check the policy of each website you visit, or link from, and contact the owners or operators of such websites if you have any concerns or questions.</p>
                </li>
                <li><p>Information from other Sources: We may receive information about you from other sources, add it to our account information and treat it in accordance with this policy. If you provide information to the platform provider or other partner, whom we provide services, your account information and order information may be passed on to us. We may obtain updated contact information from third parties in order to correct our records and fulfil the Services or to communicate with you.</p></li>
            </ol>
            
            <h3>Demographic and purchase information:</h3>
            <ol>
                <li><p>We may reference other sources of demographic and other information in order to provide you with more targeted communications and promotions. We use Google Analytics, among others, to track the user behaviour on our website. Google Analytics specifically has been enable to support display advertising towards helping us gain understanding of our users' Demographics and Interests. The reports are anonymous and cannot be associated with any individual personally identifiable information that you may have shared with us. You can opt-out of Google Analytics for Display Advertising and customize Google Display Network ads using the Ads Settings options provided by Google.</p>
                </li>
            </ol>
            
            <h3>LINKS TO THIRD PARTY SITES / AD-SERVERS</h3>
            <ol>
                <li><p>The Application may include links to other websites or applications. Such websites or applications are governed by their respective privacy policies, which are beyond our control. Once you leave our servers (you can tell where you are by checking the URL in the location bar on your browser), use of any information you provide is governed by the privacy policy of the operator of the application, you are visiting. That policy may differ from ours. If you can't find the privacy policy of any of these sites via a link from the application's homepage, you should contact the application owners directly for more information. When we present information to our advertisers -- to help them understand our audience and confirm the value of advertising on our websites or Applications -- it is usually in the form of aggregated statistics on traffic to various pages / content within our websites or Applications. We use third-party advertising companies to serve ads when you visit our websites or Applications. These companies may use information (not including your name, address, email address or telephone number or other personally identifiable information) about your visits to this and other websites or application, in order to provide advertisements about goods and services of interest to you. We do not provide any personally identifiable information to third party websites / advertisers / ad-servers without your consent.
                Modification of these terms of use, privacy policy and disclaimer</p></li>
                <li><p>Aurum Capital reserves the right to change, without notice, this Terms of Use, Privacy Policy and Disclaimer under which the Service is offered. The User's continued use of the Service will be subject to the Terms of Use in force at the time of the Use.</p>
                </li>
                <li><p>You agree to review these terms of use, Privacy Policy and Disclaimer periodically since subsequent use by you of this site shall constitute your acceptance of any changes. Aurum Capital shall have the right at any time to change or discontinue any aspect of aurumcapital.in, including, but not limited to, the community areas, content, hours of availability and equipment needed for access to use. Such changes, modifications, additions or deletions shall be effective immediately upon posting and any subsequent use by you after such posting shall conclusively be deemed to be acceptance by you of such changes, modifications or deletions.</p>
                </li>
            </ol>
            
            <h3>Contacting us: </h3>
            <ol>
                <li><p>We aim to keep our information about you as accurate as possible. If you would like to review or change the details you have supplied us with, please contact us as set out below. If you wish to change or delete any of the personal information you have entered whilst visiting our website or if you have any questions about our privacy statement kindly e-mail at support@aurumcapital.in</p>

                </li>
            </ol>
        </div>
    </div>
</section>
<!-- /privacy -->
@endsection