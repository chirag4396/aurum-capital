@extends('users.layouts.master')

@section('content')
<!-- section1 -->
<section class="performance-sec1">
	<div class="container">
		<h3>Consistent Track Record</h3>
		<p>Few great stocks can make your entire investment career. Why trade or jump in & out of 100's of so called HOT stock tips. </p>
		<div class="row">
			<div class="table-responsive">
				<table id="example" class="display" style="width:100%">
					<thead>
						<tr class="table-bg">
							<th>Stock Name</th>
							<th>% Allocation</th>
							<th>Month/Year</th>
							<th>Price Range</th>
							<th>Sell Price</th>
							<th>Total Dividend Received (if any)</th>
							<th>Total of Price + Dividend</th>
							<th>Gains/ Loss from Recommended Price</th>
						</tr>
					</thead>
					<tbody>

						<tr>
							<td>Tiger Nixon</td>
							<td>60%</td>
							<td>2016</td>
							<td>45</td>
							<td>89</td>
							<td>90</td>
							<td>89</td>
							<td>90</td>
						</tr>

						<tr>
							<td>Tiger Nixon</td>
							<td>60%</td>
							<td>2016</td>
							<td>45</td>
							<td>89</td>
							<td>90</td>
							<td>89</td>
							<td>90</td>
						</tr>
						<tr>
							<td>Tiger Nixon</td>
							<td>60%</td>
							<td>2016</td>
							<td>45</td>
							<td>89</td>
							<td>90</td>
							<td>89</td>
							<td>90</td>
						</tr>
						<tr>
							<td>Tiger Nixon</td>
							<td>60%</td>
							<td>2016</td>
							<td>45</td>
							<td>89</td>
							<td>90</td>
							<td>89</td>
							<td>90</td>
						</tr>
						<tr>
							<td>Tiger Nixon</td>
							<td>60%</td>
							<td>2016</td>
							<td>45</td>
							<td>89</td>
							<td>90</td>
							<td>89</td>
							<td>90</td>
						</tr>
						<tr>
							<td>Tiger Nixon</td>
							<td>60%</td>
							<td>2016</td>
							<td>45</td>
							<td>89</td>
							<td>90</td>
							<td>89</td>
							<td>90</td>
						</tr>
						<tr>
							<td>Tiger Nixon</td>
							<td>60%</td>
							<td>2016</td>
							<td>45</td>
							<td>89</td>
							<td>90</td>
							<td>89</td>
							<td>90</td>
						</tr>
						<tr>
							<td>Tiger Nixon</td>
							<td>60%</td>
							<td>2016</td>
							<td>45</td>
							<td>89</td>
							<td>90</td>
							<td>89</td>
							<td>90</td>
						</tr>
						<tr>
							<td>Tiger Nixon</td>
							<td>60%</td>
							<td>2016</td>
							<td>45</td>
							<td>89</td>
							<td>90</td>
							<td>89</td>
							<td>90</td>
						</tr>
						<tr>
							<td>Tiger Nixon</td>
							<td>60%</td>
							<td>2016</td>
							<td>45</td>
							<td>89</td>
							<td>90</td>
							<td>89</td>
							<td>90</td>
						</tr>
						<tr>
							<td>Tiger Nixon</td>
							<td>60%</td>
							<td>2016</td>
							<td>45</td>
							<td>89</td>
							<td>90</td>
							<td>89</td>
							<td>90</td>
						</tr>
						<tr>
							<td>Tiger Nixon</td>
							<td>60%</td>
							<td>2016</td>
							<td>45</td>
							<td>89</td>
							<td>90</td>
							<td>89</td>
							<td>90</td>
						</tr>
						<tr>
							<td>Tiger Nixon</td>
							<td>60%</td>
							<td>2016</td>
							<td>45</td>
							<td>89</td>
							<td>90</td>
							<td>89</td>
							<td>90</td>
						</tr>
						<tr>
							<td>Tiger Nixon</td>
							<td>60%</td>
							<td>2016</td>
							<td>45</td>
							<td>89</td>
							<td>90</td>
							<td>89</td>
							<td>90</td>
						</tr>
						<tr>
							<td>Tiger Nixon</td>
							<td>60%</td>
							<td>2016</td>
							<td>45</td>
							<td>89</td>
							<td>90</td>
							<td>89</td>
							<td>90</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</section>
<!-- /section1 -->	
@endsection