@extends('users.layouts.master')
@push('header')
<script>
	function answer(id){
		$('#'+id).CRUD({
			url : '{{ route('forum-answer.store') }}'
		});
	}
</script>
@endpush
@section('content')

@php
function attrReplies($ans){
	$replies = $ans->replies();
	if($replies->count()){		
		$data = '';
		$data .= '<ul>';
		foreach ($replies->get() as $k => $v) {
			$rcount = $v->replies()->count();
			$data .= '<li>'
			.'<p>'.$v->fa_answer.'</p>'
			.'<span class="investor-reply">'
			.'<span>- '.$v->user->name.($v->user->type == 101 ? '(Admin)' : ''). ' </span>'	
			.'<a href="javascript:void(0)" onclick="reply('.$v->fa_id.');"><i class="fa fa-reply"></i> Reply '.($rcount ? '('.$rcount.')' : '').'</a>'
			.'</span>'
			.attrReplies($v)
			.'</li>';
		}
		$data .= '</ul>';

		return $data;
	}
}
@endphp
<section class="investor">
	<div class="container">
		<h1>Investor Forum <span class="investor-ask" data-toggle="modal" data-target="#investorModal">Ask Question</span></h1>
		<div class="row">
			<div class="panel-group" id="accordion">
				<div class = "jscroll">
					@forelse($investor_forums as $k => $if)
					<div class="panel panel-default">
						<div class="panel-heading">
							<a data-toggle="collapse" class="panel-title investor-title" data-parent="#accordion" href="#if{{ $if->if_id }}">
								<div>
									{{ $if->if_question }}
									<div>
										<ul>
											<li><span>Asked By:</span> {{ $if->if_anonymous ? 'Anonymous' : $if->user->name }}</li>
											<li><span>Posted on:</span> {{ $if->if_created_at->format('jS M, Y - h:i A') }}</li>
											<li>Comment <span class="badge">{{ $if->answers()->where(['fa_approve' => 1])->count() }}</span></li>
										</ul>
									</div>
								</div>
							</a>
						</div>
						<div id="if{{ $if->if_id }}" class="panel-collapse collapse">
							<div class="panel-body">
								<div class="investor-ans">
									<div class="row">

										@forelse ([1,0] as $ad)
										@forelse ($if->answers()->where(['fa_admin' => $ad, 'fa_approve' => 1])->get() as $aAns)
										@php
										$rCount = $aAns->replies()->count();
										@endphp
										<ul class="investor-ans-ul">
											<li>
												<p>{{ $aAns->fa_answer }}</p>
												<span class="investor-reply">
													<span>- {{ $aAns->user->name }} {{ $ad == 1 ? '(Admin)' : '' }}</span> 
													<a href="javascript:void(0)" onclick="reply('{{ $aAns->fa_id }}');"><i class="fa fa-reply"></i> Reply {{ $rCount ? '('.$rCount.')' : '' }}</a>
												</span>
												{!! attrReplies($aAns) !!}
											</li>
										</ul>
										<hr>
										@empty

										@endforelse
										@empty

										@endforelse
										@include('users.includes.answer_forms',['if' => $if])
									</div>
								</div>
							</div>
						</div>
					</div>	
					
					@empty
					@endforelse
					<div class="hidden">
						{!! $investor_forums->links() !!}
					</div>
				</div>
			</div> 
		</div>
	</div>
</section>

<div id="investorModal" class="modal fade" role="dialog">
	<div class="modal-dialog ananomis-modal">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close signup-close" data-dismiss="modal"><i class="fa fa-times"></i></button>
				<h4 class="modal-title">Ask Question Here</h4>
			</div>
			<div class="modal-body">
				<form id = "investorForm">
					<div class="form-group">
						<textarea class="form-control" cols="4" name = "question"></textarea>
					</div>
					<div class="checkbox">
						<label><input type="checkbox" name="anonymous"> Stay Anonymous</label>
					</div>
					<button type="submit" class="btn btn-default">Submit</button>
				</form>
			</div>
		</div>

	</div>
</div>

<div id="answerRModal" class="modal fade" role="dialog">
	<div class="modal-dialog ananomis-modal">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close signup-close" data-dismiss="modal"><i class="fa fa-times"></i></button>
				<h4 class="modal-title"></h4>
			</div>
			<div class="modal-body">
				<form id = "answerRForm">
					<input type="hidden" name="user" value="{{ Auth::id() }}">
					<input type="hidden" name="question" value="{{ $if->if_id }}">
					<input type="hidden" name="answer_on" id = "ansOn">
					<div class="form-group">
						<textarea class="form-control" placeholder="Your Comment" rows="3" name = "answer"></textarea>
					</div>
					<div class="form-group">
						<input type="submit" value="Add Comment" class="investor-btn">
					</div>
				</form>
			</div>
		</div>

	</div>
</div>
@endsection

@push('footer')
<script src="//unpkg.com/jscroll/dist/jquery.jscroll.min.js"></script>
<script>
	$(function() {
		$('.jscroll').jscroll({
			autoTrigger : true,
			nextSelector : '.pagination li.active + li a',
			contentSelector : 'div.jscroll',
			callback: function(){
				$('.pagination:visible:first').hide();
			},
			loadingHtml : '<div class = "loading-forum">Loading...</div>'
		});
	});
	$('#investorForm').CRUD({
		url : '{{ route('investor-forum.store') }}',
		processResponse : function (data){
			if(data.msg == 'success'){

			}
		},
		onLoad : 'check'
	});

	function reply(id){
		$('#answerRModal').modal('toggle');
		$('#ansOn').val(id);
	}

	$('#answerRForm').CRUD({
		url : '{{ route('forum-answer.store') }}'
	});
</script>
@endpush