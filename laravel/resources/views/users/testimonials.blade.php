@extends('users.layouts.master')

@section('content')
<!-- section1 -->	
<section class="client-sec1">
	<div class="client-sec1-sub">
		<div class="container">
			<div class="row">
				<h1>Testimonials</h1>
				<p><span>O</span>ur valued clients speak some kind words for us. </p>
				<div id="myCarousel" class="carousel slide" data-ride="carousel">

					<ol class="carousel-indicators">
						<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
						<li data-target="#myCarousel" data-slide-to="1"></li>
						<li data-target="#myCarousel" data-slide-to="2"></li>
					</ol>


					<div class="carousel-inner">
						<div class="item active">
							<div class="row">
								<div class="col-lg-4 col-md-4 colsm-6 col-xs12">
									<div class="client-sec1-img">
										<img src="img/testi1.jpg" alt="img">
									</div>
								</div>
								<div class="col-lg-8 col-md-8 colsm-6 col-xs12">
									<div class="client-sec1-text">
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
										<h4>- Testimonial Name</h4>
									</div>
								</div>
							</div>	
							<br><br>

							<div class="row">
								<div class="col-lg-4 col-md-4 colsm-6 col-xs12">
									<div class="client-sec1-img">
										<img src="img/testi1.jpg" alt="img">
									</div>
								</div>
								<div class="col-lg-8 col-md-8 colsm-6 col-xs12">
									<div class="client-sec1-text">
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
										<h4>- Testimonial Name</h4>
									</div>
								</div>
							</div>								
						</div>

						<div class="item">
							<div class="row">
								<div class="col-lg-4 col-md-4 colsm-6 col-xs12">
									<div class="client-sec1-img">
										<img src="img/testi1.jpg" alt="img">
									</div>
								</div>
								<div class="col-lg-8 col-md-8 colsm-6 col-xs12">
									<div class="client-sec1-text">
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
										<h4>- Testimonial Name</h4>
									</div>
								</div>
							</div>	

							<br><br>

							<div class="row">
								<div class="col-lg-4 col-md-4 colsm-6 col-xs12">
									<div class="client-sec1-img">
										<img src="img/testi1.jpg" alt="img">
									</div>
								</div>
								<div class="col-lg-8 col-md-8 colsm-6 col-xs12">
									<div class="client-sec1-text">
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
										<h4>- Testimonial Name</h4>
									</div>
								</div>
							</div>	
						</div>

						<div class="item">
							<div class="row">
								<div class="col-lg-4 col-md-4 colsm-6 col-xs12">
									<div class="client-sec1-img">
										<img src="img/testi1.jpg" alt="img">
									</div>
								</div>
								<div class="col-lg-8 col-md-8 colsm-6 col-xs12">
									<div class="client-sec1-text">
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
										<h4>- Testimonial Name</h4>
									</div>
								</div>
							</div>	

							<br><br>

							<div class="row">
								<div class="col-lg-4 col-md-4 colsm-6 col-xs12">
									<div class="client-sec1-img">
										<img src="img/testi1.jpg" alt="img">
									</div>
								</div>
								<div class="col-lg-8 col-md-8 colsm-6 col-xs12">
									<div class="client-sec1-text">
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
										<h4>- Testimonial Name</h4>
									</div>
								</div>
							</div>	
						</div>
					</div>					

				</div>
			</div>
		</div>
	</div>
</section>
<!-- /section1 -->	
@endsection