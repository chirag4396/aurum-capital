@extends('users.layouts.master')

@section('content')
<section class="faq">
	<div class="container">
		<h1>Our investment philosophy and frequently asked questions</h1>
		<div class="panel-group" id="accordion">
			@forelse ($faqs as $fk => $faq)
			<div class="panel panel-default faq-panel">
				<div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#faq{{ $faq->faq_id }}">
					<h4 class="panel-title">
						<a>{!! $faq->faq_qus !!}</a>
					</h4>
				</div>
				<div id="faq{{ $faq->faq_id }}" class="panel-collapse collapse {{ !$fk ? 'in' : ''}}">
					<div class="panel-body">
						{!! $faq->faq_ans !!}
					</div>
				</div>
			</div>	
			@empty
				<div>
					No Faq Found
				</div>
			@endforelse
			
{{-- 
			<div class="panel panel-default faq-panel">
				<div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapse2">
					<h4 class="panel-title">
						<a>What kind of stocks do you recommend?</a>
					</h4>
				</div>
				<div id="collapse2" class="panel-collapse collapse">
					<div class="panel-body">We are market-cap agnostic. We suggest stocks which are above a threshold market-cap with sufficient liquidity. If we find value and suitable opportunity then we can go for large cap stocks, however, our primary focus remains mid and small-caps.
					</div>
				</div>
			</div>

			<div class="panel panel-default faq-panel">
				<div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapse3">
					<h4 class="panel-title">
						<a>How many stocks do you recommend in a year and what is the frequency of your recommendations?</a>
					</h4>
				</div>
				<div id="collapse3" class="panel-collapse collapse">
					<div class="panel-body">We give about 8-10 recommendations a year. With suggested portfolio weight in each of the companies. We give periodic updates on the recommendations. Though our endeavor is to give these 8-10 recommendations in a year, we give these based on market conditions. If market is not supportive or if we feel there is over-valuation, we may give fewer recommendations than this. In such a case where we give less than 8 stocks in a year, we will be extending the subscription at no cost to the subscriber till a minimum of 8 stocks in the subscription period is given. The idea is to not give recommendations just for the sake of it. The primary importance is given to the interest of our subscribers. If during a year we end up giving more than 10 stocks then also we will not reduce the subscription period of the user.
					</div>
				</div>
			</div>

			<div class="panel panel-default faq-panel">
				<div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapse4">
					<h4 class="panel-title">
						<a>Who should buy your services?</a>
					</h4>
				</div>
				<div id="collapse4" class="panel-collapse collapse">
					<div class="panel-body"><li>The investors who have a minimum investible amount of Rs 8 lakhs or above to invest (over a year) should ideally subscribe to our services. At this minimum amount it may make an economic sense.</li> 
						<li>The investors who are planning to invest for a minimum of 3 years. Our services might not be suitable for investors who have a short-term horizon of less than 3 years. Our services are suited for long-term investors.</li>
						<li>Investors who understand that during corrections and bear markets, portfolio can turn negative, though our endeavor will be to protect the investment of our subscribers with a fall lesser than the market during these times.</li>
					</div>
				</div>
			</div>

			<div class="panel panel-default faq-panel">
				<div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapse5">
					<h4 class="panel-title">
						<a>How many times you would be giving updates on a stock idea under coverage?</a>
					</h4>
				</div>
				<div id="collapse5" class="panel-collapse collapse">
					<div class="panel-body">We give periodic updates as and when it is necessary. Similarly, we will be giving exit calls in the recommended investments.
					</div>
				</div>
			</div>

			<div class="panel panel-default faq-panel">
				<div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapse6">
					<h4 class="panel-title">
						<a>Should I buy a stock if it has gone beyond the buy price range suggested by you?
						</a>
					</h4>
				</div>
				<div id="collapse6" class="panel-collapse collapse">
					<div class="panel-body">We do not recommend buying beyond the range. Our returns will be benchmarked against the buy price we suggest. This will ensure minimum variance from benchmark returns and subscriber returns.
					</div>
				</div>
			</div>

			<div class="panel panel-default faq-panel">
				<div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapse7">
					<h4 class="panel-title">
						<a>What does hold price mean? Can I add such stocks if I do not own it?
						</a>
					</h4>
				</div>
				<div id="collapse7" class="panel-collapse collapse">
					<div class="panel-body">Hold recommendation means subscriber doesn't have to do anything at that point in time and keep holding the stock till follow-up action (add, sell) is advised. For subscribers who joined after a recommendation is given, we suggest to add only in buying range, provided we have not given exit call.
					</div>
				</div>
			</div>

			<div class="panel panel-default faq-panel">
				<div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapse8">
					<h4 class="panel-title">
						<a>Do you look at valuations or market moment?</a>
					</h4>
				</div>
				<div id="collapse8" class="panel-collapse collapse">
					<div class="panel-body">We only look at valuations as our core philosophy is value investing.
					</div>
				</div>
			</div>
			
			<div class="panel panel-default faq-panel">
				<div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapse9">
					<h4 class="panel-title">
						<a>Do you give any guaranteed returns?</a>
					</h4>
				</div>
				<div id="collapse9" class="panel-collapse collapse">
					<div class="panel-body">No, we do not promise any guaranteed returns. Equity investing involves risks, especially for shorter terms and subscriber must understand these risks. 
					</div>
				</div>
			</div>
			
			<div class="panel panel-default faq-panel">
				<div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapse10">
					<h4 class="panel-title">
						<a>How many multibagger stock ideas would you give in a year?</a>
					</h4>
				</div>
				<div id="collapse10" class="panel-collapse collapse">
					<div class="panel-body">Multibaggers are not planned. They happen during the course of investing. This requires holding and staying put with your winners as long as they are not overpriced. We will be CAGR (compound annual growth rate) focused on our portfolio. As ultimately, CAGR on your portfolio is more important than multibaggers.
					</div>
				</div>
			</div>
			
			<div class="panel panel-default faq-panel">
				<div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapse11">
					<h4 class="panel-title">
						<a>Would you be also recommending any microcap, penny and SME stocks?</a>
					</h4>
				</div>
				<div id="collapse11" class="panel-collapse collapse">
					<div class="panel-body">We are market-cap agnostic. But we avoid penny stocks. We will be having a threshold market-cap for our recommendations. We will also be considering liquidity in the stocks we recommend. The illiquid stocks have high impact cost and it might be difficult to sell them.
					</div>
				</div>
			</div>
			
			<div class="panel panel-default faq-panel">
				<div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapse12">
					<h4 class="panel-title">
						<a>What are the subscription charges? What plans do you offer? Are there any other charges besides subscription fee? Is there any discount?
						</a>
					</h4>
				</div>
				<div id="collapse12" class="panel-collapse collapse">
					<div class="panel-body">The subscription charges and plans are available on the Services and Pricing page. We do not offer any discounts.
					</div>
				</div>
			</div>
			
			<div class="panel panel-default faq-panel">
				<div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapse13">
					<h4 class="panel-title">
						<a>Will recommendations come in the form of research reports?</a>
					</h4>
				</div>
				<div id="collapse13" class="panel-collapse collapse">
					<div class="panel-body">Yes. We will be publishing research reports on our recommendations. We will try to make them, simple and easy-to-understand. We will be doing both qualitative and quantitative research. That includes understanding financials and keeping track of the business on an ongoing basis.
					</div>
				</div>
			</div>
			
			
			
			<div class="panel panel-default faq-panel">
				<div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapse14">
					<h4 class="panel-title">
						<a>What returns can one expect from the recommendations?</a>
					</h4>
				</div>
				<div id="collapse14" class="panel-collapse collapse">
					<div class="panel-body">Our endeavor is generate higher returns than markets. Basic philosophy is to look at stocks that can double in 3 years. But, it does depend on market conditions and company performance. We will actively be tracking both. However, we do not guarantee any returns.
					</div>
				</div>
			</div>
			
			<div class="panel panel-default faq-panel">
				<div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapse15">
					<h4 class="panel-title">
						<a>Can the stocks and portfolio recommended turn negative?</a>
					</h4>
				</div>
				<div id="collapse15" class="panel-collapse collapse">
					<div class="panel-body">Yes, the portfolio can definitely turn negative if there is correction in the markets or during bear phase. The volatility exists in the stock markets and that is why we suggest to have a long term view on the markets. In long term, chances of losing money in the right portfolio reduces significantly.
					</div>
				</div>
			</div>
			
			<div class="panel panel-default faq-panel">
				<div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapse16">
					<h4 class="panel-title">
						<a>Will you be advising when to sell the recommended stocks?
						</a>
					</h4>
				</div>
				<div id="collapse16" class="panel-collapse collapse">
					<div class="panel-body">Yes. We will be suggesting when to fully or partially exit our recommendations. Partial exits, will be in percentage. For e.g. we suggest subscriber to sell 30% of position in stock “A” in the range of X-Y.
					</div>
				</div>
			</div>
			
			<div class="panel panel-default faq-panel">
				<div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapse17">
					<h4 class="panel-title">
						<a>When will my account be activated?
						</a>
					</h4>
				</div>
				<div id="collapse17" class="panel-collapse collapse">
					<div class="panel-body">Once you decide to subscribe, and make payment through IMPS/NEFT/RTGS, please send an email to info@aurumcapital.in mentioning your name,account number, bank name, transaction number and the amount transferred. Once, these details are verified from our bank account,subsciber will be asked to fulfill the KYC and Risk Profile questionnaire (as  mandated by regulator). Once , these are verified your account will be active

					</div>
				</div>
			</div> --}}

		</div> 
	</div>
</section>
@endsection