@extends('users.layouts.master')

@push('header')
<link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css') }}">
<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
@endpush

@section('content')
<section class="current">
	<div class="container">
		<div class="row">
			<h1>Current Recommendation </h1>
			<div class="row">
				<div class="table-responsive">
					<table id="example" class="display" style="width:100%">
						<thead>
							<tr class="table-bg">
								<th>Stock</th>
								<th>Buy/ Sell/ Hold</th>
								<th>% Allocation</th>
								<th>Date</th>
								<th>Buy Range (Price Range)</th>
								<th>Closing Price <br>(Date)</th>
								<th>Sell Price</th>
								<th>Dividend</th>
								<th>% Gains/ Loss</th>
								<th>Reports</th>
							</tr>
						</thead>
						<tbody>
							@php
								function getValues($arr) {
									if(!empty($arr)) {
										$arrs = explode(',', $arr);
										return '&#8377;'.number_format($arrs[0]).' - '.number_format($arrs[1]);
									}
									return '-';
								}
							@endphp
							@forelse ($stocks as $stock)
							@php
							$ls = $stock->latestRecommendation();

							$ranges = explode(',', $ls->cr_range);
							$sell_prices = explode(',', $ls->cr_sell_price);

							$closing = empty($ls->cr_closing_price) ? $stock->latestClosingPrice->first()->cp_value : $ls->cr_closing_price;

							switch($ls->action->ac_id){
								case 2:
								$percent = (($closing + $ls->cr_dividend) - $sell_prices[2])/$sell_prices[2];
								break;
								default:
								$percent = (($closing + $ls->cr_dividend) - $ranges[2])/$ranges[2];
								break;
							}
							$tdClass = ($percent < 0) ? 'current-td-red' : 'current-td-green';
							@endphp
							<tr class='clickable-row' data-href='{{ route('stock-reports',['id' => $stock->st_id, 'name' => strtolower(str_replace(' ', '-', trim($stock->st_title)))]) }}'>
								<td>{{ $stock->st_title }}</td>
								<td>{{ $ls->action->ac_title }}</td>
								<td>{{ $ls->cr_percent_allocation.'%' }}</td>
								<td>{{ \Carbon\Carbon::parse($ls->cr_date)->format('jS M, Y') }}</td>
								<td>{{ getValues($ls->cr_range) }}</td>
								<td>{{ '&#8377;'.number_format($closing, 2) }}</td>
								<td>{{ getValues($ls->cr_sell_price) }}</td>
								<td>{{ '&#8377;'.number_format($ls->cr_dividend, 2) }}</td>
								<td class="{{ $tdClass }}">{{ number_format($percent, 2) }}%</td>
								<td><span class="home-login2">View</span></td>
							</tr>
							@empty
							@endforelse
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection

@push('footer')
<script type="text/javascript">
	$(document).ready(function() {
		/*Setup - add a text input to each footer cell*/
		$('#example tfoot th').each( function () {
			var title = $(this).text();
			$(this).html( '<input type="text" placeholder="Search '+title+'" />' );
		} );

		/*DataTable*/
		var table = $('#example').DataTable();

		/*Apply the search*/
		table.columns().every( function () {
			var that = this;
			$( 'input', this.footer() ).on( 'keyup change', function () {
				if ( that.search() !== this.value ) {
					that
					.search( this.value )
					.draw();
				}
			});
		});
		$(".clickable-row").click(function() {
			window.location = $(this).data("href");
		});
	});	
</script>
@endpush