@extends('users.layouts.master')

@section('content')

<!-- /section1 -->
<section class="about-sec1">
	<div class="container">
		<h1>About Founders</h1>
		<div class="about-sec1-mid">
			<div class="row">
				<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
					<img src="img/Jiten Parmar.png" alt="">
					<h2>Jiten Parmar</h2>
					<h3>Partner</h3>
					<div class="social-icon">
						<a href="https://www.facebook.com/jitenp69"><i class="fa fa-facebook"></i></a>
						<a href="https://twitter.com/jitenkparmar"><i class="fa fa-twitter"></i></a>								
						<a href="https://www.linkedin.com/in/jiten-parmar-1b597918/"><i class="fa fa-linkedin"></i></a>
					</div>
				</div>
				<div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
					<p>Jiten is an entrepreneur and a passionate long-term equity investor. Jiten has been investing in the market since 1993. He started investing in Indian equity market since 2002 and prior to that he invested in the US markets.</p>
					<p> He has conducted various sessions presenting his approach to investing, what investors should look out when analyzing companies and the behavioral aspects in investing. Jiten is very well-known in the Indian equity markets and has shared his knowledge and experiences in various forums. Jiten is also a successful entrepreneur.</p>
					<p> He started his entrepreneurship journey in mid-90 when he co-founded an IT firm in the USA. Later, he successfully exited by selling it to a Nasdaq-listed company at the peak of tech cycle. He returned to India and started several successful ventures spanning multiple industries including real estate and
					flexible laminates industries.</p>

				</div>
			</div>
		</div>

		<hr>

		<div class="about-sec1-mid">
			<div class="row">
				<div class="col-lg-5 col-lg-push-5 col-md-5 col-md-push-5 col-sm-5 col-sm-push-5 col-xs-12">
					<img src="img/Niteen S Dharmawat.jpg" alt="">
					<h2>Niteen S Dharmawat</h2>
					<h3>Partner</h3>
					<div class="social-icon">
						<a href="https://www.facebook.com/dharmawat"><i class="fa fa-facebook"></i></a>
						<a href="https://twitter.com/niteen_india"><i class="fa fa-twitter"></i></a>

						<a href="https://www.linkedin.com/in/dharmawat/"><i class="fa fa-linkedin"></i></a>
					</div>
				</div>
				<div class="col-lg-7 col-lg-pull-7 col-md-7 col-md-pull-7 col-sm-7 col-sm-pull-7 col-xs-12">
					<p>Niteen has worked with leading groups including CRISIL and L&amp;T among others. He started his equity market journey 20 years ago as a hobby and ended up pursuing CFA, from CFA institute USA.</p>
					<p> His focus is long term, value investment ideas with sound fundamentals. He is a seasoned professional with areas of expertise spanning across research, corporate sales, marketing and key account management.</p>
					<p>As a part of his social responsibility, he conducted several free investor education sessions. He is a continuous learner in equity market and believes that &quot;Everything else can stop but learning&quot;.</p>

				</div>
			</div>
		</div>
	</div>
</section>
<!-- /section1 -->		
@endsection