@extends('users.layouts.master')

@section('content')
<!-- section1 -->
<section class="blog-sec1"> 
	<div class="container">				
		<div class="row">

			<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
				<div class="blog-all">
					<h1>All Blogs</h1>
					<hr>
					<ul class="nav nav-tabs nav-stacked">
						<li class="active"><a data-toggle="tab" href="#home">2018</a></li>
						<li><a data-toggle="tab" href="#menu1">2017</a></li>
						<li><a data-toggle="tab" href="#menu2">2016</a></li>
					</ul>								
				</div>
			</div>	

			<div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">

				<div class="tab-content">
					<div id="home" class="tab-pane fade in active">
						<div class="blog-sec1-mid">
							<div class="row">								
								<div class="blog-sec1-text">
									<h1>Article Name</h1>
									<ul>
										<li class="blog-li1"><span>By:</span> Author Name</li>
										<li class="blog-li3">Comment</li>
										<li class="blog-li2"><span>Posted on:</span> 14<sup>th</sup> April 2018</li>
										<li class="blog-li4">Like</li>	
									</ul>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.</p>
									<a href="#" class="blog-btn">Read More</a>
								</div>
							</div>								
						</div>

						<div class="blog-sec1-mid">
							<div class="row">									
								<div class="blog-sec1-text">
									<h1>Article Name</h1>
									<ul>
										<li class="blog-li1"><span>By:</span> Author Name</li>
										<li class="blog-li3">Comment</li>
										<li class="blog-li2"><span>Posted on:</span> 14<sup>th</sup> April 2018</li>
										<li class="blog-li4">Like</li>	
									</ul>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.</p>
									<a href="#" class="blog-btn">Read More</a>
								</div>
							</div>								
						</div>

						<div class="blog-sec1-mid">
							<div class="row">									
								<div class="blog-sec1-text">
									<h1>Article Name</h1>
									<ul>
										<li class="blog-li1"><span>By:</span> Author Name</li>
										<li class="blog-li3">Comment</li>
										<li class="blog-li2"><span>Posted on:</span> 14<sup>th</sup> April 2018</li>
										<li class="blog-li4">Like</li>	
									</ul>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.</p>
									<a href="#" class="blog-btn">Read More</a>
								</div>
							</div>								
						</div>	
					</div>

					<div id="menu1" class="tab-pane fade">
						<div class="blog-sec1-mid">
							<div class="row">									
								<div class="blog-sec1-text">
									<h1>Article Name</h1>
									<ul>
										<li class="blog-li1"><span>By:</span> Author Name</li>
										<li class="blog-li3">Comment</li>
										<li class="blog-li2"><span>Posted on:</span> 14<sup>th</sup> April 2018</li>
										<li class="blog-li4">Like</li>	
									</ul>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.</p>
									<a href="#" class="blog-btn">Read More</a>
								</div>
							</div>								
						</div>

						<div class="blog-sec1-mid">
							<div class="row">									
								<div class="blog-sec1-text">
									<h1>Article Name</h1>
									<ul>
										<li class="blog-li1"><span>By:</span> Author Name</li>
										<li class="blog-li3">Comment</li>
										<li class="blog-li2"><span>Posted on:</span> 14<sup>th</sup> April 2018</li>
										<li class="blog-li4">Like</li>	
									</ul>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.</p>
									<a href="#" class="blog-btn">Read More</a>
								</div>
							</div>								
						</div>

						<div class="blog-sec1-mid">
							<div class="row">									
								<div class="blog-sec1-text">
									<h1>Article Name</h1>
									<ul>
										<li class="blog-li1"><span>By:</span> Author Name</li>
										<li class="blog-li3">Comment</li>
										<li class="blog-li2"><span>Posted on:</span> 14<sup>th</sup> April 2018</li>
										<li class="blog-li4">Like</li>	
									</ul>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.</p>
									<a href="#" class="blog-btn">Read More</a>
								</div>
							</div>								
						</div>
					</div>

					<div id="menu2" class="tab-pane fade">
						<div class="blog-sec1-mid">
							<div class="row">									
								<div class="blog-sec1-text">
									<h1>Article Name</h1>
									<ul>
										<li class="blog-li1"><span>By:</span> Author Name</li>
										<li class="blog-li3">Comment</li>
										<li class="blog-li2"><span>Posted on:</span> 14<sup>th</sup> April 2018</li>
										<li class="blog-li4">Like</li>	
									</ul>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.</p>
									<a href="#" class="blog-btn">Read More</a>
								</div>
							</div>								
						</div>

						<div class="blog-sec1-mid">
							<div class="row">									
								<div class="blog-sec1-text">
									<h1>Article Name</h1>
									<ul>
										<li class="blog-li1"><span>By:</span> Author Name</li>
										<li class="blog-li3">Comment</li>
										<li class="blog-li2"><span>Posted on:</span> 14<sup>th</sup> April 2018</li>
										<li class="blog-li4">Like</li>	
									</ul>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.</p>
									<a href="#" class="blog-btn">Read More</a>
								</div>
							</div>								
						</div>

						<div class="blog-sec1-mid">
							<div class="row">									
								<div class="blog-sec1-text">
									<h1>Article Name</h1>
									<ul>
										<li class="blog-li1"><span>By:</span> Author Name</li>
										<li class="blog-li3">Comment</li>
										<li class="blog-li2"><span>Posted on:</span> 14<sup>th</sup> April 2018</li>
										<li class="blog-li4">Like</li>	
									</ul>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.</p>
									<a href="#" class="blog-btn">Read More</a>
								</div>
							</div>								
						</div>
					</div>								
				</div>	
			</div>
		</div>	

		<div class="row blog-pager">
			<ul class="pager">
				<li class="previous"><a href="#">Previous</a></li>
				<li class="next"><a href="#">Next</a></li>
			</ul>
		</div>

	</div>
</section>
<!-- /section1 -->
@endsection