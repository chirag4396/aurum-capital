@extends('users.layouts.master')

@push('header')
<style type="text/css">
.loader{	
	position: fixed;
	background: #f3f3f3;
	width: 100%;
	z-index: 9999;
	top: 0;
	text-align: center;
	height: 100%;
	padding-top: 15%;
	font-size: 20px;
}
</style>
@endpush

@section('content')
<div class="loader hidden">
	<img src="{{ asset('img/logo.png') }}">
	<p>Generating {{ $stock->st_title }} report please wait.</p>
</div>
<section class="current">
	<div class="container">
		<div class="row">
			<h1>{{ $stock->st_title }} Reports <a href = "{{ route('current-recommendation') }}" class="btn btn-success pull-right">Back</a></h1>
			<ul class="nav nav-pills nav-stacked">
				@forelse ($stock->reports()->get() as $report)					
				<li class="report-li">
					<p>{{ $report->sr_title }}</p>
					<div>
						<a href = "javascript:void(0)" onclick="downloadFile(this, '{{ route('download-report', ['id' => $report->sr_id, 'name' => strtolower(str_replace(' ','-',$report->sr_title))]) }}');" class="btn btn-success">Download
						</a>
						<a href="{{ route('view-report', ['id' => $report->sr_id, 'name' => strtolower(str_replace(' ','-',$report->sr_title))]) }}" class="btn btn-success">View
						</a>
					</div>
				</li>
				@empty
				<li>No Report found for {{ $stock->st_title }}</li>
				@endforelse
			</ul>
		</div>
	</div>
</section>
@endsection

@push('footer')
<script type="text/javascript">
	$('li a').on({
		'click' : function(){
			$('.loader').removeClass('hidden');
		}
	});
	function downloadFile(el, route){
	    $.get(route, function (data) {
			$(el).attr('href',data);
            $(el)[0].click();
            $('.loader').addClass('hidden');
        });
	}
</script>
@endpush