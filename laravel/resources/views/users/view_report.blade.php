@extends('users.layouts.master')

@push('header')
<link href="{{ asset('css/jquery.touchPDF.css') }}" rel="stylesheet" media="screen"/>
@endpush

@section('content')
<section class="current">
	<div class="container">
		<div class="row">
			<h1>{{ $report->sr_title }} Reports <a href="{{ route('stock-reports',['id' => $report->stock->st_id, 'name' => strtolower(str_replace(' ', '-', trim($report->stock->st_title)))]) }}" class="btn btn-primary pull-right">Back</a></h1>
			<div class="blog-read-more">
				<div id="myPDF"></div>
			</div>
		</div>
	</div>
</section>
@endsection

@push('footer')
<script type="text/javascript" src="{{ asset('js/pdf/pdf.compatibility.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/pdf/pdf.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/pdf/jquery.touchSwipe.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/pdf/jquery.touchPDF.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/pdf/jquery.panzoom.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/pdf/jquery.mousewheel.js') }}"></script>

<script type="text/javascript">
	$(function() {
		$("#myPDF").pdf({ 
			source: "{{ asset($report->sr_pdf_path) }}"			
		});
	});	
</script>
@endpush