@extends('users.layouts.master')

@section('content')
<section class="services-sec1">
	<div class="container">
		<div class="row">	
			<div class="col-md-8 col-md-offset-2">
				<h2 class="text-center">Payment</h2>
				<form method="post" class="comments-form" action="{{ route('placeorder') }}">
					{{ csrf_field() }}
					<div style="margin-bottom: 10px;">
						<label class="control-label">Service Plans</label>
						<select name="plan" class="form-control" onchange="$('#amount').val(this.value.split('-')[1]);">
							<option value="-1">--Select--</option>
							@forelse ($plans as $k => $p)
							<option value="{{ $p->sp_id.'-'.$p->sp_price }}" {{ (empty($posted['plan'])) ? ($k == 0 ? 'selected' : '') : (($posted['plan'] == $p->sp_id.'-'.$p->sp_price ) ? 'selected' : '') }}>{{ $p->sp_title }}</option>
							@empty								
							@endforelse
						</select>						
					</div>
					<div class="form-group">
						<label>Amount <span class="required">*</span></label>
						<input class="form-control pay-amount" type="number" min = "0" id = "amount" name = "amount" readonly value="{{ (empty($posted['amount'])) ? '25000' : $posted['amount'] }}"/>
					</div>
					<input hidden name="productinfo" value="{{  (empty($posted['productinfo'])) ? 'Aurum Capital Subscription Plan Payment' : $posted['productinfo'] }}" size="64" />

					<input hidden name="surl" value="{{ (empty($posted['surl'])) ? route('orderStatus') : $posted['surl'] }}" size="64" />
					<input hidden name="furl" value="{{  (empty($posted['furl'])) ? route('orderStatus') : $posted['furl']  }}" size="64" />
					<div class="form-group">
						<label>Contact Person Name: <span class="required">*</span></label>	
						<input readonly class="form-control" name="firstname" type="text" id="firstname" value="{{ (empty($posted['firstname'])) ? Auth::user()->name : $posted['firstname'] }}" />
					</div>
					<div class="form-group">
						<label>Mobile Number: <span class="required">*</span></label>
						<input readonly class="form-control" type="text" name="phone" value="{{ (empty($posted['phone'])) ? Auth::user()->mobile : $posted['phone'] }}" />

					</div>
					<div class="form-group">
						<label>Email ID: <span class="required">*</span></label>
						<input readonly class="form-control" name="email" type="text" id="email" value="{{ (empty($posted['email'])) ? Auth::user()->email : $posted['email'] }}" />

					</div>				
					
					<input type="hidden" name="udf1" value="{{ (empty($posted['udf1'])) ? '' : $posted['udf1'] }}" />
					
					<input type="hidden" name="udf2" value="{{ (empty($posted['udf2'])) ? '' : $posted['udf2'] }}" />
					
					
					
					<input type="hidden" name="udf3" value="{{ (empty($posted['udf3'])) ? '' : $posted['udf3'] }}" />
					
					<input type="hidden" name="udf4" value="{{  (empty($posted['udf4'])) ? '' : $posted['udf4'] }}" />
					
					
					
					<input type="hidden" name="udf5" value="{{ (empty($posted['udf5'])) ? '' : $posted['udf5'] }}" />
					<div class="form-group text-center">
						<input class = "btn btn-success" type="submit" value="Pay Now">
					</div>  
				</form>
				
			</div>
		</div>
	</div>
</section>
@endsection
