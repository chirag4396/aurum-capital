@extends('users.layouts.master')

@section('content')
<!-- privacy -->
<section class="privacy-main">
    <div class="container">
        <div class="row">
            <h1>Terms of Use</h1>
            <hr>               
            <ol>
                <li>
                    <span>ACCEPTANCE OF TERMS</span>
                    <p>This Web- Site is offered to the user of the Service (User) on the condition that the User accepts the terms contained herein, without any modification whatsoever. For the purposes of this Terms of Use, 'Service' shall mean and include, without limitation, access to all, one or some of the following: HTML Code, literature, information, software, products, tools, online educational course material, any research or recommendation service whether authored by Aurum Capital or by any third party and published, provided or made available on aurumcapital.in whether pursuant to subscription or otherwise.
                    </p>
                    <p>You understand that by checking the "I have read and accept the Terms of Use" Checkbox, or by using the site (including any content provided therein), or your account, you are agreeing to be bound by these terms of use. If you do not accept these terms of use in their entirety, you may not access or use the site.</p>
                </li>
                <li>
                    <span>INTELLECTUAL PROPERTY RIGHTS (IP)</span>
                    <p>Aurum Capital hereby grants to the User, the right to use the Service in accordance with these terms, and for no other purpose. Please note that all elements contained on this Web-Site including all individual articles, columns and other elements making up the Service are also Copyrighted works and are owned by Aurum Capital and/ or its Suppliers and are protected by Copyright, Service Marks and Trademark laws, International Treaty provisions and/or other proprietary rights under the laws of India and this Terms of Use. By using the service you agree to abide by all applicable Copyright and other laws as well as any additional Copyright notices or restrictions contained in the Service. Aurum Capital or other third parties shall be entitled to obtain equitable relief for any violation of their IP, over and above all other remedies available to it to protect its interests therein. Violators will be liable to be prosecuted to the maximum extent possible. Copying for reproduction, for redistribution or other purpose of the web site or any part thereof including but not limited to text content and graphics to any other server or location including caching of any kind is expressly prohibited. The Service is non-exclusive, non-transferable, non-assignable, non-sub-licensable, revocable license to the User. The Service is not sold to the User. Aurum Capital owns the Service, its applications and Trademarks.
                    </p>
                </li>
                <li>
                    <span>LINKS TO THIRD PARTY SITES</span>
                    <p>This Web- Site may contain links to Web Sites operated by parties other than Aurum Capital. The links in this Site will let the User leave aurumcapital.in and proceed to the linked site. The User's use of each such site is also subject to this Terms of Use and other terms of use, if any, contained within each such site. In the event that any of the Terms contained herein conflict with the terms of use contained within any such site, then the terms of use for such site shall prevail. The linked Sites are not under the control of Aurum Capital and Aurum Capital is not responsible for the contents of any linked Site or any link contained in a linked Site, or any changes or updates to such sites. Aurum Capital will not directly or indirectly be liable for any loss that may arise to the User as a result of his accessing the linked Sites.</p>
                </li>
                <li>
                    <span>ACCESS RESTRICTION/ FEES</span>
                    <p>Aurum Capital reserves the right to deny, in its sole discretion, any User access to this Web Site or any portion thereof without notice or justification. Further Aurum Capital reserves the right at any time to charge fees for access to any service provided by it.</p>
                </li>


                <li>
                    <span>REGISTRATION AND SUBSCRIPTION</span>
                    <p>If you wish to receive access to the restricted editorial content of the Website you will need to register (as a Registered User) or subscribe (as a Subscriber) with us and the following provisions of this Clause will apply.</p>
                    <p>1.1 You agree to:</p> 
                    <p>a) provide true, accurate, correct and complete information which you are required to provide when you register or subscribe as a user of the Website, whether in respect of the Subscription Services or the Free Services ("Personal Information"); and</p>
                    <p>b) notify us immediately of any changes to the Personal Information.</p>
                    <p>c) receive calls, sms or emails regarding our products or services and various promotional offers for solicitation.</p>
                    <p>1.2 You agree not to:</p>
                    <p>a) impersonate any other person or entity or to use a false name or a name that you are not authorised to use, or disclose your password or user name to any other person, or allow your password or user name to be used by any other person to access the Services.
                    </p>
                    <p>1.3 We reserve the right to decline any application from you to register or subscribe as a user of the Subscription Services and/or the Free Services at our sole discretion. If you are accepted you will receive from us a user name and password. If you are allowed to select a user name, we reserve the right to modify such or provide you with a user name of our choice if, in our sole opinion, such user name infringes or violates the rights of any person or third party or is defamatory, offensive or is in any other way improper or inappropriate.
                    </p>
                    <p>1.4 We reserve the right to terminate your account (including user name and password) if any Personal Information is untrue, inaccurate, out-of-date or incomplete.
                    </p>
                    <p>1.5 We reserve the right to temporarily suspend or permanently terminate your account (including user name and password) if you share log in credentials with third parties, your account accessed from multiple locations and/or IP addresses, or any other misuse of the account.</p>
                    <p>You must become a "Registered User" in order to submit User Information and access certain other features of the Site, which requires that you create an account on aurumcapital.in. To become a Registered User you must be at least 13 years old. Any registration by, use of or access to the Site by anyone under age 13 is unauthorized, unlicensed and in violation of these Terms of Use. By using the Site, you represent and warrant that you are at least 13 years old. When you register, you may be asked to choose a user name and password. Aurum Capital has physical, electronic and procedural safeguards that comply with federal standards to guard users' non-public personal information. You are responsible for safeguarding your passwords and you agree not to disclose your passwords to any third party. You agree that you shall be solely responsible for any activities or actions under your password, whether or not you have authorized such activities or actions. You shall immediately notify Aurum Capital of any unauthorized use of your password. You agree that the information that you provide to us on registration and at all other times will be true, accurate, current, and complete.</p>

                </li>
                <p>Please click here to read our detailed "<a href="{{ route('privacy') }}">Privacy Policy</a>".</p>
                <li>
                    <span>YOUR OBLIGATIONS</span>
                    <p>1 You:</p>
                    <p>1.1 agree not to use the website(s) and/or the services (or any part thereof) including any discussion Forums for any illegal purpose and agree to use it in accordance with all relevant laws;</p>
                    <p>1.2 agree not to upload or transmit through the website(s) and/or the services any computer viruses, macro viruses, trojan horses, worms or anything else designed to interfere with, interrupt or disrupt the normal operating procedures of a computer;</p>
                    <p>1.3 will not upload or transmit through the website(s) and/or the services any material which is defamatory, offensive, or of an obscene or menacing character, or that may cause annoyance, inconvenience or needless anxiety;</p>
                    <p>1.4 will not use the website(s) and/or the services in a way that may cause the website(s) and/or the services to be interrupted, damaged, rendered less efficient or such that the effectiveness or functionality of the website(s) and/or the services is in any way impaired;</p>
                    <p>1.5 will not use the website(s) and/or the services including any Discussion Forums in any manner which violates or infringes the rights of any person, firm or company (including, but not limited to, rights of intellectual property, rights of confidentiality or rights of privacy);</p>
                    <p>1.6 will not attempt any unauthorised access to any part or component of the website(s) and/or the services;</p>
                    <p>1.7 agree that in the event that you have any right, claim or action against any Users arising out of that User's use of the website(s) and/or the services, then you will pursue such right, claim or action independently of, and without recourse to us; and agree not to allow any third party directly or indirectly to use your subscription, user name or password.</p>
                    <p>1.8 Agree not to directly or indirectly share your subscription, user name or password or any other credentials with any third party or parties or persons.</p>
                </li>
                <li>
                    <span>LIMITS ON PERSONAL AND NON-COMMERCIAL USE</span>
                    <p>The User expressly agrees to use the Service strictly for personal purpose. The User shall not recompile, disassemble, copy, modify, distribute, transmit, display, perform, reproduce, publish or create derivative works from, transfer, or sell any information, software, products, tools or services accessed from this web site. The User may not rent, lease, sell, sublicense, lend or in any manner allow any other party to use the Service, with or without consideration. By viewing the contents of this website you agree this condition of viewing and you acknowledge that any unauthorized use is unlawful and may subject you to civil or criminal penalties.
                    </p>
                </li>
                <li>
                    <span>PRIVACY</span>
                    <p>The User represents that he is aware that in the process of subscribing to the Service or parts of it, Aurum Capital may obtain information relating to the User, including that of a confidential nature. This information will be used by Aurum Capital for its internal purposes and will be kept confidential. Notwithstanding anything contained above, Aurum Capital reserves the right to disclose personal information where it believes in good faith that such disclosure is required by law, to perform necessary credit checks or collect or report debts owed to Aurum Capital, to protect Aurum Capital's rights or property or for other bonafide uses. The User agrees and warrants that all information that Aurum Capital may possess or may obtain pursuant to the use of Service has been possessed or obtained with the permission of the User.
                    </p>Please click here to read our detailed "<a href="{{ route('privacy') }}">Privacy Policy</a>".</p>
                </li>


                <li>
                    <span>HYPERLINKING TO SITE AND REFERENCING SITE PROHIBITED</span>
                    <p>Unless expressly authorized by website, no one may hyperlink this site, or portions thereof, (including, but not limited to, logotypes, trademarks, branding or copyrighted material) to theirs for any reason. Further, you are not allowed to reference the url (aurumcapital.in) in any commercial or non-commercial media without express permission from Aurum Capital. You specifically agree to cooperate with the Website to remove or de-activate any such activities and be liable for all damages
                    </p>
                </li>

                <li>
                    <span>TERMS RELATED TO ONLINE PAYMENTS
                    </span>
                    <p><b>Online Card Payments:</b></p>
                    <p>Credit/Debit Card payments are processed through online payment gateway system that Aurum Capital may use from time to time. By using a credit/debit card to pay for our services, you confirm that the card being used is yours. All credit/debit card holders are subject to validation checks and authorisation by the card issuer. The Card information provided will be authenticated by your bank through the payment gateway system directly without any information being stored at our website. In approximately 25-30 seconds (depending on your internet connection) your bank will issue, using the online payment gateway that Aurum Capital may be using, an authorization code and confirmation of completion of transaction. The information provided is used by the payment gateway system that Aurum Capital may be using to process the payment.
                    </p>
                    <p>Please note Aurum Capital does not save or store your credit card information on any of its servers. This information is passed on to our payment gateway for processing.
                    </p>
                    <p>Aurum Capital uses the reasonable security measures to protect your card information. Your account will be activated only after the successful completion of the transaction with reasonable security measures to protect card data.
                    </p>

                    <p><b>Internet Banking/UPI</b></p>
                    <p>If you have an account with any of the banks made available for making payment by online payment gateway system that Aurum Capital may be using, then you can pay through respective bank's net banking options or UPI account and the amount will be automatically debited from your account through an online gateway system, which enables safe and secure transaction.</p>
                    <p><b>Transaction Confirmation</b></p>
                    <p>Once your transaction has been completed, you will receive a confirmation of your transaction from Aurum Capital. If you do not receive a confirmation of your transaction, first look into your "spam" or "junk" folder to verify that it has not been misdirected, and if still not found, please contact us at support@aurumcapital.in
                    </p>
                    <p><b>Advance Notification for Subsequent Payment</b></p>
                    <p>After the initial payment made by you, Aurum Capital would, on a best effort basis send an advance notification mail every time your subscription fees become due and payable.
                    </p>
                    <p><b> 
                        Modification/Cancellation of Credit Card facility
                    </b></p>
                    <p>You may request for cancellation of auto debit/Standing Instruction facility by giving 30 days notice or such period as may be specified in a particular offer. The notice should be sent to Aurum Capital in writing or via email and the same will be effected within a minimum period of 7 days of the receipt of the request. The Company will not be responsible for any delays, which are beyond its control.</p>
                    <p>Aurum Capital shall not be responsible for any fraudulent or disputed transactions. In case of a dispute regarding the transaction through your credit card the same must be resolved by you directly with your Credit Card issuer. You will not make Aurum Capital a party to any such dispute and Aurum Capital shall not be liable to you for any consequence arising out of the fraudulent or disputed transactions. This facility is available on such credit cards as intimated by Aurum Capital from time to time.
                    </p>
                    <p>No extra cost will be charged to you for this facility. In the event you are dissatisfied with the auto debit/ Standing Instruction facility being made available in any respect or with any of the terms of service or alterations thereto, your sole and exclusive remedy is to discontinue the use of the Facility.</p>
                    <p><b> 
                        Disclaimer
                    </b></p>
                    <p>You agree that the credit/ debit card information are of sensitive nature and are aware of the risks involved in making payment through the internet. You shall be responsible for safe keeping the credit/ debit card information and shall not share it with any other person.</p>
                    <p>Aurum Capital will not be liable for any loss or damage caused by the misuse of such information due to your negligence or default.
                    </p>

                    <p><b>Aurum Capital as a merchant shall be under no liability whatsoever in respect of any loss or damage arising directly or indirectly out of the decline of authorization for any transaction, on account of your having exceeded the preset limit mutually agreed by us with our acquiring bank from time to time.</b></p>
                </li>
                <li>
                    <span>MODIFICATION OF THESE TERMS OF USE, PRIVACY POLICY AND DISCLAIMER</span>
                    <p>Aurum Capital reserves the right to change, without notice, this Terms of Use, Privacy Policy and Disclaimer under which the Service is offered. The User's continued use of the Service will be subject to the Terms of Use in force at the time of the Use.</p>
                    <p>You agree to review these terms of use, Privacy Policy and Disclaimer periodically since subsequent use by you of this site shall constitute your acceptance of any changes. Aurum Capital shall have the right at any time to change or discontinue any aspect of aurumcapital.in, including, but not limited to, the community areas, content, hours of availability and equipment needed for access to use. Such changes, modifications, additions or deletions shall be effective immediately upon posting and any subsequent use by you after such posting shall conclusively be deemed to be acceptance by you of such changes, modifications or deletions.</p>
                </li>


                <li>
                    <span>INDEMNIFICATION</span>

                    <p>YThe User agrees to indemnify, defend and hold harmless Aurum Capital, its officers, partners, directors, employees, representatives and agents, any third party providers, distributors from and against any cause of action, claim or demand, including without limitation any reasonable legal accounting or other professional fees, brought by or on the user's behalf in excess of the liability described herein or by / on account of a third party due to or arising out of User's use of this Web site, the Service contained herein, the violation of any intellectual property or any other right of any person or entity.
                    </p>
                </li>
                <li>
                    <span>DISCLAIMER OF WARRANTIES</span>

                    <p>Aurum Capital's research, recommendation and educational services published or provided by Aurum Capital and authored by Aurum Capital or a third party are general recommendation/educational services and are not to be construed as individual investor specific Portfolio Management and Advisory Service. Aurum Capital is not registered as such under the SEBI (Portfolio Managers) Regulations, 1993. Aurum Capital is neither a university nor an educational institution and is not affiliated with any university or educational institution and will not grant a degree or any other certificate that is recognized by the Central Government, the State Government or any other bodies set up by the Government in India, including the Securities and Exchange Board of India. The course/training material provided by Aurum Capital on its website aurumcapital.in, through email, classroom sessions have not been certified by any university or other educational institution or other organization qualified to provide certification.</p>
                    <p>
                    The User warrants that he/she/it shall obtain independent investment advice before making any Investment based on such recommendation.</p>
                    <p>The recommendation service, views, articles, reports, educational course material and other contents are provided on an "As Is" basis by Aurum Capital. Use of the Service is at any persons, including a Subscriber's/Client's, own risk. Information herein is believed to be reliable but Aurum Capital does not warrant its completeness or accuracy and shall not be responsible or liable for any losses incurred by a user for acting based on the views expressed in this service.</p>
                    <p>Aurum Capital expressly disclaims all warranties and conditions of any kind, whether express, implied or statutory pertaining to the Services provided by it including but not limited to implied warranties and conditions of merchantability, fitness for a particular purpose, data accuracy and completeness and any warranties relating to viruses and non-infringement in the Service and provision of service free from disruption or interruption.</p>
                    <p>The Service should not be construed to be an advertisement for solicitation for buying or selling of any securities or assets. Aurum Capital provides research recommendations on securities listed on Indian securities exchanges only. Aurum Capital shall not be responsible (directly or indirectly) for use or otherwise of Service by a User, including a User who is a citizen or resident or located in any locality, state, country or other jurisdiction outside India specifically a resident of the United States of America or Canada and we hereby expressly disclaim any implied warranties imputed by the laws of any other jurisdiction. We consider ourselves and intend to be subject to the jurisdiction only of the Courts in Pune, India. If you don't agree with above please do not read the material on any of our pages.</p>
                    <p>Our investment recommendations are general in nature and available electronically to all kind of investors irrespective of subscribers' investment objectives and financial situation/risk profile. Aurum Capital shall not be responsible for any direct/indirect loss or liability incurred to the user as a consequence of his or any other person on his behalf taking any investment decisions based on the information, recommendations, research reports, analysis, quotes, educational course material etc. provided on the web site.</p>

                    <p>Aurum Capital shall also not be liable for errors, omissions or typographical errors, disruption delay, interruption, failure, deletion or defect of/in the Service provided by it.</p>

                    <p>Aurum Capital does not warrant accuracy of any feed from the stock exchanges/ currency/commodities exchanges or any other third party.</p>

                    <p>Aurum Capital shall not be liable, directly or indirectly, to the User or any third party, as a consequence of the failure of its equipment, howsoever defined, or that of any Stock exchange/ currency/commodities exchanges, Internet Service Provider, User or any third party to function in such manner as is reasonably expected of such equipment. Aurum Capital shall not be responsible for any downtime of such equipment.</p>

                    <p>Where Aurum Capital offers to send alerts to the Subscriber through SMS, Aurum Capital shall not be liable to pay or bear any cost, charges, fee charged by a service provider to the Subscriber for receipt of such SMS. Aurum Capital may at any time without notice withdraw the facility of sending SMS alerts to the Subscribers. In case Subscriber does not want to receive SMS alerts from Aurum Capital, he/she may send a written intimation to Aurum Capital on the same.</p>

                    <p>Where Aurum Capital offers to send One Time Password (OTP) to the Subscriber through SMS for log-in purpose on the website, Aurum Capital shall not be liable to pay or bear any cost, charges, fee charged by a service provider to the Subscriber for receipt of such SMS. Aurum Capital may at any time without notice withdraw the facility of sending SMS OTP to the Subscribers.</p>

                    <p>Where Aurum Capital offers to send One Time Password (OTP) to the Subscriber through email for log-in purpose on the website, Aurum Capital shall not be liable to pay or bear any cost, charges, fee charged by a service provider to the Subscriber for receipt of such email. Aurum Capital may at any time without notice withdraw the facility of sending email OTP to the Subscribers.</p> 

                    <p>Aurum Capital makes no representations or warranties, either express or implied that the research, development, marketing, distribution, use or sale of the Service will not infringe any patent, copyright or other right of any third party.</p>

                </li>
                <li>
                    <span>DISCLAIMER FOR HARM CAUSED TO YOUR COMPUTER FROM INTERACTING WITH THIS WEBSITE</span>
                    <p>The website assumes no responsibility for damage to computers or software of the visitor or any person the visitor subsequently communicates with from corrupting code or data that is inadvertently passed to the visitor's computer. Again, visitor views and interacts with this site, or banners or pop-ups or advertising displayed thereon, at his own risk.</p>
                </li>
                <li>
                    <span>POSTING COMMENTS</span>
                    <p>For posting comments on Website/Investor Forum</p>
                    <p>1. The views, opinions and comments posted are yours, and are not endorsed by Aurum Capital. You shall be solely responsible for the comment posted. Comments posted on Investor Forum may be moderated and Aurum Capital reserves the right to edit, modify, delete, reject, or otherwise remove any views, opinions and comments posted or part thereof without notice.</p>
                    <p>2. You shall ensure that your comment is not inflammatory, abusive, derogatory, defamatory &/or obscene, or contain pornographic matter and/or does not constitute hate mail, or violate privacy of any person (s) or breach confidentiality or otherwise is illegal, immoral or contrary to public policy. Nor should it contain anything infringing copyright &/or intellectual property rights of any person(s).</p>
                    <p>3. User Comments are made available to you for your information and personal non-commercial use solely as intended through the normal functionality of the Aurum Capital Service. User Comments shall not be used, downloaded (either permanently or temporarily) copied, reproduced, distributed, transmitted, broadcast, displayed, sold, licensed, downloaded, or otherwise commercially exploited in any manner.</p>
                    <p>4. For signup to the aurumcapital.in using your social account:<br/>
                        You authorize Aurum Capital to access information which is made available by the social platform.<br/>
                    You can delink your social account from the aurumcapital.in at any time.</p>
                    <p>5. You agree that Investor Forum is not a platform to solicit, discuss or seek any ideas, views, opinions on specific stock/derivatives.</p>
                    <p>6. You shall not conduct or forward surveys, contests, or chain letters or post any commercial content.</p>
                    <p>7. Use of Investor Forum is at your own risk. Aurum Capital does not guarantee the accuracy, integrity or quality of any content posted on Investor Forum. Aurum Capital assumes no responsibility or liability for the deletion or failure to store, inaccuracy in transmission, use of any data, content posted, uploaded on Investor Forum.</p>
                    <p>8. You may not send, submit, post, or otherwise transmit, messages or material that contain software virus, that are designed to interrupt, destroy, and negatively affect in any manner whatsoever, any electronic equipment / software / coding in connection with the use of this portal.</p>
                    <p>9. In the event that you breach any of the Terms of Use, Aurum Capital shall have the right to delete any material relating to the violations without notice. Aurum Capital also reserve the right to ban anyone who wilfully violates these Terms of Use. Inobservance of these terms and conditions may lead to an immediate warning, an infraction and deletion of the concerned post. Repeated offences shall lead to the subscriber being permanently banned from Investor Forum or aurumcapital.in.</p>
                    <p>10. Aurum Capital may place reasonable restrictions on the maximum number of days that messages will be retained, the maximum size of any message that may be sent from or received by a User.</p>
                    <p>11. Aurum Capital reserves the full right to edit or remove any post or prohibit any user from posting any messages on Investor Forum at any time. The determination of what is construed as indecent, vulgar, spam, improper, irrelevant, etc. as noted in these points is up to Aurum Capital. Intimation of deletion may not be communicated every time.</p>
                    <p>12. Aurum Capital on a best effort basis will not share any personal sensitive information to any other person or entity.</p>
                    <p>13. By submitting messages on Investor Forum you agree to indemnify and hold harmless Aurum Capital from all claims, costs and expenses (including legal expenses) arising out of any messages posted or published by you that are in breach of these Terms of Use.</p>
                </li>
                <li>
                    <span>LIMITATION OF LIABILITY</span>
                    <p>The User agrees that neither Aurum Capital nor its partners or employees or affiliates shall be liable for any direct, indirect, incidental, special or consequential damages, resulting from the use/delivery/performance or the inability to use/deliver/perform the Service or for cost of procurement of substitute goods and repair & correction services or resulting from the Services subscribed to or obtained or messages received or transactions entered into through or of User's transmissions or data, even if Aurum Capital or its employees have been advised of the possibility of such damages.</p>
                    <p>The User further agrees that Aurum Capital shall not be liable for any damages arising from interruption, suspension or termination of Service, whether such interruption, suspension or termination was justified or not, negligent or intentional, inadvertent or advertent.</p>
                    <p>The User also agrees that his sole remedy under this Terms of Use is cancellation of the Service. By viewing, using, or interacting in any manner with this site, including banners, advertising, or pop-ups, and as a condition of the website to allow his lawful viewing, Visitor forever waives all right to claims of damage of any and all description based on any causal factor resulting in any possible harm, no matter how heinous or extensive, whether physical or emotional, foreseeable or unforeseeable, whether personal or business in nature.</p>
                </li>
                <li>
                    <span>REFUND POLICY</span>
                    <p>a) Your subscription will be enabled immediately on competition of KYC and Risk Profiling post receipt of your payment. </p>
                    <p>b) Your subscription starts from the date of the payment and not from the date of your competition of regulatory formalities.</p>
                    <p>c) In case your subscription is suspended due to any reasons, including but not limited to violation of terms of use, privacy policy and in the event of your suspension revoked, there will not be any extension of period for loss due to suspension.</p>
                    <p>d) The Subscriber will not be eligible for any refund if the subscriber is in breach of these Terms of Use.</p>
                    <p>f) The Subscriber will be eligible for a refund only if Aurum Capital decides to close down the services before the completion of the subscription period of the subscriber. The refund will be on a pro-rata basis for the remaining period for which services are not to be provided due to closure of services after adjustment of any discounts given and taxes paid.</p>
                </li>
                <li>
                    <span>USE OF PASSWORD</span>
                    <p>User shall be responsible for creating and safe keeping of Password. User shall be responsible for periodically changing the password. In the event a User is given a password by Aurum Capital, the User shall ensure that such password is kept in a secure manner. User may not assign his/her password obtained to any person or entity without the prior written consent of Aurum Capital. Notwithstanding anything contained herein, in the event any liability arising to the User as a consequence of misuse of the password shall be borne by the User. We may investigate any reported violation of these Conditions or complaints and take any action that we deem appropriate (which may include, but is not limited to, issuing warnings, suspending, terminating or attaching conditions to your access and/or removing any materials on the website(s) and/or the services).</p>
                </li>
                <li>
                    <span>SUBMISSIONS</span>
                    <p>Visitor agrees as a condition of viewing, that any communication between Visitor and Website is deemed a submission. All submissions, including portions thereof, graphics contained thereon, or any of the content of the submission, shall become the exclusive property of the Website and may be used, without further permission, for commercial use without additional consideration of any kind.</p>
                </li>
                <li>
                    <span>MONITORING</span>
                    <p>We have the right, but not the obligation, to monitor any activity and content associated with the website(s) and/or the services. The report on stocks will have Subscriber details watermarked. The Subscriber shall not be copying, downloading, screen capturing or accessing in any other unauthorized manner. The reports are meant only for the Subscriber. We may investigate any reported violation of these Conditions or complaints and take any action that we deem appropriate (which may include, but is not limited to, issuing warnings, suspending, terminating or attaching conditions to your access and/or removing any materials on the website(s) and/or the services).</p>
                </li>
                <li>
                    <span>GOVERNING LAW AND JURISDICTION</span>
                    <p>This Terms of Use is governed by the laws of the Republic of India. The User hereby consents and submits to the exclusive jurisdiction and venue of Courts in Pune, India in all disputes arising out of or relating to the use of this website or Service.
                    </p>
                </li>
                <li>
                    <span>GENERAL</span>
                    <p>Nothing contained in this Terms of Use is in derogation of Aurum Capital's right to comply with Governmental, Court and Law enforcement requests or requirements relating to use of this website, Service or information provided to or gathered by Aurum Capital with respect to such use.</p>
                    <p>This Terms of Use constitutes the entire agreement between the User and Aurum Capital with respect to this Web site and the Services provided by Aurum Capital.</p>
                </li>
                <li>
                    <span>FOREIGN JURISDICTIONS</span>
                    <p>All Users of the Service in countries other than India understand that by using the Service, they may be violating the local laws in such countries. If the User chooses to access the Service from outside India, especially the United States of America or Canada, he shall be responsible for compliance with foreign and local laws. The Users agree that they will solely be liable for any liability incurred by them in this regard and will indemnify Aurum Capital for any liability Aurum Capital may incur in any foreign jurisdiction as a consequence of citizens / residents of countries other than India using the Service. This service does not constitute an offer to sell or a solicitation to any person in any jurisdiction where it is unlawful to make such an offer or solicitation.</p>
                </li>
                <li>
                    <span>TERMINATION</span>
                    <p>If the User violates any term of this Terms of Use, Aurum Capital may terminate the User's access to this Web -Site, without waiving any other rights. The User would continue to remain liable for any liabilities that may have arisen before the termination.</p>
                    <p>In addition to the general restrictions above, the following specific restrictions and conditions apply to your use of content on the Aurum Capital Website.</p>
                    <p>The content on the Aurum Capital Website, except all User Submissions (as defined below), including without limitation, the text, software, scripts, graphics, photos, sounds, music, videos, interactive features, online educational course material and the like ("Content") and the trademarks, service marks and logos contained therein ("Marks"), are owned by or licensed to Aurum Capital, subject to copyright and other intellectual property rights under the law. Content on the Website is provided to you AS IS for your information and personal use only and shall not be downloaded, copied, reproduced, distributed, transmitted, broadcast, displayed, sold, licensed, or otherwise exploited for any other purposes whatsoever without the prior written consent of the Aurum Capital. Aurum Capital reserves all rights not expressly granted in and to the Website and the Content.</p>
                    <p>You may access these videos or other information solely:</p>
                    <p>a) for your information and personal non-commercial use;</p>
                    <p>b) as intended through and permitted by the normal functionality of the Aurum Capital Service; and</p>
                    <p>c) for Streaming.</p>
                    <p>"Streaming" means a contemporaneous digital transmission of an audiovisual work via the Internet from the Aurum Capital Service to a user's device in such a manner that the data is intended for real-time viewing and not intended to be downloaded (either permanently or temporarily) copied, stored, permanently downloaded, or redistributed by the user. Accessing Videos for any purpose or in any manner other than Streaming is expressly prohibited. Videos are made available "as is."</p>
                    <p>d) You may access Aurum Capital Content and other content only as permitted under these terms. Aurum Capital reserves all rights not expressly granted in and to the Aurum Capital Content and the Aurum Capital Service.</p>
                    <p>e) You agree to not engage in the use, copying, or distribution of any of the Content of the Website for any commercial purposes.</p>
                    <p>f) You agree not to circumvent, disable or otherwise interfere with security-related features of the Aurum Capital Website or features that prevent or restrict use or copying of any Content or enforce limitations on use of the Aurum Capital Website or the Content therein</p>
                </li>
                <li>
                    <span>SUBCRIPTION CHARGES AND CONDITIONS</span>
                    <p>a) This service or rights to access the Website cannot be transferred or gifted during the life time of the Subscriber. The Subscriber however may bequeath the subscription to any one person on death by way of a will. The subscription will in such cases only last during the life time of that one successor.</p>
                    <p>b) The subscription will last during the life time of the Subscriber or the successor after paying the subscription charges.</p>
                    <p>c) No request for change in the composition of the service will be entertained / accepted by Aurum Capital.</p>
                    <p>d) The subscription fee will be payable annually or as and when the paid subscription period ends. Aurum Capital may decide to increase/decrease or in any way modify the charges applicable to this service. The Subscriber agrees to be bound by them.</p>
                    <p>e) If the subscriber does not pay the annual maintenance fee, his access will be suspended. The service can be reactivated any time by paying the subscription fee for the current year.</p>
                    <p>f) In case the subscription is discontinued for any reason whatsoever, the Subscriber will not be eligible to access any content on website or updates over email including reports/portfolio tracker/investor forum</p>
                    <p>g) Aurum Capital shall not be liable in damages for any delay or default in performance hereunder or any direct/indirect loss if such delay or default or loss is caused by winding up, transfer, assignment of Aurum Capital's business or due to conditions beyond its control including, but not limited to Acts of God, Government restrictions, strikes, wars, insurrections and/or any other cause beyond the reasonable control of Aurum Capital.</p>
                    <p>h) Aurum Capital may at its discretion and from time to time, change, add, remove features of the composition of the services without notice.</p>
                </li>
                <li>
                    <span>RESTRICTIONS</span> 
                    <p>You agree you will not do any of the following while using or accessing the aurumcapital.in (Web Site) or any content thereon:</p>
                    <p>a) Circumvent, disable, or otherwise interfere with security-related features of the Web Site or features that prevent or restrict use or copying of any Content or User Information;</p>
                    <p>b) Upload, email, transmit, provide, or otherwise make available (i) any User Information which you do not have the lawful right to copy, transmit, display, or make available (including any User Information that would violate any confidentiality or fiduciary obligations that you might have with respect to the User Information); or (ii) any User Information that infringes the intellectual property rights of, or violates the privacy rights of, any third party (including without limitation copyright, trademark, patent, trade secret, or other intellectual property right, moral right, or right of publicity);</p>
                    <p>c) Use any meta tags or other hidden text or metadata utilizing a Aurum Capital name, trademark, URL or product name;</p>
                    <p>d) Upload, email, or otherwise transmit any unsolicited or unauthorized advertising, promotional materials, junk mail, spam, chain letters, pyramid schemes, or any other form of solicitation;</p>
                    <p>e) Forge any TCP/IP packet header or any part of the header information in any posting, or in any way use the Web Site to send altered, deceptive, or false source-identifying information;</p>
                    <p>f) Upload, email, transmit, provide, or otherwise make available, via the Web Site, any User Information that is unlawful, obscene, harmful, threatening, harassing, defamatory, or hateful, or that contain objects or symbols of hate, invade the privacy of any third party, contain nudity, are deceptive, threatening, abusive, inciting of unlawful action, or are otherwise objectionable in the sole discretion of Aurum Capital;</p>
                    <p>g) Upload, email, transmit, provide, or otherwise make available any User Information that contains software viruses or any other computer code, files, or programs designed to</p>
                    <p>(i) Interrupt, destroy, or limit the functionality of any computer software; or</p>
                    <p>(ii) Interfere with the access of any user, host or network, including without limitation overloading, flooding, spamming, mail-bombing, or sending a virus to the Web Site;</p>
                    <p>h) Upload, email, transmit, provide, or otherwise make available any User Information that includes code that is hidden or otherwise surreptitiously contained within the User Information that is unrelated to the immediate, aesthetic nature of the User Information;</p>
                    <p>i) Interfere with or disrupt (or attempt to interfere with or disrupt) any web page available at the Web Site, servers, or networks connected to the Web Site, or the technical delivery systems of Aurum Capital's providers, or disobey any requirements, procedures, policies, or regulations of networks connected to the Web Site;</p>
                    <p>j) Attempt to probe, scan, or test the vulnerability of any Aurum Capital system or network or breach or impair or circumvent any security or authentication measures protecting the Web Site;</p>
                    <p>k) Attempt to decipher, decompile, disassemble, or reverse-engineer any of the software used to provide the Web Site;</p>
                    <p>l) Attempt to access, search, or meta-search the Web Site or content thereon with any engine, software, tool, agent, device, or mechanism other than software and/or search agents provided by Aurum Capital or other generally available third-party web browsers, including without limitation any software that sends queries to the Web Site to determine how a Web Site or web page ranks;</p>
                    <p>m) Violate the terms of service or any other rule or agreement applicable to you or Aurum Capital through the Web Site's inclusion in, reference to, or relationship with any third party or third-party site or service, or your use of any such third-party site or service;</p>
                    <p>n) Collect or store personal data about other users without their express permission;</p>
                    <p>o) Impersonate or misrepresent your affiliation with any person or entity, through pretexting or some other form of social engineering, or commit fraud;</p>
                    <p>p) Solicit any user for any investment or other commercial or promotional transaction;</p>
                    <p>q) Violate any applicable law, regulation, or ordinance;</p>
                    <p>r) Use, launch, or permit to be used any automated system, including without limitation "robots," "crawlers," or "spiders"; or</p>
                    <p>s) Use the Web Site or content thereon in any manner not permitted by this Agreement.</p>
                </li>
                <li>
                    <span>CONTACTING US</span>
                    <p>We aim to keep our information about you as accurate as possible. If you would like to review or change the details you have supplied us with, please contact us as set out below. If you wish to change or delete any of the personal information you have entered whilst visiting our website or if you have any questions about our privacy statement kindly e-mail at support@aurumcapital.in</p>

                    <p>Your feedback is welcome and encouraged. You may submit feedback by emailing us at support@aurumcapital.in You agree, however, that</p>


                    <p>a) By submitting unsolicited ideas to Aurum Capital or any of its employees or representatives, by any medium including but not limited to email, written, or oral communication, you automatically forfeit your right to any intellectual property rights in such ideas; and </p>
                    <p>b) Such unsolicited ideas automatically become the property of Aurum Capital. You hereby assign and agree to assign all rights, title, and interest you have in such feedback and ideas to Aurum Capital together with all intellectual property rights therein. In addition, you warrant that all moral rights in any Feedback have been waived, and you do hereby waive any such moral rights. If you have questions about these Terms of Use or about the Site or content thereon, please contact Aurum Capital at support@aurumcapital.in</p>
                </li>


            </ol>
        </div>
    </div>
</section>
<!-- /privacy -->
@endsection