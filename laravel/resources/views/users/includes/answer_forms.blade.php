<form id = "answerForm{{ $if->if_id }}">
    <input type="hidden" name="user" value="{{ Auth::id() }}">
    <input type="hidden" name="question" value="{{ $if->if_id }}">
    <input type="hidden" name="answer_on" value="0">
    <div class="form-group">
        <textarea class="form-control" placeholder="Your Comment" rows="3" name = "answer"></textarea>
    </div>
    <div class="form-group">
        <input type="submit" value="Add Comment" class="investor-btn">
    </div>
</form>

@push('footer')
    <script>
        answer('answerForm{{ $if->if_id }}');
    </script>
@endpush