@extends('users.layouts.master')
@push('header')
@php
$ID = "kyc";
@endphp
<style type="text/css">
	#imagePreview{
		width: 200px;
		object-fit: contain;
	}
</style>
@endpush
@section('content')
<section class="kyc">
	<div class="container">
		<div class="row">
			<h1><i class="glyphicon glyphicon-flag"></i> KYC Details</h1>
			<hr>			
			@php
			$successKyc = Auth::user()->kycDetail()->where(['kyc_status' => 1])->orderBy('kyc_created_at', 'desc')->first();
			$rejectedKyc = Auth::user()->kycDetail()->where(['kyc_reject' => 1])->orderBy('kyc_created_at', 'desc')->first();
			@endphp
			@if (!$successKyc)
				@if ($rejectedKyc)
				<div class="alert alert-danger text-center">You previous PAN Card verification was rejected, Please upload updated copy.</div>
				@endif
			@endif
			@php
			$newKyc = Auth::user()->kycDetail()->orderBy('kyc_created_at', 'desc')->first();
			@endphp
			@if (Auth::user()->kyc)
			<div class="text-center">
				@if ($successKyc)
				@if ($successKyc->kyc_status)
				<h3>Your PAN "{{ $successKyc->kyc_number }}" is verified!!</h3>
				@endif
				@else
				<h3>We got your PAN Card details, once we approve it, we'll let you know!!</h3>
				@endif
			</div>
			@else			
			<p>As per SEBI Investment Advisors Regulation 2014, we are required to do KYC compliance for all our clients.  We need your pancard number and a self-attested scanned copy of the pancard.</p>
			<div class="col-md-6 col-md-offset-3">
				<form id="{{ $ID }}Form"> 
					<div class="form-group"> 
						<label>Please enter your PAN Number </label> 
						<input class="form-control" name="number" type="text" required>
					</div> 
					<div class="form-group"> 
						<label>Attach Your PAN Card Photo / Scanned Copy </label>						
						<img src="{{ asset('img/no-image.png') }}" width="300" height="150" id = "imagePreview">
						<div class="clearfix"></div>
						<input type = "file" id ="image" accept="image/png, image/jpeg, image/jpg" class="hidden form-control1" name = "image">		
						<label class="btn btn-success" for = "image">Select Image</label>
					</div> 
					<button type="submit" class="btn btn-default btn-primary kyc-submit">Submit</button>
				</form>
				<div class="clearfix"></div>				
			</div>
			@endif
		</div>
	</div>
</section>
@endsection

@push('footer')
<script type="text/javascript">
	$('#{{ $ID }}Form').CRUD({
		url : '{{ route('admin.'.$ID.'.store') }}',
		processResponse: function(data){
			if(data.msg == 'success'){
				window.setTimeout(function(){
					location.href = data.location;
				},2000);
			}
		}
	});

	imageUpload('image');
</script>
@endpush