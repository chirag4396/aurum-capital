@extends('users.layouts.master')

@section('content')
<!-- contact -->
<section class="contact-sec1">
	<div class="container">
		<div class="row">
			<div class="col-md-7 contact-mid2">
				<div class="row">
					<div class="contact-map">
						<iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d3784.1176235704493!2d73.87134281516168!3d18.478330537433582!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1s408%2C+Ganga+Collidium+1%2C+Dhanganga%2C+Gangadham+Phase+1%2C+614+Marketyard%2C+Bibwewadi!5e0!3m2!1sen!2sin!4v1526709061480" width="100%" height="550" frameborder="0" style="border:0" allowfullscreen>
						</iframe>
					</div>
				</div>

				<div class="contact-bottom">
					<h4>Get connected</h4>							
					<div class="contact-icon">
						<a href="https://www.facebook.com/AurumCapital.in"><i class="fa fa-facebook"></i></a>
						<a href="https://twitter.com/capitalaurum"><i class="fa fa-twitter"></i></a>
						<a href="https://www.linkedin.com/in/aurumcapital"><i class="fa fa-linkedin"></i></a>
					</div>
				</div>
			</div>

			<div class="col-md-5 contact-mid2 contact-mid1">
				<h4>Address</h4>
				<div class="cont-info">
					<p class="footer-para1">408, Ganga Collidium 1, Dhanganga, Gangadham Phase 1,
						614 Marketyard, Bibwewadi,
						Pune 411037 India
					</p>
					<!--p class="footer-para2">+91 9763301771</p-->
					<p class="footer-para3">
						<a href="mailto:info@aurumcapital.in">info@aurumcapital.in</a>
					</p>
				</div>

				<div id = "contactBox">		
				<h4>Send Your Enquiry</h4>											
					<form id = "contactForm">
						<label>Name</label>
						<input type="text" name="name" data-validate = "empty|alphaSpace">
						<label>Email</label>
						<input type="email" name="email" data-validate = "empty|email">
						<label>Mobile</label>
						<input type="number" name="mobile" data-validate = "empty|digits">
						<label>Message</label>
						<textarea  name="query" data-validate = "empty"></textarea>
						{!! Captcha::display() !!}
						<input type="submit" value="Send Message">
					</form>
				</div>

			</div>	
		</div>				
	</div>
</section>
<!-- contact -->
@endsection

@push('footer')
	<script type="text/javascript">
		$('#contactForm').CRUD({
			url : '{{ route('enquiry.store') }}',
			processResponse : function (data) {
				if(data.msg == 'successEnquiry'){
					$('#contactBox').html('<h4 class="enquiry-h1">We received your message. We will get back to you shortly!!!</h4>');
				}
			},
			onLoad : 'check'
		});
	</script>
@endpush