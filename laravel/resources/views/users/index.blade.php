@extends('users.layouts.master')

@section('content')
<div class="container home-container">
	<!-- section1 -->
	<section class="home-sec1">
		<div class="row">
			<h1>About Aurum Capital</h1>
			<p>Aurum, the Latin word for gold and the source of its chemical symbol, &quot;Au&quot;. We at Aurum want to set the gold standard in equity investment and research. Aurum capital is founded by Jiten Parmar and Niteen S Dharmawat. The combined experience of the founding team is more than 4 decades in Indian industry in general and equity markets in particular. This rich industry background helps us understand companies, complexities of business and tipping points that make or break a company. Our approach is not limited to just number crunching but going  beyond. We focus on the business, strategic direction, the competitive landscape, the governance process, the background of the promoters, turnarounds, to name a few. Often these are more critical items than just numbers. Jiten and Niteen have successfully waded through multiple bear and bull market cycles.</p>
		</div>
	</section>
	<!-- /section1 -->

	<!-- section2 -->
	<section class="home-sec2">
		<div class="row">
			<p class="home-sec2-para">Investment Philosophy of <span>Aurum Capital</span></p>
		</div>

		<div class="row home-middle">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="home-sec2-mid">
					<div class="home-sec2-fa">
						<i class="fa fa-cogs"></i>
					</div>
					<div class="home-sec2-text">
						<h1>Market cycles</h1>
						<p>The most important aspect of the equity market is going through at least one full bear-bull cycle with self-invested money. The founding team of Aurum Capital has gone through multiple such cycles and brings that experience for the benefit of investors.</p>

						<p>We understand these cycles and have the necessary patience to bide over bear cycles and corrections, which are inevitable in the equity markets.</p>
					</div>
				</div>

			</div>

			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="home-sec2-mid">
					<div class="home-sec2-fa">
						<i class="fa fa-industry"></i>
					</div>
					<div class="home-sec2-text">
						<h1>Real industry experience</h1>
						<p>The founders of Aurum Capital have worked in the industry. They just do not do number crunching instead look beyond excel, to relate the ground realities with the performance of the company.</p>

						<p>We do active scuttlebutt and connect with different players in the ecosystem of the companies. We track, be it the suppliers, dealers, distributors, buyers and so on. We do both, qualitative and quantitative analysis.</p>
					</div>
				</div>
			</div>

			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="home-sec2-mid">
					<div class="home-sec2-fa">
						<i class="fa fa-rupee"></i>
					</div>
					<div class="home-sec2-text">
						<h1>Value Investing</h1>
						<p>Our philosophy is of value investing. We buy value and not the market trends. We understand the different phases of the market and calibrate our investing approach accordingly.</p>

						<p>We believe that the principles of value investing equip you to handle the periods of over-exuberance and extreme fear better when the prices get distorted.</p>
					</div>
				</div>

			</div>

			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="home-sec2-mid">
					<div class="home-sec2-fa">
						<i class="fa fa-lightbulb-o"></i>
					</div>
					<div class="home-sec2-text">
						<h1>Focus on multi-bagger ideas</h1>
						<p>While we are value investors and conscious of the price we pay, we are also focused on multi-bagger than pocket money ideas.</p>

						<p>We believe that such ideas do not happen consistently in a short period but require patience and consistent tracking. </p>
					</div>
				</div>

			</div>
		</div>
	</section>
	<!-- /section2 -->	

</div>
@endsection