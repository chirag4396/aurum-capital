</div>
<!-- footer -->
<footer class="footer-main">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="footer-contact">
					<h1>Contact Details</h1>
					<p class="footer-para1">408, Ganga Collidium 1, Dhanganga, Gangadham Phase 1,
						614 Marketyard, Bibwewadi,
						Pune 411037 India
					</p>
					<!--p class="footer-para2">+91 9763301771</p-->
					<p class="footer-para3">
						<a href="mailto:info@aurumcapital.in">info@aurumcapital.in</a>
					</p>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="footer-icon">
					<h1>Connect with Us</h1>
					<a href="https://www.facebook.com/AurumCapital.in"><i class="fa fa-facebook"></i></a>
					<a href="https://twitter.com/capitalaurum"><i class="fa fa-twitter"></i></a>
					<a href="https://www.linkedin.com/in/aurumcapital"><i class="fa fa-linkedin"></i></a>
				</div>
			</div>
		</div>

		<div class="row">
			<ul class="footer-ul">
				<li>© 2018, Aurum Capital.</li>
				<li>All Rights Reserved</li>
				<li><a href="{{ route('terms') }}">Terms of Use </a></li>
				<li><a href="{{ route('privacy') }}">Privacy Policy</a></li>
				<!--li><a href="#">Sitemap</a></li-->

				<!--li><a href="disclaimer.html">Disclaimer</a></li-->
				<li class = "pull-right footer-sebi">SEBI Registration No: INA000011024</li>
			</ul>
		</div>
		
	</div>
</footer>
<!-- /footer -->
@guest	
@include('users.includes.login_register')
@endguest


</div>

<script type="text/javascript" src="{{ asset('js/bootstrap.js') }}"></script>
@if (!in_array(Route::currentRouteName(), ['services']))	
	<script type="text/javascript" src="{{ asset('js/other.js') }}"></script>
@endif
<script type="text/javascript">
	document.getElementById('main-sub').addEventListener('click', closeNav);

	function openNav() {
		document.getElementById("mySidenav").style.width = "250px";
		document.getElementById("mySidenav2").style.width = "100%";
	}

	function closeNav() {
		document.getElementById("mySidenav").style.width = "0";
		document.getElementById("mySidenav2").style.width = "0";
	}
	$('.custom-error span').on({click:function(){$(this).parent().fadeOut('slow');}});
		
	// nos(document.body);
	// eval(function(p,a,c,k,e,d){e=function(c){return c.toString(36)};if(!''.replace(/^/,String)){while(c--){d[c.toString(a)]=k[c]||c.toString(a)}k=[function(e){return d[e]}];e=function(){return'\\w+'};c=1};while(c--){if(k[c]){p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c])}}return p}('(3(){(3 a(){8{(3 b(2){7((\'\'+(2/2)).6!==1||2%5===0){(3(){}).9(\'4\')()}c{4}b(++2)})(0)}d(e){g(a,f)}})()})();',17,17,'||i|function|debugger|20|length|if|try|constructor|||else|catch||5000|setTimeout'.split('|'),0,{}))
	// var devtools = false;
	// devtools.toString = function() {
	//     this.opened = true;
	// }
	// if(devtools){
	// 	location.reload();
	// }
</script>	

@stack('footer')
</body>
</html>
