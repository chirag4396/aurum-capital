<!DOCTYPE html>
<html lang="en">
<head>
	<script type="text/javascript">
		// Internet Explorer 6-11
		var isIE = /*@cc_on!@*/false || !!document.documentMode;
		// Edge 20+
		var isEdge = !isIE && !!window.StyleMedia;
		if(isIE || isEdge){
			@if (Route::currentRouteName() != 'browser')
			window.location = '{{ route('browser') }}';
			@endif
		}
	</script>
	<title>Home :: Aurum Capital</title>
	<meta name="google-site-verification" content="xcU9GVN2QmhDdYnw5fCqOHzy6rY-892cs-Nl996iZB4" />
	<link rel="icon" href="{{ asset('img/fav.png') }}" type="image/x-icon" />
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" media="all" />
	<link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" media="all" />
	
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="" />
	<script type="text/javascript" src="{{ asset('js/jquery-2.1.4.min.js') }}"></script>
	<script src="{{ asset('js/crud.js') }}"></script>
	@stack('header')	
	<link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css" media="all" />
</head>
<body oncontextmenu="return false;">
	@if (Session::has('page_error'))
	<div class="custom-error custom-error-red">
		{{ Session::get('page_error') }}
		<span class="fa fa-times"></span>
	</div>
	@endif
	@if (!Auth::guest())
	@if (Auth::user()->paid)
	@php
	$newKyc = Auth::user()->kycDetail()->orderBy('kyc_created_at', 'desc')->first();
	@endphp
	@if (!Auth::user()->kyc && !$newKyc)

	<div class="custom-error custom-error-blue">
		<span class="fa fa-times"></span>
		Please complete your <a href="{{ route('kyc-upload',['id' => Auth::id()]) }}" class="btn btn-sm home-login2">KYC</a> 
		@if (!Auth::user()->risk)
		and <a href="{{ route('risk-upload',['id' => Auth::id()]) }}" class="btn btn-sm home-login2">Risk Profile</a> Questionnaire to become our subscriber.
		@endif

	</div>
	
	@else
	@if ($newKyc)
		
	
	@if (!$newKyc->kyc_status)
	<div class="custom-error custom-error-blue">
		@if ($newKyc->kyc_reject)
		Your KYC details is Rejected. Please upload again <a href="{{ route('kyc-upload',['id' => Auth::id()]) }}" class="btn btn-sm home-login2">KYC</a>
		@else
		Your KYC is under review.
		@endif
		@if (!Auth::user()->risk)
		Please complete your <a href="{{ route('risk-upload',['id' => Auth::id()]) }}" class="btn btn-sm home-login2">Risk Profile</a> Questionnaire to become our subscriber.
		@endif
		
		<span class="fa fa-times"></span>
	</div>
		@else	
			@if (!Auth::user()->risk)
			<div class="custom-error custom-error-blue">
				Please complete your <a href="{{ route('risk-upload',['id' => Auth::id()]) }}" class="btn btn-sm home-login2">Risk Profile</a> Questionnaire to become our subscriber.
				<span class="fa fa-times"></span>
			</div>
			@endif
		@endif
	@endif
	@endif

	@endif
	@endif	
	<div id="main">
		<!-- header -->
		<header>
			<div class="row">
				<div class="header-main">
					<div class="home-menu">
						@if (Route::currentRouteName() != 'browser')

						<div id="mySidenav" class="sidenav">
							<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
							<a href="{{ route('home') }}">Home</a>
							<a href="{{ route('about') }}">About Founders</a>
							<a href="{{ route('services') }}">Services & Pricing</a>
							<a href="https://aurumcapital.in/blogs/">Blog</a>
							<!--a href="{{ route('testimonials') }}">Testimonials</a-->
							<!--a href="{{ route('past-performances') }}">Past Performance</a-->
							<a href="{{ route('current-recommendation') }}">Current Recommendation </a>
							<a href="{{ route('investor') }}">Investor Forum</a>
							<a href="{{ route('faqs') }}">FAQ's</a>
							<a href="{{ route('contact') }}">Contact Us</a>
						</div>	

						<div id="mySidenav2" class="sidenav">
							<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
							<a href="{{ route('home') }}">Home</a>
							<a href="{{ route('about') }}">About Founders</a>
							<a href="{{ route('services') }}">Services & Pricing</a>
							<a href="https://aurumcapital.in/blogs/">Blog</a>
							<!--a href="{{ route('testimonials') }}">Testimonials</a-->
							<!--a href="{{ route('past-performances') }}">Past Performance</a-->
							<a href="{{ route('current-recommendation') }}">Current Recommendation </a>
							<a href="{{ route('investor') }}">Investor Forum</a>
							<a href="{{ route('faqs') }}">FAQ's</a>
							<a href="{{ route('contact') }}">Contact Us</a>			
						</div>

						<div class="menu" onclick="openNav()">&#9776;</div>
						@endif
						<div class="home-logo">
							<a href="{{ route('home') }}">							
								<img src="{{ asset('img/logo.png') }}" alt="img">								
							</a>
						</div>
					</div>
					@if (Route::currentRouteName() != 'browser')

					@guest
					<div class="home-login">
						<a href="javascript:void(0)" class="home-login1" data-toggle="modal">Login</a>
						<a href="javascript:void(0)" class="home-login2" data-toggle="modal">Free Sign Up</a>
					</div>
					@else
					<div class="login-menu">
						<div class="dropdown">
							<a class="dropdown-toggle" type="button" id="menu1" data-toggle="dropdown"><i class="fa fa-user"></i> {{ Auth::user()->name }}
								<span class="caret"></span>
							</a>
							<ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
								{{-- <li><a role="menuitem" href="#"><i class="glyphicon glyphicon-share"></i> Go to Website</a></li>
								<li  class="divider"></li>
								<li><a role="menuitem" href="#" class="stock-768"> <i class="glyphicon glyphicon-stats"></i> Stock</a></li>
								<li><a role="menuitem" href="#"><i class="fa fa-user"></i> My Profile</a></li> --}}
								@if (Auth::user()->paid)
								<li><a role="menuitem" href="{{ route('kyc-upload',['id' => Auth::id()]) }}"><i class="glyphicon glyphicon-flag"></i> KYC Details</a></li>
								@if (Auth::user()->risk)
								<li><a role="menuitem" href="{{ route('risk-result',['id' => Auth::id()]) }}"><i class="fa fa-pie-chart"></i> Risk Profile</a></li>
								@else
								<li><a role="menuitem" href="{{ route('risk-upload',['id' => Auth::id()]) }}"><i class="fa fa-pie-chart"></i> Risk Profile</a></li>
								@endif
								<li class="divider"></li>
								@endif
								{{-- <li><a role="menuitem" href="myaccount.html"><i class="fa fa-credit-card"></i> My Account</a></li>
								<li><a role="menuitem" href="changepassword.html"><i class="glyphicon glyphicon-cog"></i> Change Password</a></li> --}}
								<li>										
									<a class="home-login2" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
										<i class="glyphicon glyphicon-share"></i> Sign Out
									</a>
									<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
										{{ csrf_field() }}
									</form>
								</li>
							</ul>
						</div>
					</div>
				</div>					
				@endguest	
				@endif					
			</div>
		</div>		
	</header>

	<!-- header -->	
	<div id="main-sub">