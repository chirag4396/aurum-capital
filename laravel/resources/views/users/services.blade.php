@extends('users.layouts.master')

@section('content')
<section class="services-sec1">
	<div class="container">
		<div class="row">
			<h1>Our Services & Pricing</h1>
			<p>These plans will cover comprehensively researched portfolio stock ideas in a simple and easy to understand format. We will also be providing regular updates on all the stocks under coverage. Our focus will be on the companies that are fundamentally strong and/or offering value investment opportunities. The companies can also be in special situation, turnarounds, and have an opportunity to appreciate. Please read our <a href = "{{ route('faqs') }}">FAQs</a>, <a href = "{{ route('terms') }}">Terms of Use</a> and <a href = "{{ route('privacy') }}">Privacy Policy</a> before subscribing our Services.</p>

			<h2><span>1 year plan:</span> &#8377; 25,000</h2>
			<h2><span>2 year plan:</span> &#8377; 40,000</h2>

			<h3>Online:</h3>
			<p>Before making payment, please do a free sign up on the website, if you have not done it already.</p>
			<a href="{{ route('get-pay') }}"><h2>Pay Online</h2></a>

			<h3>Account Transfer:</h3>
			<p>If you are transferring through Cheque/DD/Direct account then please send an email to <a href="mailto:info@aurumcapital.in">info@aurumcapital.in</a> mentioning your name, email id, account number, bank name, transaction number and the amount transferred. <b>We do not accept cash. Please do not deposit CASH. Payment can be through Cheque, DD, or direct account transfer.</b></p>
			<div class="services-sec1-mid">
				<h4><span>Account Name:</span> AURUM CAPITAL</h4>
				<h4><span>Account Number:</span> 7212058645</h4>
				<h4><span>Account Type:</span> Current Account</h4>
				<h4><span>Bank:</span> Kotak Mahindra Bank</h4>
				<h4><span>IFSC Code:</span> KKBK0001771</h4>
				<h4><span>Branch Code:</span> 1771</h4>
			</div>
			
		</div>
	</div>
</section>
@endsection