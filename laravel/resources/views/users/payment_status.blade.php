@extends('users.layouts.master')

@section('content')
<section class="services-sec1">
	<div class="container">
		<div class="row">
			<h1>Payment Status</h1>
			<div class="col-md-6 col-md-offset-3">
				@if ($status)
				<strong>Thank you for choosing {{ config('app.name') }}.</strong>								
				<p>We have received the payment. Please also complete the mandatory <a href="{{ route('kyc-upload',['id' => Auth::id()]) }}" class="btn btn-sm home-login2">KYC</a> and <a href="{{ route('risk-upload',['id' => Auth::id()]) }}" class="btn btn-sm home-login2">Risk Profiling Questionnaire</a> to become our subscriber.</p>
				@else			
				<strong>Payment Failure!</strong> Please Try again or we'll contact to you shortly or please feel free to contact us at <b>{{ config('app.support_email') }}</b>
				@endif
			</div>
		</div>
	</div>
</section>
@endsection

@push('footer')
<script>
</script>
@endpush
