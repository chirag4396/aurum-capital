@extends('users.layouts.master')

@push('header')
@php
$ID= 'risk-profile';
@endphp
<style type="text/css">
.radio {
	position: relative;
	padding: 0 6px;
	margin: 10px 0 0;
}

.form-group input[type='radio'] {
	display: none;
}

.radio label {
	color: #333;
	font-weight: normal;
	padding: 0;
	font-size: 16px;
}

.radio label:before {
	content: " ";
	display: inline-block;
	position: relative;
	top: 5px;
	margin: 0 5px 0 0;
	width: 18px;
	height: 18px;
	border-radius: 11px;
	border: 2px solid #0066cc;
	background-color: transparent;
}

.radio input[type=radio]:checked + label:after {
	border-radius: 11px;
	width: 10px;
	height: 10px;
	position: absolute;
	top: 16px;
	left: 10px;
	content: " ";
	display: block;
	background: #0066cc !important;
}
</style>
@endpush

@section('content')
<section class="riskprofile">	
	@if (!is_null($edit))
	@php		
	$riskAns = \App\RiskProfileAnswer::where('rpa_rp', Auth::user()->recentRiskProfile->rp_id)->get();
	@endphp
	@endif
	<div class="container">
		<div class="row">
			<h1><i class="fa fa-pie-chart"></i> Risk Profile</h1>
			<hr>
			<p>Find out portfolio strategy that suits your risk profile.</p>

			<div class="row">
				<form id="{{ $ID }}Form" class="form-horizontal">
					@forelse ($risk_questions as $k => $rq)
					<div class="form-group">
						<label>{{ ($k+1).'. '.$rq->rq_qus }}</label>
						<div class="risk-error hidden" id = "err{{ $rq->rq_id }}"></div>
						@if ($k)
						@forelse ($rq->options()->get() as $ro)
						@php
						$chk = Auth::user()->risk ? ( isset($riskAns[$k]) ? ($riskAns[$k]->rpa_ans == $ro->ro_id ? 'checked' : '') : '') : '';
						@endphp
						<div class="radio">
							<input type="radio" id = "op{{ $ro->ro_id }}" name="op{{ $rq->rq_id }}" value="{{ $ro->ro_id }}" {{ $chk }}>
							<label for="op{{ $ro->ro_id }}">{{ $ro->ro_option }}</label>
						</div>							
						@empty							
						@endforelse
						@else
						<div style="width: 30%;" class="input-group date" >
							<input class="form-control" placeholder="DD-MM-YYYY" name="op{{ $rq->rq_id }}" data-format="DD-MM-YYYY" type="date" max = "{{ \Carbon\Carbon::now()->subYears(18)->format('Y-m-d') }}" value ="{{ Auth::user()->risk ? Auth::user()->recentRiskProfile->rp_dob : \Carbon\Carbon::now()->subYears(18)->format('Y-m-d') }}">
						</div>						
						@endif
					</div>
					@empty

					@endforelse
					<button type="submit" class="btn btn-primary riskprofile-submit">Submit</button>
					<div class="alert alert-warning hidden text-center" id = "formAlert"></div>
				</form>
			</div>
		</div>
	</div>
</section>	
@endsection

@push('footer')
<script type="text/javascript">

	function check(){
		var check = [false];
		for(var i =2; i <= 10; i++){		
			if (!$("input[name='op"+i+"']:checked").val()) {
				$('#err'+i).removeClass('hidden').html('Please select these.');
				check.push(true);
			}else {
				$('#err'+i).addClass('hidden').html('');
				check.push(false);
			}
		}
		return check;
	}

	$('#{{ $ID }}Form').on({
		'submit' : function(e){
			e.preventDefault();
			$('#formAlert').removeClass('hidden').html('Generating you Risk profile score, please wait!!');
			var fd = new FormData(this);
			
			if($.inArray(true,check()) < 0){
				$.ajax({
					url : '{{ route('admin.'.$ID.'.store') }}',
					type : 'post',
					data : fd,
					processData : false,
					contentType : false,
					success : function(data){
						if(data.msg == 'success'){
							$('#formAlert').removeClass('hidden').html('Risk profile score is generated successfully.');
							window.setTimeout(function(){
								location.href = data.location;
							},2000);
						}
					}
				});
			}else{
				$('#formAlert').removeClass('hidden').html('Please select all options.');
			}
		}
	});
</script>
@endpush