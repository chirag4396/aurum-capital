@extends('users.layouts.master')

@section('content')

<!-- /section1 -->
<section class="about-sec1">
	<div class="container">
		<h1>Sorry, we are not supporting Internet Explorer.</h1>
		<img src="img/ie.png" alt="img" class="ig-img">

		<hr>

		<h1>You may please use any one of these browsers for better experience.</h1>
		<img src="img/chome.png" alt="img" class="ig-img">
		<img src="img/safari.png" alt="img" class="ig-img">
		<img src="img/fire.png" alt="img" class="ig-img">
		<img src="img/opera.png" alt="img" class="ig-img">
	</div>
</section>
<!-- /section1 -->		
@endsection