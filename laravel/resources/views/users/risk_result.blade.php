@extends('users.layouts.master')

@section('content')
<section class="riskprofile">
	@php
	$score = Auth::user()->recentRiskProfile->rp_score;
	@endphp
	@if ($score)
	<div class="riskprofile-result">
		<h1>Your Profile is <span>{{ ($score >= 50) ? 'High' : (($score >= 25 && $score < 50) ? 'Medium' : 'Low') }}</span></h1>
		<div class="riskprofile-output">
			{{-- @if ($score >= 50) --}}
			<div class="row {{ ($score >= 50) ? 'active' : '' }}">
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-2">
					<p>High</p>
				</div>
				<div class="col-lg-9 col-md-9 col-sm-9 col-xs-10">
					<ul>
						<li>You are an experienced or sophisticated investor.Security of capital is secondary to the pursuit of high investment returns. You are happy to accept the risks this involves. You have a strong bias towards investments with high growth potential. The portfolio allocation will be  stocks (85-90%) and bonds/liquid funds/cash (10-15%).</li>
						<li>Spread your capital equally in all stock ideas that are close to buying ranges.</li>
						<li>Do not give more than 25% allocation to a single stock.</li>
						<li>Can concentrate 75% of the portfolio in 7-10 stocks.</li>
					</ul>
				</div>
			</div>
			{{-- @elseif($score >= 25 || $score < 50) --}}
			<div class="row {{ ($score > 25 && $score < 50) ? 'active' : '' }}">
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-2">
					<p>Medium</p>
				</div>
				<div class="col-lg-9 col-md-9 col-sm-9 col-xs-10">
					<ul>
						<li>You are a balanced yet assertive investor with understanding of investment market behavior. You prefer a balance between capital growth and capital security while interested in maximizing long term capital growth. You are happy to take calculated risks in order to maximize long term capital growth and willing to invest in growth stocks. The portfolio allocation will be stock investment (60-70%), FDs/bonds (10-25%), and liquid funds/cash (5-15%).</li>
						<li>Spread your capital in all stock ideas that are close to buying ranges.</li>
						<li> Do not give more than 20% allocation to a single stock.</li>
						<li>Try to have a portfolio comprising of at least 8-12 stocks.</li>
					</ul>
				</div>
			</div>			
			{{-- @else --}}
			<div class="row {{ ($score <= 25) ? 'active' : '' }}">
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-2">
					<p>Low</p>
				</div>
				<div class="col-lg-9 col-md-9 col-sm-9 col-xs-10">
					<ul>
						<li>You are a conservative investor who does not wish to take any investment risk. Your priority is to safeguard your investment capital. You are prepared to forego higher returns for peace of mind. The portfolio allocation will be stocks (20-30%), FDs/bonds (30-40%) and liquid funds/cash (20-30%).</li>
						<li> Spread your capital in all stock ideas that are close to buying ranges.</li>
						<li> Do not give more than 10% allocation to a single stock.</li>
						<li> Try to have a portfolio comprising of at least 10-15 stocks.</li>
					</ul>
				</div>
			</div>
			{{-- @endif --}}
		</div>
	</div>
	<div class="text-center" style="margin-bottom: 10px;">
		<a href = "{{ route('risk-upload',['id' => Auth::id(), 'edit' => 'edit']) }}" class="btn btn-primary riskprofile-submit">Edit Risk Profile</a>
	</div>		
	@endif
</section>
@endsection