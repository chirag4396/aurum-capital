@extends('admin.layouts.master')
@section('title')
Create Monthly Performance
@endsection
@php
	$ID = 'monthly-performance';
@endphp
@section('content')
<div class="right_col" role="main">	
	<div class="page-title">
		<div class="title_left">
			<h3> Create Monthly Performance</h3>
		</div>
		<div class="pull-right">
			<a href = "{{ route('admin.'.$ID.'.index') }}" class="btn btn-danger">Back</a>
		</div>
	</div>
	<div class="clearfix">
	</div>
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">				
				<div class="x_content">
					@include('admin.forms.monthly_performance_form')
				</div>
			</div>
		</div>
	</div>
</div>

@endsection