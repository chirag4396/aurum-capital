        <!-- footer content -->
        <footer>
          <div class="pull-right">
            {{ config('app.name') }}
        </div>
        <div class="clearfix"></div>
    </footer>
    <!-- /footer content -->
</div>
</div>

<!-- jQuery -->
<script src="{{ asset('admin-assets/jquery/dist/jquery.min.js') }}"></script>
<!-- Bootstrap -->
<script src="{{ asset('admin-assets/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('admin-assets/fastclick/lib/fastclick.js') }}"></script>
<!-- NProgress -->
<script src="{{ asset('admin-assets/nprogress/nprogress.js') }}"></script>
<script src="{{ asset('admin-assets/js/custom.js') }}"></script>
<script src="{{ asset('js/crud.js') }}"></script>
<script type="text/javascript">
	var mainUrl = '{{ route('home') }}/';
	$(window).on('load',function(){
		$('#loader').addClass('hidden');
	});
</script>
@stack('footer')
</body>
</html>