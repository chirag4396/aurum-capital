@extends('admin.layouts.master')

@section('title')
All Clients
@endsection
@push('header')
<!-- Datatables -->
<link href="{{ asset('admin-assets/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('admin-assets/datatables.net-buttons-bs/css/buttons.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('admin-assets/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('admin-assets/datatables.net-responsive-bs/css/responsive.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('admin-assets/datatables.net-scroller-bs/css/scroller.bootstrap.min.css') }}" rel="stylesheet">
@php
$ID = '';
@endphp
@endpush

@section('content')

<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>All Clients</h3>
      </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_content">            
            <table id="{{ $ID }}Table" class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th>Sr. No.</th>
                  <th>ID</th>
                  <th>NAME</th>
                  <th>EMAIL</th>
                  <th>MOBILE</th>
                  <th>ACTION</th>
                </tr>
              </thead>
              <tbody>
                @forelse ($clients as $k => $c)
                <tr id = "tr-{{ $c->id }}">
                  <td>{{ ++$k }}</td>                  
                  <td>{{ $c->detail->ud_client_id or '-' }}</td>                  
                  <td>{{ $c->name }}</td>          
                  <td>{{ $c->email }}</td>                  
                  <td>{{ $c->detail->ud_mobile or '-' }}</td>                  
                  <td>                    
                    <a href = "{{ route('admin.import', ['id' => $c->id]) }}" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i> Upload </a>|
                    <a href = "{{ route('admin.report', ['id' => $c->id]) }}" class="btn btn-primary btn-xs"><i class="fa fa-list"></i> Weekly Report </a>|
                    
                    @if ($c->approved)
                      <a href = "{{ route('admin.change-status', ['id' => $c->id, 'status' => 0]) }}" class="btn btn-danger btn-xs"><i class="fa fa-times"></i> Disallow Access </a>|
                    @else
                      <a href = "{{ route('admin.change-status', ['id' => $c->id, 'status' => 1]) }}" class="btn btn-success btn-xs"><i class="fa fa-check"></i> Allow Access </a>|
                    @endif
                    
                    <a href = "{{ route('admin.user.edit', ['id' => $c->id]) }}" class="btn btn-success btn-xs"><i class="fa fa-pencil"></i> Edit </a>
                    <form action="{{ route('admin.user.destroy', ['id' => $c->id]) }}" method="post">
                      {{ csrf_field() }}
                      <input type="hidden" name="_method" value="DELETE">
                      <button class="btn btn-danger btn-xs"><i class="fa fa-trash"></i> Delete </button>
                    </form>
                  </td>
                </tr>
                @empty                
                @endforelse
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

    {{-- <div class="modal fade" id="{{ $ID }}Modal" tabindex="-1" role="dialog" aria-labelledby="Modal" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="{{ $ID }}ModalLabel">Edit {{ $ID }}</h4>
          </div>
          <div class="modal-body">
            @include('admin.forms.category_form')
          </div>
        </div>
      </div>
    </div> --}}

    {{-- <div class="modal fade" id="{{ $ID }}DModal" tabindex="-1" role="dialog" aria-labelledby="Modal" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header text-center">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">Are You Sure you want to delete <span id = "delName"></span> category ? it'll also delete all related Sub Categories, Child Categories and Products</h4>
            <br>
            <div>
              <form id = "{{ $ID }}DForm">
                <button type = "button" class="btn btn-danger" id = "yes">Yes</button>
                <button type = "button" class="btn btn-success" data-dismiss="modal">No</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div> --}}
  </div>
</div>

@endsection

@push('footer')
<script src="{{ asset('admin-assets/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-buttons/js/buttons.flash.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
{{-- <script src="{{ asset('admin-assets/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js') }}"></script> --}}
<script src="{{ asset('admin-assets/datatables.net-keytable/js/dataTables.keyTable.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-responsive-bs/js/responsive.bootstrap.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-scroller/js/dataTables.scroller.min.js') }}"></script>
<script src="{{ asset('admin-assets/jszip/dist/jszip.min.js') }}"></script>
<script src="{{ asset('admin-assets/pdfmake/build/pdfmake.min.js') }}"></script>
<script src="{{ asset('admin-assets/pdfmake/build/vfs_fonts.js') }}"></script>

<script type="text/javascript">  

  Table.init(ID);


</script>
@endpush