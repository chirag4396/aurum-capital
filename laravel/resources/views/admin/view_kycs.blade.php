@extends('admin.layouts.master')

@section('title')
All KYCS
@endsection
@push('header')
<!-- Datatables -->
<link href="{{ asset('admin-assets/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('admin-assets/datatables.net-buttons-bs/css/buttons.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('admin-assets/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('admin-assets/datatables.net-responsive-bs/css/responsive.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('admin-assets/datatables.net-scroller-bs/css/scroller.bootstrap.min.css') }}" rel="stylesheet">
@php
$ID = 'kyc';
@endphp
<script type="text/javascript">
  var ID = '{{ $ID }}';
</script>
@endpush

@section('content')

<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div{{--  class="title_left" --}}>
      <h3>
        @isset ($user)
        All KYCS of {{ $user->name.' ('.$user->mobile.')' }}
        <a href="{{ route('admin.clients', ['type' => 1, 'tname' => 'paid']) }}" class="btn btn-primary pull-right">Back</a>        
        @else
        All New KYC Requests
        <a href="{{ redirect()->back() }}" class="btn btn-primary pull-right">Back</a>        
        
        @endisset          
      </h3>
    </div>
  </div>
  <div class="clearfix"></div>
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_content">            
          <table id="{{ $ID }}Table" class="table table-striped table-bordered">
            <thead>
              <tr>
                <th>Sr. No.</th>
                @if (!isset($user))
                <th>Name</th>
                @endif
                <th>PAN Card</th>
                <th>Created At</th>
                <th>Status</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              @forelse ($kycs as $k => $kyc)
              <tr id = "tr-{{ $kyc->id }}">                  
                <td>{{ ++$k }}</td>                  
                @if (!isset($user))
                <td>{{ $kyc->user->name or ''}}</td>
                @endif
                <td>{{ $kyc->kyc_number }}</td>
                <td>{{ \Carbon\Carbon::parse($kyc->kyc_created_at)->format('jS M, Y h:i A') }}</td>
                @if ($kyc->kyc_reject)
                <td><span class="badge badge-danger">Rejected</span></td>
                @else
                @if ($kyc->kyc_status)
                <td><span class="badge badge-success">Approved</span></td>
                @else
                <td><span class="badge badge-warning">Not Approved</span></td>
                @endif
                @endif
                <td>
                  <button  class="btn btn-primary btn-xs" onclick="viewDetail('{{ $kyc->kyc_id }}');"><i class="fa fa-eye"></i> View</button>
                    {{-- |<a href = "{{ route('admin.'.$ID.'.edit', ['id' => $kyc->kyc_id]) }}" class="btn btn-success btn-xs"><i class="fa fa-pencil"></i> Edit </a>|
                    <form action="{{ route('admin.'.$ID.'.destroy', ['id' => $kyc->kyc_id]) }}" method="post">
                      {{ csrf_field() }}
                      <input type="hidden" name="_method" value="DELETE">
                      <button class="btn btn-danger btn-xs"><i class="fa fa-trash"></i> Delete </button>
                    </form> --}}
                  </td>
                </tr>
                @empty    
                <tr align="center">
                  <td colspan="4">No Investor Education found <a class="btn btn-primary" href="{{ route('admin.'.$ID.'.create') }}">Add one now</a></td>
                </tr>            
                @endforelse
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="{{ $ID }}ViewModal" tabindex="-1" role="dialog" aria-labelledby="Modal" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="{{ $ID }}ModalLabel">View {{ $ID }}</h4>
          </div>
          <div class="modal-body" id = "viewData"></div>
        </div>
      </div>
    </div>

    {{-- <div class="modal fade" id="{{ $ID }}DModal" tabindex="-1" role="dialog" aria-labelledby="Modal" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header text-center">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">Are You Sure you want to delete <span id = "delName"></span> category ? it'll also delete all related Sub Categories, Child Categories and Products</h4>
            <br>
            <div>
              <form id = "{{ $ID }}DForm">
                <button type = "button" class="btn btn-danger" id = "yes">Yes</button>
                <button type = "button" class="btn btn-success" data-dismiss="modal">No</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div> --}}
  </div>
</div>

@endsection

@push('footer')
<script src="{{ asset('admin-assets/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-buttons/js/buttons.flash.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
{{-- <script src="{{ asset('admin-assets/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js') }}"></script> --}}
<script src="{{ asset('admin-assets/datatables.net-keytable/js/dataTables.keyTable.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-responsive-bs/js/responsive.bootstrap.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-scroller/js/dataTables.scroller.min.js') }}"></script>
<script src="{{ asset('admin-assets/jszip/dist/jszip.min.js') }}"></script>
<script src="{{ asset('admin-assets/pdfmake/build/pdfmake.min.js') }}"></script>
<script src="{{ asset('admin-assets/pdfmake/build/vfs_fonts.js') }}"></script>

<script type="text/javascript">  

  // Table.init(ID);

  function viewDetail(id) {
    $('#viewData').html('<div class="text-center">Loading...</div>');
    $('#{{ $ID }}ViewModal').modal('toggle');
    $.ajax({
      url : '{{ route('admin.kyc.index') }}/'+id,
      type : 'get',
      success : function(data){
        $('#viewData').html(data);
        // console.log(data);
      }
    });
  }

  function others() {
    $('#rejectBtn').on({
      'click' : function () {
        $('#reasonBox').removeClass('hidden');
      }
    });
    $('#other').on({
      'click change' : function () {
        if($(this).is(':checked')){
          $('#otherBox').removeClass('hidden'); 
        }else{
          $('#otherBox').addClass('hidden'); 
        }
      }
    });
  }
</script>
@endpush