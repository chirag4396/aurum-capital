@extends('admin.layouts.master')
@section('title')
Upload KYC
@endsection

@php
$ID = 'kyc';
@endphp

@push('header')
<script>
	ID = '{{ $ID }}';
</script>
</style>
@endpush
@section('content')

<div class="right_col" role="main">	
	<div class="page-title">
		<div class="title_left">
			<h3>
				Upload KYC
				{{-- <a href = "{{ route('admin.report',['id' => $user->id]) }}" class="btn btn-primary pull-right"><i class="fa fa-list"></i> Report</a> --}}
			</h3>
		</div>		
	</div>
	<div class="clearfix">
	</div>
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_content">					
					<div class="col-md-6">
						<div id = "uploadBox">
							<form action="" class="form-horizontal form-label-left" method="post"  enctype="multipart/form-data" id = "uploadForm">
								<div class="form-group">
									<label class="control-label col-md-4 col-sm-4 col-xs-12">Display Image
									</label>
									<div class="col-md-3 col-sm-3 col-xs-12 text-center">			
										<div class="clearfix"></div>
										<img src="{{ asset(isset($article) ? $article->ar_image : 'images/no-image.png') }}" id = "imagePreview">
										<div class="clearfix"></div>
										<input type = "file" id ="image" accept="image/png, image/jpeg, image/jpg" class="hidden form-control1" name = "image">		
										<label class="btn btn-success" for = "image">Choose Image</label>			
									</div>
								</div>
								<div class="form-group">
									<label>Select Excel File:</label>
									<input type="file" name = "excel">
									<button type="submit" class="btn btn-primary">Upload</button>
								</div>										
							</form>
						</div>
						{{-- <div id = "importBox" class="hidden">
							<form action="{{ route('admin.import-data') }}" class="form-horizontal form-label-left" method="post">
								{{ csrf_field() }}

								<input type="hidden" name="excelFile" id = "excelFile">
								<input type="hidden" name="client" value="{{ $user->id }}">
								<button type="submit" class="btn btn-primary" id = "excelBtn">Import</button>
							</form>
						</div> --}}
					</div>									
					{{-- <div class="col-md-6">
						<a href="{{ asset('excels/weekly-report-quants-capital-(sample).xlsx') }}" download class="btn btn-danger">Download Sample xls file</a>
					</div>							 --}}
				</div>
			</div>
			<div class="clearfix"></div>
			@if (Session::has('status'))
				<div class="alert alert-success">Successfully Imported, you can Upload any other Excel for {{ $user->name }}</div>
			@endif
		</div>
	</div>
</div>		
</div>
</div>

@endsection

@push('footer')
<script type="text/javascript">
	imageUpload('image');
	$('#uploadForm').CRUD({
		{{-- url : "{{ route('admin.upload-kyc') }}", --}}
		processResponse : function(data){    		
			if(data.msg == 'success'){
				$('#excelFile').val(data.file);
				$('#uploadBox').addClass('hidden');
				$('#importBox').removeClass('hidden');
			}
		}
	});	
	// $('#excelBtn').on({
	// 	'click' : function(){			
	// 		$('#loader h1').html('Extracting Data from Excel, Please wait!!');
	// 		$('#loader').removeClass('hidden');
	// 	}
	// });
</script>
@endpush