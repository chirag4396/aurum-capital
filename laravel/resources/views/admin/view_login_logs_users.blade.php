@extends('admin.layouts.master')

@section('title')
Login Logs
@endsection
@push('header')
<!-- Datatables -->


{{-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css"> --}}

<link href="{{ asset('admin-assets/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('admin-assets/datatables.net-buttons-bs/css/buttons.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('admin-assets/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('admin-assets/datatables.net-responsive-bs/css/responsive.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('admin-assets/datatables.net-scroller-bs/css/scroller.bootstrap.min.css') }}" rel="stylesheet">
@php
$ID = 'user';
@endphp
@endpush

@section('content')

<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>Login Logs</h3>
      </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_content">
            <table id="users-table" class="table cutom-table">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>Mobile</th>
                  <th>Email</th>
                  <th>Date</th>
                </tr>
              </thead>
              <tbody></tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="{{ $ID }}ViewModal" tabindex="-1" role="dialog" aria-labelledby="Modal" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="{{ $ID }}ModalLabel"></h4>
          </div>
          <div class="modal-body text-center" id = "viewData">            
          </div>
        </div>
      </div>
    </div> 
  </div>
</div>

@endsection

@push('footer')


<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>



<script src="{{ asset('admin-assets/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') }}"></script>

<script src="{{ asset('admin-assets/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-keytable/js/dataTables.keyTable.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-responsive-bs/js/responsive.bootstrap.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-scroller/js/dataTables.scroller.min.js') }}"></script>

<script src="{{ asset('admin-assets/jszip/dist/jszip.min.js') }}"></script>

<script src="{{ asset('admin-assets/pdfmake/build/pdfmake.min.js') }}"></script>
<script src="{{ asset('admin-assets/pdfmake/build/vfs_fonts.js') }}"></script>

<script type="text/javascript">  
  function viewDetail(id, name, stat) {
    $('#{{ $ID }}ViewModal').modal('toggle');
    $('#viewData').html('<div class="text-center">Loading...</div>');
    $('#{{ $ID }}ModalLabel').html(stat +' '+ name + ' Subscription');
    $.post('{{ route('admin.block-form') }}',{id : id}, function( data ) {
      $('#viewData').html(data);
    });
  }
  $(function() {
    var t = $('#users-table').DataTable({
      processing: true,
      serverSide: true,
      pageLength : 50,
      ajax: {
        url : '{!! route('admin.all-logs-users') !!}'
      },
      columns: [      
      { data: 'id', name: 'id' },
      { data: 'name', name: 'name'},
      { data: 'mobile', name: 'mobile' },
      { data: 'email', name: 'email' },
      { data: 'action', name: 'action',"orderable": false },
      ],
      dom: "Bfrtip",
      buttons: [      
      {
        extend: "excel",
        className: "btn-sm",
        exportOptions: {
          columns: [0,1,2,3,4]
        }
      },
      {
        extend: "pdfHtml5",
        className: "btn-sm",
        exportOptions: {
          columns: [0,1,2,3,4]
        }
      }
      ],
      responsive: true,
      columnDefs: [ {        
        "targets": 0
      } ],
      order: [[ 1, 'asc' ]]
    });
    t.on( 'order.dt search.dt', function () {
      t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
        cell.innerHTML = i+1;
      } );
    } ).draw();
  });

</script>
@endpush