@push('header')
@php
$ID = 'article';
@endphp
<script>
	ID = '{{ $ID }}';
</script>

<style type="text/css">
#imagePreview{
	height: 150px;
	object-fit: contain;
}
</style>
@endpush

<form id = "{{ $ID }}Form" class="form-horizontal form-label-left" enctype="multipart/formdata">	
	<div class="form-group">
		<label class="control-label col-md-4 col-sm-4 col-xs-12">Title
		</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<input type="text" class="form-control col-md-7 col-xs-12" name = "title" value="{{ $article->ar_title or '' }}">
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-4 col-sm-4 col-xs-12">Display Image
		</label>
		<div class="col-md-3 col-sm-3 col-xs-12 text-center">			
			<div class="clearfix"></div>
			<img src="{{ asset(isset($article) ? $article->ar_image : 'images/no-image.png') }}" id = "imagePreview">
			<div class="clearfix"></div>
			<input type = "file" id ="image" accept="image/png, image/jpeg, image/jpg" class="hidden form-control1" name = "image">		
			<label class="btn btn-success" for = "image">Choose Image</label>			
		</div>
	</div>
	<div class="form-group label-floating">
		<label class="control-label col-md-4 col-sm-4 col-xs-12">PDF/DOCX/DOC</label>
		<input type = "file" id ="pdf" accept="application/msword, text/plain, application/pdf" class="hidden form-control1" name="pdf">
		<label class="btn btn-success" for = "pdf">Choose PDF/DOCX/DOC</label>
	</div> 
	@isset ($article)
	<input type="hidden" name="id" value = "{{ $article->ar_id }}">
	@endisset
	<div class="ln_solid">
	</div>
	<div class="form-group text-center">							
		<button type="submit" class="btn btn-success">{{ isset($article) ? 'Update' : 'Add' }}</button>
	</div>					
</form>
@push('footer')
<script>
	imageUpload('image');		
	$('#{{ $ID }}Form').CRUD({
		url : '{{ route('admin.'.$ID.'.store') }}'
	});
</script>
@endpush