<form>
	@forelse ($risk_questions as $k => $rq)
	<div class="form-group">
		<label>{{ ($k+1).'. '.$rq->rq_qus }}</label>
		@if ($k)
		@forelse ($rq->options()->get() as $ro)
		@php
		$chk = $riskAns[$k] ? ($riskAns[$k]->rpa_ans == $ro->ro_id ? 'checked' : '') : '';
		@endphp
		<div>
			<input type="radio" id = "op{{ $ro->ro_id }}" name="op{{ $rq->rq_id }}" value="{{ $ro->ro_id }}" {{ $chk }} disabled>
			<label for="op{{ $ro->ro_id }}">{{ $ro->ro_option }}</label>
		</div>							
		@empty							
		@endforelse
		@else
		<div style="width: 30%;" class="input-group date" >
			<input type="date" value = "{{ $rp->rp_dob }}" class="form-control" readonly>
		</div>						
		@endif
	</div>
	@empty

	@endforelse
</form>