@push('header')
<link href="{{ asset('css/select2.css') }}" rel="stylesheet" />
@endpush

<form id = "{{ $ID }}Form" class="form-horizontal form-label-left" enctype="multipart/formdata">	
	<div class="form-group">
		<label class="control-label col-md-4 col-sm-4 col-xs-12">Stock</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<select class="form-control stock" name = "stock">
				<option value="-1">--select--</option>
				@forelse (\App\Stock::where('st_active', 1)->get() as $st)
				<option value="{{ $st->st_id }}">{{ $st->st_title }}</option>
				@empty
				@endforelse
			</select>
		</div>
	</div>	
	<div class="form-group">
		<label class="control-label col-md-4 col-sm-4 col-xs-12">Action</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<select class="form-control todo" name = "to_do">
				<option value="-1">--select--</option>
				@forelse (\App\Action::get() as $ac)
				<option value="{{ $ac->ac_id }}">{{ $ac->ac_title }}</option>
				@empty
				@endforelse
			</select>
		</div>
	</div>	
	<div class="form-group">
		<label class="control-label col-md-4 col-sm-4 col-xs-12">Percent Allocation</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<input type="number" class="form-control" name = "percent_allocation" step=".01">
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-4 col-sm-4 col-xs-12">Date</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<input type="date" class="form-control" name = "date">
			{{-- <div class='input-group date' id='datetimepicker1'>
				<input type='text' class="form-control" value="{{ \Carbon\Carbon::now() }}" name = "date" />
				<span class="input-group-addon">
					<span class="glyphicon glyphicon-calendar"></span>
				</span>
			</div> --}}
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-4 col-sm-4 col-xs-12">Price Range</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<div class="col-md-4" style="padding-left: 0px!important;">				
				<input type="number" class="form-control" name = "ranges[]" step=".01" placeholder="from">
			</div>
			<div class="col-md-4">				
				<input type="number" class="form-control" name = "ranges[]" step=".01" placeholder="to">
			</div>
			<div class="col-md-4" style="padding-right: 0px!important;">				
				<input type="number" class="form-control" name = "ranges[]" step=".01" placeholder="Average">
			</div>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-4 col-sm-4 col-xs-12">Sell Price</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<div class="col-md-4" style="padding-left: 0px!important;">				
				<input type="number" class="form-control" name = "sell_prices[]" step=".01" placeholder="from">
			</div>
			<div class="col-md-4">				
				<input type="number" class="form-control" name = "sell_prices[]" step=".01" placeholder="to">
			</div>
			<div class="col-md-4" style="padding-right: 0px!important;">				
				<input type="number" class="form-control" name = "sell_prices[]" step=".01" placeholder="Average">
			</div>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-4 col-sm-4 col-xs-12">Closing Price (optional)</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<input type="number" class="form-control" name = "closing_price" step=".01">
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-4 col-sm-4 col-xs-12">Dividend</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<input type="number" class="form-control" name = "dividend" step=".01">
		</div>
	</div>
	@isset ($cr)
	<input type="hidden" name="id" value = "{{ $cr->cr_id }}">
	@endisset
	<div class="ln_solid">
	</div>
	<div class="form-group text-center">							
		<button type="submit" class="btn btn-success">{{ isset($cr) ? 'Update' : 'Add' }}</button>
	</div>					
</form>
@push('footer')
<script src="{{ asset('js/select2.min.js') }}"></script>
<script>
	function geturl(name) {
		var link = '{{ route('admin.home') }}/'+name;
		return link;
	}
	multiSelect('.stock', 'stock')
	$('.todo').select2();
	$('#{{ $ID }}Form').CRUD({
		url : '{{ route('admin.'.$ID.'.store') }}'
	});
</script>
@endpush