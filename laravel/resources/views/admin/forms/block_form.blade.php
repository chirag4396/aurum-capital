<div class="form-group">
	<form action="{{ route('admin.user.update',['id' => $user->id]) }}" method="post">
		{{ csrf_field() }}
		<input type="hidden" name="_method" value = "PUT">
		<input type="hidden" name="block" value = "{{ $user->block ? 0 : 1 }}">
		<div class="form-group">
			<textarea class="form-control" placeholder="Send some notes" rows="5" name = "note"></textarea>
		</div>
		<div class="form-group">			
			<button class="btn btn-success">{{ $user->block ? 'Un-Block' : 'Block' }} Access</button>
		</div>
	</form>
</div>