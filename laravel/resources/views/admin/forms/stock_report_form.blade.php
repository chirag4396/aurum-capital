@push('header')
<link href="{{ asset('css/select2.css') }}" rel="stylesheet" />
@endpush

<form id = "{{ $ID }}Form" class="form-horizontal form-label-left" enctype="multipart/formdata">	
	<div class="form-group">
		<label class="control-label col-md-4 col-sm-4 col-xs-12">Stock</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<select class="form-control stock" name = "stock">
				<option value="-1">--select--</option>
				@forelse (\App\Stock::where(['st_active' => 1])->get() as $st)
				<option value="{{ $st->st_id }}">{{ $st->st_symbol }}</option>
				@empty
				@endforelse
			</select>
		</div>
	</div>	
	<div class="form-group">
		<label class="control-label col-md-4 col-sm-4 col-xs-12">Title</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<input type="text" class="form-control" name = "title">
		</div>
	</div>	
	<div class="form-group">
		<label class="control-label col-md-4 col-sm-4 col-xs-12">Upload Report (PDF)</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<input type="file" class="form-control" name = "pdf">
		</div>
	</div>	
	<div class="ln_solid">
	</div>
	<div class="form-group text-center">							
		<button type="submit" class="btn btn-success">{{ isset($cr) ? 'Update' : 'Add' }}</button>
	</div>					
</form>
@push('footer')
<script src="{{ asset('js/select2.min.js') }}"></script>
<script>
	$('.stock').select2();
	$('#{{ $ID }}Form').CRUD({
		url : '{{ route('admin.'.$ID.'.store') }}'
	});
</script>
@endpush