@push('header')
@php
$ID = 'monthly-performance';
@endphp
<script>
	ID = '{{ $ID }}';
</script>
@endpush

<form id = "{{ $ID }}Form" class="form-horizontal form-label-left" enctype="multipart/formdata">
	<div class="form-group">
		<label class="control-label col-md-4 col-sm-4 col-xs-12">Date
		</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<input  type="date" class="form-control col-md-7 col-xs-12" name = "date" value="{{ $mp->mp_date or '' }}">
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-4 col-sm-4 col-xs-12">Quants Alpha
		</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<input step=".01" type="number" class="form-control col-md-7 col-xs-12" name = "dns" value="{{ $mp->mp_dns or '' }}">
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-4 col-sm-4 col-xs-12">INDEX
		</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<input type="number" step=".01" class="form-control col-md-7 col-xs-12" name = "nifty" value="{{ $mp->mp_nifty or '' }}">
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-4 col-sm-4 col-xs-12">INDEX + Quants Alpha
		</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<input step=".01" type="number" class="form-control col-md-7 col-xs-12" name = "nifty_dns" value="{{ $mp->mp_nifty_dns or '' }}">
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-4 col-sm-4 col-xs-12">FD
		</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<input step=".01" type="number" class="form-control col-md-7 col-xs-12" name = "fd" value="{{ $mp->mp_fd or '' }}">
		</div>
	</div>
	<div class="form-group">
		<label step=".01" class="control-label col-md-4 col-sm-4 col-xs-12">FD + Quants Alpha
		</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<input step=".01" type="number" class="form-control col-md-7 col-xs-12" name = "fd_dns" value="{{ $mp->mp_fd_dns or '' }}">
		</div>
	</div>
	@isset ($mp)
	    <input type="hidden" name="id" value = "{{ $mp->mp_id }}">
	@endisset
	<div class="ln_solid">
	</div>
	<div class="form-group text-center">							
		<button type="submit" class="btn btn-success">{{ isset($mp) ? 'Update' : 'Add' }}</button>
	</div>					
</form>
@push('footer')
<script>		
	$('#{{ $ID }}Form').CRUD({
		url : '{{ route('admin.'.$ID.'.store') }}'
	});
</script>
@endpush