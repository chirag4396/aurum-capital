<form id = "{{ $ID }}Form" class="form-horizontal form-label-left" enctype="multipart/formdata">	
	<div class="form-group">
		<label class="control-label col-md-4 col-sm-4 col-xs-12">Title</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<input type="text" class="form-control" name = "title" value="{{ $stock->st_title or '' }}">
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-4 col-sm-4 col-xs-12">Symbol</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<input type="text" class="form-control" name = "symbol" value="{{ $stock->st_symbol or '' }}">
		</div>
	</div>
	@isset ($stock)
	<input type="hidden" name="id" value = "{{ $stock->st_id }}">
	@endisset
	<div class="ln_solid">
	</div>
	<div class="form-group text-center">							
		<button type="submit" class="btn btn-success">{{ isset($stock) ? 'Update' : 'Add' }}</button>
	</div>					
</form>
@push('footer')
<script>
	$('#{{ $ID }}Form').CRUD({
		url : '{{ route('admin.'.$ID.'.store') }}'
	});
</script>
@endpush