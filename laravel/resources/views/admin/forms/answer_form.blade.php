<div class="form-group">
    <form id = "answerForm">
        <input type="hidden" name="user" value="{{ Auth::id() }}">
        <input type="hidden" name="question" value="{{ $qus->if_id }}">
        <input type="hidden" name="admin" value="1">        
        <div class="form-group">
            <textarea class="form-control" placeholder="Answer" rows="5" readonly>{{ $qus->if_question }}</textarea>
        </div>        
        <div class="form-group">
            <textarea class="form-control" placeholder="Answer" rows="5" name = "answer"></textarea>
        </div>
        <div class="form-group">
            <button class="btn btn-success">Answer</button>            
        </div>
    </form>
</div>

<script type="text/javascript">
    addAnswer();
</script>