@push('header')
<link href="{{ asset('css/quill.snow.css') }}" rel="stylesheet" />
@endpush

<form id = "{{ $ID }}Form" class="form-horizontal form-label-left" enctype="multipart/formdata">	
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Question</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<div id = "qus">{!! $faq->faq_qus or '' !!}</div>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Answer</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<div id = "ans">{!! $faq->faq_ans or '' !!}</div>
		</div>
	</div>
	@isset ($faq)
	<input type="hidden" name="id" value = "{{ $faq->faq_id }}">
	@endisset
	<div class="ln_solid">
	</div>
	<div class="form-group text-center">							
		<button type="submit" class="btn btn-success">{{ isset($faq) ? 'Update' : 'Add' }}</button>
	</div>					
</form>
@push('footer')
<script src="{{ asset('js/quill.min.js') }}"></script>
<script>
	var qus = new Quill('#qus', {      
		placeholder: 'Question...',    
		theme: 'snow'
	});

	var ans = new Quill('#ans', {
		placeholder: 'Answer...',    
		theme: 'snow'
	});
	$('#{{ $ID }}Form').CRUD({
		url : '{{ route('admin.'.$ID.'.store') }}',
		extraVariables : function(){
			return {
				'qus' : document.querySelector("#qus .ql-editor").innerHTML,
				'ans' : document.querySelector("#ans .ql-editor").innerHTML
			};
		}
	});
</script>
@endpush