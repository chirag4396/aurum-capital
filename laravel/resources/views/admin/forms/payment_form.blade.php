<div class="form-group">
	<form action="{{ route('admin.user.update',['id' => $user->id]) }}" method="post">
		{{ csrf_field() }}
		<input type="hidden" name="_method" value = "PUT">
		<input type="hidden" name="paid" value="{{ $user->paid ? 0 : 1 }}">
		@php
		if($user->lastSubscriptionLog()->count()) {
			$sl = $user->lastSubscriptionLog->first()->subscription;
		}
		@endphp
		@isset ($sl)		    
			<input type="hidden" name="subscription" value="{{ $sl->sub_id }}">
		@endisset
		<div class="form-group">
			<input type="number" name="amount" class="form-control" placeholder="Enter amount recieved" value = "{{ $sl->sub_amount or 25000 }}">
		</div>
		<div class="form-group">
			<label class="pull-left">Year of Subscription</label>
			<select class="form-control" name="year">
				<option value = "1" {{ isset($sl) ? ($sl->sub_year == '1' ? 'selected' : '') : '' }}>1 Year</option>
				<option value = "2" {{ isset($sl) ? ($sl->sub_year == '2' ? 'selected' : '') : '' }}>2 Year</option>
			</select>			
		</div>
		<div class="form-group">
			<textarea class="form-control" placeholder="Send some notes" rows="5" name = "note"></textarea>
		</div>
		<div class="form-group">
			@if ($user->paid)
			<button class="btn btn-danger">Cancel Subscription</button>
			@else
				@if($user->lastSubscriptionLog()->count())
					<button class="btn btn-success">Continue Subscription</button>
				@else
					<button class="btn btn-success">Approve Subscription</button>
				@endif
			@endif
		</div>
	</form>
</div>