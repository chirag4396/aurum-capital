<div class="form-group">
    <table id="forum-table" class="table table-striped table-bordered">
        <thead>
            <tr>
                <th>Answer</th>
                <th>User</th>
                <th>Date</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($if->answers()->get() as $ans)
            <tr>
                <td>{{ $ans->fa_answer }}</td>
                <td>{{ $ans->user->name }}</td>
                <td>{{ $ans->fa_created_at->format('jS M, Y - h:i A') }}</td>
                <td>
                    <form action="{{ route('admin.forum-answer.update', ['id' => $ans->fa_id]) }}" method="post">
                        <input type="hidden" name="_method" value="PUT">
                        <input type="hidden" name="approve" value="{{ $ans->fa_approve ? 0 : 1 }}">
                        {{ csrf_field() }}
                        @if ($ans->fa_approve)
                            <button class="btn btn-sm btn-danger">Deny</button>
                        @else
                            <button class="btn btn-sm btn-success">Approve</button>
                        @endif
                    </form>
                </td>
            </tr>
            @empty

            @endforelse
        </tbody>
    </table>
</div>