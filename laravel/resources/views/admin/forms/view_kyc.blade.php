<form id = "rejectForm" action="{{ route('admin.kyc.update',['id' => $kyc->kyc_id]) }}" method="post">
	<div class="text-center">
		<div class="form-group">
			<label>PAN Number: {{ $kyc->kyc_number }}</label>
		</div>
		<div class="form-group">	
			<a href="{{ asset($kyc->kyc_image) }}" download><img src="{{ asset($kyc->kyc_image) }}" style="max-width: 500px"></a>
		</div>
		@if (!$kyc->kyc_reject)
		<div class="form-group">
			{{ csrf_field() }}
			<input type="hidden" name="_method" value = "PUT">
			<input type="hidden" name="status" value="{{ $kyc->kyc_status ? 0 : 1 }}">		
			<button class="btn btn-success" name = "approve" value="approve">Approve It</button>		
			<button type = "button" class="btn btn-danger" id = "rejectBtn">Reject</button>			
		</div>
		@endif
	</div>
	<div id = "reasonBox" class="hidden form-group">
		<fieldset>			
			<legend>Reasons</legend>
			@forelse (\App\RejectReason::where(['rr_default' => 1])->get() as $rr)
			<div class="form-group">
				<input type="checkbox" name="reason[]" value="{{ $rr->rr_id }}" id = "{{ $rr->rr_id }}"> <label for = "{{ $rr->rr_id }}">{{ $rr->rr_title }}</label>
			</div>
			@empty				
			@endforelse
			<div class="form-group">
				<input type="checkbox" id = "other"> <label for = "other">Other</label>
			</div>
			<div class="form-group">
				<textarea name="new_reason" id = "otherBox" placeholder="Personalise reason" class="hidden form-control" rows="3"></textarea>
			</div>
			<div class="form-group">
				<button class="btn btn-danger" name = "reject" value="reject">Submit</button>
			</div>
		</fieldset>
	</div>
</form>

<script type="text/javascript">
	others();
</script>