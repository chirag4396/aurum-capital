<form id = "deleteForm" method = "post">
	{{ csrf_field() }}
	<input type="hidden" name="id" value = "">
	<input type="hidden" name="_method" value = "DELETE">
	<button type = "submit" class="btn btn-danger">Yes</button>
	<button type = "button" class="btn btn-success" data-dismiss="modal">No</button>
</form>