<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
   <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
   <title>Aurum Capital</title>
   <style type="text/css">
   /* Client-specific Styles */
   #outlook a {padding:0;} /* Force Outlook to provide a "view in browser" menu link. */
   body{width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;}
   /* Prevent Webkit and Windows Mobile platforms from changing default font sizes, while not breaking desktop design. */
   .ExternalClass {width:100%;} /* Force Hotmail to display emails at full width */
   .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;} /* Force Hotmail to display normal line spacing.  */
   #backgroundTable {margin:0; padding:0; width:100% !important; line-height: 100% !important;}
   img {outline:none; text-decoration:none;border:none; -ms-interpolation-mode: bicubic;}
   a img {border:none;}
   .image_fix {display:block;}
   p {margin: 0px 0px !important;}
   table td {border-collapse: collapse;}
   table { border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; }
   a {color: #9ec459;text-decoration: none;text-decoration:none!important;}
   /*STYLES*/
   table[class=full] { width: 100%; clear: both; }
   /*IPAD STYLES*/
   @media only screen and (max-width: 640px) {
      a[href^="tel"], a[href^="sms"] {
         text-decoration: none;
         color: #9ec459; /* or whatever your want */
         pointer-events: none;
         cursor: default;
      }
      .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
         text-decoration: default;
         color: #9ec459 !important;
         pointer-events: auto;
         cursor: default;
      }
      table[class=devicewidth] {width: 440px!important;text-align:center!important;}
      td[class=devicewidth] {width: 440px!important;text-align:center!important;}
      img[class=devicewidth] {width: 440px!important;text-align:center!important;}
      img[class=banner] {width: 440px!important;height:147px!important;}
      table[class=devicewidthinner] {width: 420px!important;text-align:center!important;}
      table[class=icontext] {width: 345px!important;text-align:center!important;}
      img[class="colimg2"] {width:420px!important;height:243px!important;}
      table[class="emhide"]{display: none!important;}
      /*img[class="logo"]{width:440px!important;height:110px!important;}*/

   }
   /*IPHONE STYLES*/
   @media only screen and (max-width: 480px) {
      a[href^="tel"], a[href^="sms"] {
         text-decoration: none;
         color: #9ec459; /* or whatever your want */
         pointer-events: none;
         cursor: default;
      }
      .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
         text-decoration: default;
         color: #9ec459 !important; 
         pointer-events: auto;
         cursor: default;
      }
      table[class=devicewidth] {width: 280px!important;text-align:center!important;}
      td[class=devicewidth] {width: 280px!important;text-align:center!important;}
      img[class=devicewidth] {width: 280px!important;text-align:center!important;}
      img[class=banner] {width: 280px!important;height:93px!important;}
      table[class=devicewidthinner] {width: 260px!important;text-align:center!important;}
      table[class=icontext] {width: 186px!important;text-align:center!important;}
      img[class="colimg2"] {width:260px!important;height:150px!important;}
      table[class="emhide"]{display: none!important;}
      /*img[class="logo"]{width:280px!important;height:70px!important;}*/

   }
</style>
</head>
<body>
   <!-- Start of LOGO -->
   <table width="100%" bgcolor="#e8eaed" cellpadding="0" cellspacing="0" border="0" id="backgroundTable">
      <tbody>
         <tr>
            <td>
               <table width="80%" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                  <tbody>
                     <tr>
                        <td width="100%">
                           <table bgcolor="#e8eaed" width="80%" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth">
                              <tbody>
                                 <tr>
                                    <!-- start of image -->
                                    <td align="center" style="background-color: #fff;">
                                       <a target="_blank" href="#"><img border="0" alt="" border="0" style="display:block; border:none; outline:none; text-decoration:none; width: auto; height: auto; margin-top: 20px;" src="{{ asset('img/logo.png') }}" class="logo"></a>
                                    </td>
                                 </tr>
                              </tbody>
                           </table>
                           <!-- end of image -->
                        </td>
                     </tr>
                  </tbody>
               </table>
            </td>
         </tr>
      </tbody>
   </table>
</td>
</tr>
</tbody>
</table>
<!-- End of LOGO -->    
<!-- start textbox-with-title -->
<table width="100%" bgcolor="#e8eaed" cellpadding="0" cellspacing="0" border="0" id="backgroundTable">
   <tbody>
      <tr>
         <td>
            <table width="80%" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
               <tbody>
                  <tr>
                     <td width="100%">
                        <table bgcolor="#ffffff" width="80%" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                           <tbody>
                              <!-- Spacing -->
                              <tr>
                                 <td width="100%" height="20"></td>
                              </tr>
                              <!-- Spacing -->
                              <tr>
                                 <td>
                                    <table width="80%" align="center" cellpadding="0" cellspacing="0" border="0" class="devicewidthinner">
                                       <tbody>  
                                          <!-- content -->
                                          <tr>
                                             <td style="font-family: Helvetica, arial, sans-serif; font-size: 18px; color: #333333; text-align:center;line-height: 24px;">
                                                {!! $msg !!}
                                             </td>
                                          </tr>
                                          @isset ($note)
                                          <tr>
                                             <td width="100%" height="5"></td>
                                          </tr>
                                          <tr>
                                             <td style="font-family: Helvetica, arial, sans-serif; font-size: 18px; color: #333333; text-align:center;line-height: 24px;">
                                                {!! $note !!}
                                             </td>
                                          </tr>
                                          @endisset
                                          <!-- End of content -->
                                          @isset ($route)                                          
                                          <!-- Spacing -->
                                          <tr>
                                             <td width="100%" height="5"></td>
                                          </tr>
                                          <!-- Spacing -->
                                          <!-- button -->
                                          <tr>
                                             <td style="font-family: Helvetica, arial, sans-serif; font-size: 14px; font-weight:bold; color: #333333; text-align:left;line-height: 24px;">
                                                <a href="{{ $route }}" style="color: #fff; text-decoration: none; font-weight: bold; display: block; background-color: #3860ca; width: 200px; margin: 0 auto; text-align: center; padding: 5px 0; margin-top: 20px;">Check</a>
                                             </td>
                                          </tr>
                                          <!-- /button -->
                                          @endisset
                                          
                                          <!-- Spacing -->
                                          <tr>
                                             <td width="100%" height="20"></td>
                                          </tr>
                                          <!-- Spacing -->
                                          
                                       </tbody>
                                    </table>
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                     </td>
                  </tr>
               </tbody>
            </table>
         </td>
      </tr>
   </tbody>
</table>
<!-- end of textbox-with-title -->

<!-- Start of postfooter -->
<table width="100%" bgcolor="#202020" cellpadding="0" cellspacing="0" border="0" id="backgroundTable">
   <tbody>
      <tr>
         <td>
            <table width="80%" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
               <tbody>
                  <tr>
                     <td width="100%">
                        <table width="80%" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                           <tbody>
                              <!-- Spacing -->
                              <tr>
                                 <td width="100%" height="20"></td>
                              </tr>
                              <!-- Spacing -->
                              <tr>
                                 <td align="center" valign="middle" style="font-family: Helvetica, arial, sans-serif; font-size: 13px;color: #ffffff">
                                    You have received this email because you have subscribed to {{ config('app.email') }}
                                 </td>
                              </tr>
                              <!-- Spacing -->
                              <tr>
                                 <td width="100%" height="20"></td>
                              </tr>
                              <!-- Spacing -->
                           </tbody>
                        </table>
                     </td>
                  </tr>
               </tbody>
            </table>
         </td>
      </tr>
   </tbody>
</table>
<!-- End of postfooter -->
</body>
</html>