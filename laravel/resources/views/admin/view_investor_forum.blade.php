@extends('admin.layouts.master')

@section('title')
    All Forum Questions
@endsection

@push('header')
    <!-- Datatables -->


    {{-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css"> --}}

    <link href="{{ asset('admin-assets/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('admin-assets/datatables.net-buttons-bs/css/buttons.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('admin-assets/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('admin-assets/datatables.net-responsive-bs/css/responsive.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('admin-assets/datatables.net-scroller-bs/css/scroller.bootstrap.min.css') }}" rel="stylesheet">
    @php
        $ID = 'forum';
    @endphp
@endpush

@section('content')

    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>All Forum Questions</h3>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_content">
                            <table id="forum-table" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>Question</th>
                                    <th>User</th>
                                    <th>Date</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="{{ $ID }}ViewModal" tabindex="-1" role="dialog" aria-labelledby="Modal" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="{{ $ID }}ModalLabel">View {{ $ID }}</h4>
                        </div>
                        <div class="modal-body" id = "viewData">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('footer')


    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>



    <script src="{{ asset('admin-assets/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') }}"></script>

    <script src="{{ asset('admin-assets/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js') }}"></script>
    <script src="{{ asset('admin-assets/datatables.net-keytable/js/dataTables.keyTable.min.js') }}"></script>
    <script src="{{ asset('admin-assets/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('admin-assets/datatables.net-responsive-bs/js/responsive.bootstrap.js') }}"></script>
    <script src="{{ asset('admin-assets/datatables.net-scroller/js/dataTables.scroller.min.js') }}"></script>

    <script src="{{ asset('admin-assets/jszip/dist/jszip.min.js') }}"></script>

    <script src="{{ asset('admin-assets/pdfmake/build/pdfmake.min.js') }}"></script>
    <script src="{{ asset('admin-assets/pdfmake/build/vfs_fonts.js') }}"></script>

    <script type="text/javascript">

        {{-- Table.init(ID); --}}

        function viewDetail(id, url) {
            $('#{{ $ID }}ViewModal').modal('toggle');
            $('#viewData').html('<div class="text-center">Loading...</div>');
            $.post(url, {id : id}, function( data ) {
                $('#viewData').html(data);
            });
        }

        $(function() {
            $('#forum-table').DataTable({
                processing: true,
                serverSide: true,
                pageLength : 50,
                ajax: {
                    url : '{!! route('admin.all-forums') !!}'
                },
                columns: [
                    { data: 'if_question', name: 'if_question' },
                    { data: 'name', name: 'name' },
                    { data: 'if_created_at', name: 'if_created_at', "orderable": false },
                    { data: 'action', name: 'action',"orderable": false }
                ],
                dom: "Bfrtip",
                buttons: [
                    {
                        extend: "excel",
                        className: "btn-sm",
                        exportOptions: {
                            columns: [0,1,2]
                        }
                    },
                    {
                        extend: "pdfHtml5",
                        className: "btn-sm",
                        exportOptions: {
                            columns: [0,1,2]
                        }
                    }
                ],
                responsive: true,
            });
        });
        function addAnswer(){
            $('#answerForm').CRUD({
                url : '{{ route('admin.forum-answer.store') }}'
            });
        }
    </script>
@endpush