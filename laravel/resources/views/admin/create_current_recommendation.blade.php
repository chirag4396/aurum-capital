@extends('admin.layouts.master')
@section('title')
Create New Recommendation
@endsection

@php
	$ID = 'current-recommendation';
@endphp
@push('header')
	<link href="{{ asset('css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" media="screen">
@endpush
@section('content')
<div class="right_col" role="main">	
	<div class="page-title">
		<div class="title_left">
			<h3>Create New Recommendation</h3>
		</div>
		<div class="pull-right">
			<a href = "{{ route('admin.'.$ID.'.index') }}" class="btn btn-danger">Back</a>
		</div>
	</div>
	<div class="clearfix">
	</div>
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">				
				<div class="x_content">
					@include('admin.forms.current_recommendation_form', ['ID' => $ID])
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@push('footer')
	<script type="text/javascript" src = "{{ asset('js/bootstrap-datetimepicker.js') }}"></script>
	<script type="text/javascript">
		$('#datetimepicker1').datetimepicker({
			todayHighlight:'TRUE',
			autoclose: true,
			minDate: '0',
			maxDate: '+1Y+6M'
		});
	</script>
@endpush