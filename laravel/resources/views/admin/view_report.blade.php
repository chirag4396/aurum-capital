@extends('admin.layouts.master')

@section('title')
@endsection
@push('header')
<!-- Datatables -->
<link href="{{ asset('admin-assets/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('admin-assets/datatables.net-buttons-bs/css/buttons.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('admin-assets/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('admin-assets/datatables.net-responsive-bs/css/responsive.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('admin-assets/datatables.net-scroller-bs/css/scroller.bootstrap.min.css') }}" rel="stylesheet">
@php
$ID = '';
function rs($value){  
  return '&#8377; '.number_format($value,2);
}

function convertDate($value)
{
  return \Carbon\Carbon::parse($value)->format('jS M Y');
}

$months = ['','Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

$now = \Carbon\Carbon::now()->format('m');
$cYear = \Carbon\Carbon::now()->format('Y');
@endphp
@endpush

@section('content')

<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="{{ (Auth::user()->type == 101) ? '' : 'title_left' }}">
        <h3>Weekly Report of {{ $user->name }} 
          @if (Auth::user()->type == 101)          
          <a href = "{{ route('admin.import', ['id' => $user->id]) }}" class="btn btn-primary pull-right"><i class="fa fa-pencil"></i> Upload Excel</a>
          @endif
        </h3>
      </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">          
          <div class="x_content">
            <!-- start accordion -->
            <div class="x_content">
              <div class="col-md-6 table-responsive">
                <table class="table table-striped table-bordered">
                  <tr>
                    <th>Client Name</th>
                    <td>{{ $user->name }}</td>
                  </tr>            
                  <tr>
                    <th>Date of inception</th>
                    <td>{{ \Carbon\Carbon::parse($user->detail->ud_date_inception)->format('M-Y') }}</td>                
                  </tr>
                  <tr>
                    <th>Client ID</th>
                    <td>{{ $user->detail->ud_client_id }}</td>
                  </tr>
                  <tr>
                    <th>Portfolio Size</th>
                    <td>{!! rs($user->detail->ud_portfolio_size) !!}</td>                    
                  </tr>                
                </table>
              </div>
              <div class="clearfix"></div>
              <hr>
              <h4>Reports</h4>
              <div class="x_panel">                
                <div class="x_content">
                  <!-- start accordion -->
                  <div class="accordion" id="accordion1" role="tablist" aria-multiselectable="true">
                    @forelse ($years as $yk => $year)                  
                    <div class="panel">
                      <a class="panel-heading" role="tab" data-toggle="collapse" data-parent="#accordion1" href="#year{{ $year->year }}" aria-expanded="true" aria-controls="collapseOne">
                        <h4 class="panel-title">{{ $year->year }}</h4>
                      </a>
                      <div id="year{{ $year->year }}" class="panel-collapse collapse {{ $year->year == $cYear ? 'in' : '' }}" role="tabpanel" aria-labelledby="headingOne" aria-expanded="true" style="">
                        <div class="panel-body">
                          <div class="x_content">
                            <div class="" role="tabpanel" data-example-id="togglable-tabs">
                              <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                                @forelse ($months as $mk => $month)
                                @if ($mk)
                                <li role="presentation" class="{{ $mk == $now ? 'active' : '' }}"><a href="#{{ $mk.$year->year }}" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">{{ $month }}</a></li>                                
                                @endif
                                @empty
                                @endforelse
                              </ul>
                              <div class="tab-content">
                                @forelse ($months as $mkt => $month)
                                @if ($mkt)
                                <div role="tabpanel" class="tab-pane fade {{ $mkt == $now ? 'active in' : '' }}" id="{{ $mkt.$year->year }}" aria-labelledby="home-tab">
                                 <div class="col-xs-2">
                                   <!-- required for floating -->
                                   <!-- Nav tabs -->
                                   <ul class="nav nav-tabs tabs-left">
                                     @forelse ($user->reports($mkt, $year->year)->get() as $k => $report)
                                     <li {!! !$k ? 'class="active"' : '' !!}><a href="#report{{ $report->rep_id }}" data-toggle="tab">{{ convertDate($report->rep_date) }}</a></li>
                                     @empty
                                     @endforelse
                                   </ul>
                                 </div>

                                 <div class="col-xs-10">
                                   <!-- Tab panes -->
                                   <div class="tab-content">
                                     @forelse ($user->reports($mkt, $year->year)->get() as $i => $report)                  
                                     <div class="tab-pane {{ !$i ? 'active' : '' }}" id="report{{ $report->rep_id }}">
                                       <div class="col-md-12 table-responsive">
                                         <div class="col-md-6 alert-success">
                                           <h4>Booked P&L Till Date:{!! rs($report->rep_booked_p_l) !!}</h4>
                                         </div>
                                         <div class="clearfix"></div>
                                         <hr>
                                         <h4>NIFTY & Strategy Report</h4>
                                         @php
                                         $nifty = $report->nifty;
                                         $strategy = $report->strategy;
                                         @endphp
                                         <table class="table table-striped table-bordered">
                                           <thead>
                                             <th colspan="2">NIFTY Report</th>
                                             <th colspan="2">Strategy Report</th>
                                           </thead>
                                           <tbody>
                                             <tr>
                                               <td>Nifty Closing Previous Week</td>
                                               <td>{!! rs($nifty->nr_closing_previous) !!}</td>
                                               <td>Margin Deployed</td>
                                               <td>{!! rs($strategy->st_margin_deployed) !!}</td>
                                             </tr>
                                             <tr>
                                               <td>NIFTY Current Week</td>
                                               <td>{!! rs($nifty->nr_current_week) !!}</td>
                                               <td>Current Profit & Loss of Open Position </td>
                                               <td>{!! rs($strategy->st_current_p_n_l) !!}</td>
                                             </tr>
                                             <tr>
                                               <td>% Return from last week</td>
                                               <td>{{ $nifty->nr_percent_return_last_week.'%' }}</td>
                                               <td>% Return on Open Position</td>
                                               <td>{{ $strategy->st_per_on_open_position.'%' }}</td>
                                             </tr>
                                             <tr>
                                               <td>NIFTY at the time of Inception</td>
                                               <td>{!! rs($nifty->nr_time_of_inception) !!}</td>
                                               <td>Net Gain Till Date</td>
                                               <td>{!! rs($strategy->st_net_gain) !!}</td>
                                             </tr>
                                             <tr>
                                               <td>% Return on NIFTY</td>
                                               <td>{{ $nifty->nr_percent_return.'%' }}</td>
                                               <td>% Additional return on overall Portfolio (till today)</td>
                                               <td>{{ $strategy->st_per_overall_portfolio.'%' }}</td>
                                             </tr>
                                           </tbody>
                                         </table>
                                       </div>
                                       <div class="col-md-12 table-responsive">
                                         <h4>Open Positions</h4>
                                         <table class="table table-striped table-bordered">
                                           <thead>
                                             <th>Daysleft</th>
                                             <th>Ser/Exp</th>
                                             <th>Strike Price</th>
                                             <th>Option Type</th>
                                             <th>Units</th>
                                             <th>Trade Price</th>
                                             <th>Theoritical Price</th>
                                             <th>RealizedIV</th>
                                             <th>Last IV</th>
                                             <th>LTP</th>
                                           </thead>
                                           <tbody>
                                             @forelse ($report->openPositions()->get() as $op)
                                             <tr>
                                               <td>{{ $op->op_days }}</td>
                                               <td>{{ convertDate($op->op_ser_exp) }}</td>
                                               <td>{!! rs($op->op_strike_price) !!}</td>
                                               <td>{{ $op->op_option_type }}</td>
                                               <td>{{ number_format($op->op_units,2) }}</td>
                                               <td>{!! rs($op->op_trade_price) !!}</td>
                                               <td>{!! rs($op->op_theoritical_price) !!}</td>
                                               <td>{{ number_format($op->op_realized_iv,2) }}</td>
                                               <td>{{ number_format($op->op_last_iv,2) }}</td>
                                               <td>{{ number_format($op->op_ltp,2) }}</td>
                                             </tr>
                                             @empty

                                             @endforelse
                                           </tbody>
                                         </table>
                                       </div>
                                     </div>
                                     @empty
                                     @endforelse
                                   </div>
                                 </div>
                               </div>
                               @endif
                               @empty

                               @endforelse                                
                             </div>
                           </div>

                         </div>
                       </div>
                     </div>
                   </div>
                   @empty

                   @endforelse
                 </div>
                 <!-- end of accordion -->


               </div>
             </div>             

             <div class="clearfix"></div>

           </div>           
           <!-- end of accordion -->
         </div>
       </div>      
     </div>
   </div>    
 </div>
</div>

@endsection

@push('footer')
<script src="{{ asset('admin-assets/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-buttons/js/buttons.flash.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
{{-- <script src="{{ asset('admin-assets/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js') }}"></script> --}}
<script src="{{ asset('admin-assets/datatables.net-keytable/js/dataTables.keyTable.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-responsive-bs/js/responsive.bootstrap.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-scroller/js/dataTables.scroller.min.js') }}"></script>
<script src="{{ asset('admin-assets/jszip/dist/jszip.min.js') }}"></script>
<script src="{{ asset('admin-assets/pdfmake/build/pdfmake.min.js') }}"></script>
<script src="{{ asset('admin-assets/pdfmake/build/vfs_fonts.js') }}"></script>

<script type="text/javascript">  

  Table.init(ID);


</script>
@endpush