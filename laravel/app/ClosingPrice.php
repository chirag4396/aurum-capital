<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClosingPrice extends Model
{
    public $primaryKey = 'cp_id';

    protected $fillable = ['cp_value', 'cp_stock'];

    CONST CREATED_AT = 'cp_created_at';
    
    CONST UPDATED_AT = null;
}
