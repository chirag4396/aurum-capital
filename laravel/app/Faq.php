<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
	public $primaryKey = 'faq_id';

	protected $fillable = ['faq_qus', 'faq_ans', 'faq_soft_delete'];

	public $timestamps = false;	
}
