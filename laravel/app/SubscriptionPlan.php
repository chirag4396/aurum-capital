<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubscriptionPlan extends Model
{
	protected $primaryKey = 'sp_id';

	protected $fillable = ['sp_title', 'sp_price'];

	public $timestamps = false;    
}
