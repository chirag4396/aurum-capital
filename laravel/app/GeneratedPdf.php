<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GeneratedPdf extends Model
{
    public $primaryKey = 'gp_id';

    protected $fillable = [
        'gp_path', 'gp_created_at' ,'gp_user', 'gp_delete'
    ];

    CONST CREATED_AT = 'gp_created_at';

    CONST UPDATED_AT = null;
}
