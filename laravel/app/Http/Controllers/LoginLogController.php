<?php

namespace App\Http\Controllers;

use App\LoginLog;
use App\User;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class LoginLogController extends Controller
{
    public function allLogs(Request $r) {        
        $logs = LoginLog::where('ll_user', $r->id)->get();

        return Datatables::of($logs)->editColumn('ll_created_at', function ($log) {
            return $log->ll_created_at->format('jS M, Y - h:i A');                
        })->make(true);   
    }

    public function allLogsUsers() {
        $users = User::where('type', 102)->get();

        return Datatables::of($users)->addColumn('action', function ($user) {
            return '<a class="btn btn-success btn-sm" href = "'.route('admin.logs', ['id' => $user->id]).'"><i class="fa fa-eye"></i> View Logs</a>| <button class="btn btn-'.($user->block ? 'success' : 'danger').'" onclick="viewDetail(\''.$user->id.'\', \''.$user->name.'\', \''.($user->block ? 'Un-Block' : 'Block').'\');">'.($user->block ? 'Un-Block' : 'Block').'</button>';
        })->make(true);   
    }

    public function blockForm(Request $r){
        return view('admin.forms.block_form')->with(['user' => User::find($r->id)]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

    }
    
    public function logs($id) {
        return view('admin.view_login_logs')->with(['user' => User::find($id)]);
    }
    
    public function viewLogUser() {
        return view('admin.view_login_logs_users');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\LoginLog  $loginLog
     * @return \Illuminate\Http\Response
     */
    public function show(LoginLog $loginLog)
    {
        return $loginLog;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\LoginLog  $loginLog
     * @return \Illuminate\Http\Response
     */
    public function edit(LoginLog $loginLog)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\LoginLog  $loginLog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LoginLog $loginLog)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\LoginLog  $loginLog
     * @return \Illuminate\Http\Response
     */
    public function destroy(LoginLog $loginLog)
    {
        //
    }
}
