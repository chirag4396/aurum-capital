<?php

namespace App\Http\Controllers;

use App\StockReport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Traits\GetData;
use App\Ajaxray\PHPWatermark\Watermark;

class StockReportController extends Controller
{
    use GetData;

    protected $res = ['msg' => 'error'];

    protected $filename;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('admin.create_stock_report');
    }
    
    public function viewReport($id, $name){
        $report = StockReport::find($id);
        $report->sr_pdf_path = $this->convert($report->sr_pdf_path);

        return view('users.view_report')->with(['report' => $report]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r) {

        $sr = $this->changeKeys('sr_' , $r->all());
        $r->title = ucwords($r->title);
        $title = strtolower($r->title).'-'.uniqid();
        $path = 'pdfs/reports';
        $this->filename = $title.'.'.$r->file('pdf')->getClientOriginalExtension();
        $r->file('pdf')->move($path, $this->filename);

        $sr['sr_pdf_path'] = $path.'/'.$this->filename;

        $stockReport = StockReport::create($sr);

        if ($stockReport) {
            $this->res['msg'] = 'success';
        }

        return $this->res;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\StockReport  $stockReport
     * @return \Illuminate\Http\Response
     */
    public function show(StockReport $stockReport)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\StockReport  $stockReport
     * @return \Illuminate\Http\Response
     */
    public function edit(StockReport $stockReport)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\StockReport  $stockReport
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StockReport $stockReport)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\StockReport  $stockReport
     * @return \Illuminate\Http\Response
     */
    public function destroy(StockReport $stockReport)
    {
        //
    }

    public function water($path){
        // Initiate with source image or pdf
        $watermark = new Watermark($path);

        // Customize some options (See list of supported options below)
        $watermark->setFontSize(80)
            ->setRotate(320)
            ->setOpacity(.3);
        $text = Auth::user()->email."\n".Auth::user()->mobile;
        // Watermark with Text
        $watermark->withText($text, $path);

    }


    public function convert($pathToPdf){
        $pdf = new \Spatie\PdfToImage\Pdf($pathToPdf);
        $pages = $pdf->getNumberOfPages();

        $images = [];
        for ($i = 1; $i <= $pages; $i++){
            $pdf->setPage($i)->setOutputFormat('png')->setCompressionQuality(100)->saveImage('pdfs/images/');
            $this->water('pdfs/images/'.$i.'.png');
            $images[] = 'pdfs/images/'.$i.'.png';
        }
        $temp = 'pdfs/temp/'.$this->filename;
        $img = new \Imagick($images);
        $img->setImageFormat('pdf');
        $img->writeImages($temp , true);
        return $temp;

    }
}
