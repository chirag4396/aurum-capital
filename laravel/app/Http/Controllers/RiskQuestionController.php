<?php

namespace App\Http\Controllers;

use App\RiskQuestion;
use Illuminate\Http\Request;

class RiskQuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RiskQuestion  $riskQuestion
     * @return \Illuminate\Http\Response
     */
    public function show(RiskQuestion $riskQuestion)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RiskQuestion  $riskQuestion
     * @return \Illuminate\Http\Response
     */
    public function edit(RiskQuestion $riskQuestion)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RiskQuestion  $riskQuestion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RiskQuestion $riskQuestion)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RiskQuestion  $riskQuestion
     * @return \Illuminate\Http\Response
     */
    public function destroy(RiskQuestion $riskQuestion)
    {
        //
    }
}
