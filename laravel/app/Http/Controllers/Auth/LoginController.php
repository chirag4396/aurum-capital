<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\LoginLog;
use App\User;
use App\Http\Traits\GetData;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers, GetData;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    protected function authenticated(Request $request, $user) {
        if ($request->ajax()) {
            if($user){
                LoginLog::create([
                    'll_user' => $user->id,
                    'll_ip' => $this->checkIp(),
//                    'll_status' => 0
                ]);
                $res['msg'] = 'successLogin';
                return $res;
            }
        }
    }

    protected function sendFailedLoginResponse(Request $request) {
        if ($request->ajax()) {
            if(!$this->checkBlock($request)){       
                if($this->checkAlreadyLoggedIn($request)){
                    $res['msg'] = 'loginSame';
                }else{
                    $res['msg'] = 'errorLogin';
                }
            }else{
                $res['msg'] = 'block';
            }
            return $res;
        }

        throw ValidationException::withMessages([
            $this->username() => [trans('auth.failed')],
        ]);
    }

    protected function attemptLogin(Request $request)
    {
        
        if ($request->ajax()) {            
            if(!$this->checkBlock($request)){
                if($this->checkAlreadyLoggedIn($request)){
                    $this->sendFailedLoginResponse($request);
                }else{
                    return $this->guard()->attempt(
                        $this->credentials($request), $request->filled('remember')
                    );
                }
            }else{
                $this->sendFailedLoginResponse($request);
            }
        }else{        
            return $this->guard()->attempt(
                $this->credentials($request), $request->filled('remember')
            );
        }
    }

    protected function checkAlreadyLoggedIn(Request $request){
        return !is_null($this->findLoginLog($this->findUser($request)->id));
    }
    
    protected function findUser(Request $request){
        return User::where($this->username(), $request->email)->first();        
    }
    
    protected function findLoginLog($id) {
        return LoginLog::where(['ll_status' => 1, 'll_user' => $id])->first();
    }
    
    protected function checkBlock(Request $request){
        return $this->findUser($request)->block;
    }

    public function logout(Request $request) {
        $redirect = (Auth::user()->type == 101) ? '/admin' : '/';
        $pre = $this->findLoginLog(Auth::id());
        if($pre){
            $pre->update(['ll_status' => 0]);
        }
        
        $this->guard()->logout();

        $request->session()->invalidate();

        return redirect($redirect);
    }
}
