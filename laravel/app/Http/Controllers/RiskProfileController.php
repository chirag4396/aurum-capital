<?php

namespace App\Http\Controllers;

use App\RiskProfile;
use App\RiskQuestion;
use App\RiskOption;
use App\RiskProfileAnswer;
use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use App\Http\Traits\GetData;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\User;
use App\Events\EmailEvent;

class RiskProfileController extends Controller
{
    use GetData;    
    
    public function upload($id, $edit = null) {
        $user = User::find($id);
        if(Auth::id() == $id) {
            if(Auth::user()->type == 102){                
                if(!Auth::user()->paid) {
                    return redirect()->to('services');
                }
                return view('users.risk_profile')->with(['risk_questions' => RiskQuestion::get(), 'edit' => $edit]);
            }
            return redirect()->to('/')->with(['page_error' => 'You are not allowed to upload kyc!!']);
        }
        return redirect()->to('/')->with(['page_error' => 'Please login with '.$user->name.'!!']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function allRiskProfile($id){
        return view('admin.view_risk_profiles')->with(['riskProfiles' => RiskProfile::where('rp_user', $id)->orderBy('rp_created_at', 'desc')->get(), 'user' => User::find($id)]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r) {
        $user = Auth::user();
        
        $emailSet['to'] = ['email' => $user->email,'name' => $user->name];
        $emailSet['blade'] = 'normal';

        $d = $this->removePrefix($r->all(),'op');
        $new = [];
        $total = 0;
        $rp = RiskProfile::create([
            'rp_user' => $user->id
        ]);
        foreach ($d as $key => $value) {
            if($key > 1){            
                $point = RiskOption::find($value)->ro_points;
            }else{
                $date = Carbon::parse($value);
                $now = Carbon::now();

                $diff = $now->diffInYears($date);
                if ($diff <= 30) {
                    $point = 10;
                    $value = 4;
                }else if ($diff > 30 && $diff < 46){
                    $point = 7;
                    $value = 3;
                }else if ($diff > 47 && $diff < 61){
                    $point = 5;
                    $value = 2;
                }else{
                    $point = 1;
                    $value = 1;
                }                
            }
            $new[] = [
                'rpa_qus' => $key,
                'rpa_ans' => $value,
                'rpa_point' => $point,
                'rpa_rp' => $rp->rp_id
            ];
            $total +=$point;
        }

        if(RiskProfileAnswer::insert($new)){
            $res = ($total >= 50) ? 'High' : (($total>= 25 && $total < 50) ? 'Medium' : 'Low');
            $rp->update(['rp_score' => $total, 'rp_dob' => $date]);

            $emailSet['data'] = [
                'msg' => __('email.risk_profile_recieved', ['score' => $res ]),
                'route' => route('risk-result', ['id' => $user->id])
            ];

            $emailSet['subject'] = ($user->risk ? __('email.risk_profile_update_subject') : __('email.risk_profile_complete_subject'));

            $user->update(['risk' => 1]);
            
            $this->res['msg'] = 'success';

            $this->res['location'] = route('risk-result', ['id' => $user->id]);
        }
        event(new EmailEvent($emailSet));
        return $this->res;
    }

    public function result($id){
        if (Auth::id() != $id) {
            return redirect()->to('/');
        }
        return view('users.risk_result');
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\RiskProfile  $riskProfile
     * @return \Illuminate\Http\Response
     */
    public function show(RiskProfile $riskProfile)
    {
        // return $riskProfile->user;
        return view('admin.forms.view_risk_profile_details')->with(['riskAns' => $riskProfile->answers()->get(), 'rp' => $riskProfile,'risk_questions' => RiskQuestion::get()]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RiskProfile  $riskProfile
     * @return \Illuminate\Http\Response
     */
    public function edit(RiskProfile $riskProfile)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RiskProfile  $riskProfile
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RiskProfile $riskProfile)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RiskProfile  $riskProfile
     * @return \Illuminate\Http\Response
     */
    public function destroy(RiskProfile $riskProfile)
    {
        //
    }
}
