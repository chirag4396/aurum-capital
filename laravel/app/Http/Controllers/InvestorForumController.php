<?php

namespace App\Http\Controllers;

use App\InvestorForum;
use Illuminate\Http\Request;
use Symfony\Component\VarDumper\Cloner\Data;
use Illuminate\Support\Facades\Auth;
use Yajra\Datatables\Datatables;
use App\Http\Traits\GetData;
use Yajra\DataTables\Services\DataTable;

class InvestorForumController extends Controller
{
    use GetData;

    protected $res = ['msg' => 'error'];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function allForums() {
        $ifs = InvestorForum::orderBy('if_approve', 'desc')->get();

        return Datatables::of($ifs)->editColumn('if_question', function ($if){
            return str_limit($if->if_question, 100);
        })->addColumn('name', function ($if){
            return $if->user->name;
        })->editColumn('if_created_at', function ($if){
            return $if->if_created_at->format('jS M, Y - h:i A');
        })->addColumn('action', function ($if){
            return '<button class = "btn btn-sm btn-success" onclick="viewDetail(\''.$if->if_id.'\', \''.route('admin.all-answers').'\');">Answers ('.$if->answers()->count().')</button>'
                .'<button class = "btn btn-sm btn-success" onclick="viewDetail(\''.$if->if_id.'\', \''.route('admin.answer-form').'\');">Give Answer</button>'
                .'<form method="post" action="'.route('admin.investor-forum.update', ['id' => $if->if_id]).'">'
                    .'<input type = "hidden" value = "PATCH" name = "_method">'
                    .'<input type = "hidden" value = "'.( $if->if_approve ? 0 : 1 ).'" name = "approve">'
                    .csrf_field()
                    .'<button class = "btn btn-sm btn-'.( $if->if_approve ? 'danger' : 'success' ).'">'.( $if->if_approve ? 'Deny' : 'Approve' ).'</button>'
                .'</form>';
        })->make(true);
    }

    public function index()
    {
        return view('admin.view_investor_forum');
    }

    public function answerForm(Request $r){
        return view('admin.forms.answer_form')->with(['qus' => InvestorForum::find($r->id)]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r) {
        $nif = $this->changeKeys('if_', $r->all());
        if(isset($r->anonymous)){
            $nif['if_anonymous'] = 1;
        }
        $nif['if_user'] = Auth::id();

        InvestorForum::create($nif);
        $this->res['msg'] = 'success';
        return $this->res;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\InvestorForum  $investorForum
     * @return \Illuminate\Http\Response
     */
    public function show(InvestorForum $investorForum)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\InvestorForum  $investorForum
     * @return \Illuminate\Http\Response
     */
    public function edit(InvestorForum $investorForum)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\InvestorForum  $investorForum
     * @return \Illuminate\Http\Response
     */
    public function update(Request $r, InvestorForum $investorForum)
    {
        $nif = $this->changeKeys('if_', $r->all());
        $investorForum->update($nif);
        if(isset($r->approve)){
            return redirect()->back();
        }

        return $this->res;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\InvestorForum  $investorForum
     * @return \Illuminate\Http\Response
     */
    public function destroy(InvestorForum $investorForum)
    {
        //
    }
}
