<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use App\Http\Traits\GetData;
use App\User;
use App\Kyc;
use App\SubscriptionLog;
use \Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use App\Subscription;
use Yajra\Datatables\Datatables;
use App\Events\EmailEvent;
use DB;
use setasign\Fpdi;
use App\Pdf\PDF;
use Intervention\Image\ImageManager;
// use Intervention\Image;

class UserController extends Controller
{
    use GetData;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */    
    public function downloadKyc(){

    // $files = Kyc::where('kyc_status', 0)->get()->pluck('kyc_image')->toArray();
        $files = Kyc::get()->pluck('kyc_image')->toArray();

    // $rmfiles = Kyc::select(DB::raw("REPLACE(kyc_image,'img/pan-cards/', '') as kyc_image"))->where('kyc_status', 1)->get()->pluck('kyc_image')->toArray();

        \Zipper::make('aurum-new-kyc-documents.zip')->add($files)->close();

        return response()->download('aurum-new-kyc-documents.zip');
    }    

    public function allUser(Request $r){        
        $users = User::where([
            'soft_delete' => 0,
            'type' => 102,
            'paid' => $r->type
        ])->orderBy('created_at', 'desc');

        return Datatables::of($users)->editColumn('created_at', function ($user) {
            return $user->created_at->format('jS M, Y - h:i A');
        })->addColumn('color', function ($user) {
            $newKyc = $user->kycDetail()->orderBy('kyc_created_at', 'desc')->first();
            if($newKyc) {
                if ($newKyc->kyc_status) {
                    return 'tr-green';
                }else {
                    return 'tr-yellow';
                }
            }else{
                return 'tr-red';                
            }
        })->addColumn('action', function ($user) {
            if($user->paid){
                $first = '<button class="btn btn-danger btn-sm" onclick="viewDetail('.$user->id.');"><i class="fa fa-times"></i> Cancel</button>';
            }else{
                if($user->lastSubscriptionLog()->count()) {
                    $first = '<button class="btn btn-warning btn-sm" onclick="viewDetail('.$user->id.');"><i class="fa fa-play"></i> Continue</button>';
                }else{
                    $first = '<button class="btn btn-success btn-sm" onclick="viewDetail('.$user->id.');"><i class="fa fa-check"></i> Approve</button>';                
                }
            }
            return '<a class="btn btn-success btn-sm" href = "'.route('admin.risk-profiles',['id' => $user->id]).'"><i class="fa fa-eye"></i> Risk Profiles ('.$user->riskProfile()->count().')</a>| <a class="btn btn-success btn-sm" href = "'.route('admin.kycs',['id' => $user->id]).'"><i class="fa fa-eye"></i> Kycs ('.$user->kycDetail()->count().')</a>| '.$first;
        })
        ->make(true);        
    }

    public function index() {
        // return view('admin.view_users')->with(['users' => $this->allUser()]);
    }

    public function getClients($type, $tname) {
        return view('admin.view_users')->with(['type' => $type, 'tname' => $tname]);
    }

    public function paymentForm(Request $r){
        return view('admin.forms.payment_form')->with(['user' => User::find($r->id)]);
    }

    public function create(){

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $r, $id)
    {
        $user = User::find($id);
        $emailSet['to'] = ['email' => $user->email,'name' => $user->name];
        $emailSet['blade'] = 'normal';

        if(isset($r->block)){
            $user->update(['block' => $r->block]);
            if($r->block){                
                $emailSet['data'] = [
                    'note' => $r->note,
                    'msg' => __('email.block')
                ];
                $emailSet['subject'] = __('email.block_subject');
            }else{
                $emailSet['data'] = [
                    'note' => $r->note,
                    'msg' => __('email.unblock')
                ];
                $emailSet['subject'] = __('email.unblock_subject');
            }
            event(new EmailEvent($emailSet));
            return redirect()->back();
        }
        if(isset($r->paid)){
            if(isset($r->subscription)) {
                $sub = Subscription::find($r->subscription);

                if($sub->sub_cancel){
                    $emailSet['data'] = [
                        'note' => $r->note,
                        'msg' => __('email.resume_subscription', ['amount' => number_format($sub->sub_amount)])
                    ];
                    $emailSet['subject'] = __('email.resume_subject');

                }else{
                    $emailSet['data'] = [
                        'note' => $r->note,
                        'msg' => __('email.cancel_subscription')
                    ];                    
                    $emailSet['subject'] = __('email.cancel_subscription_subject');                    
                }                
                $sub->update(['sub_cancel' => !$sub->sub_cancel]);                    
            }else{
                $sub = Subscription::create([
                    'sub_user' => $id,
                    'sub_amount' => $r->amount, 
                    'sub_note' => $r->note, 
                    'sub_updated_by' => Auth::id(),
                    'sub_year' => $r->year,
                    'sub_end_data' => Carbon::now()->addYear($r->year)
                ]);
                
                $emailSet['data'] = [
                    'note' => $r->note,
                    'msg' => __('email.confirmation_success', ['amount' => number_format($sub->sub_amount)]), 
                    'route' => route('kyc-upload',['id' => $id])
                ];
                
                $emailSet['subject'] = __('email.confirmation_subject');
                
                
            }
            
            SubscriptionLog::create([
                'sl_user' => $user->id,
                'sl_added_by' => Auth::id(),
                'sl_status' => $r->paid,
                'sl_sub' => $sub->sub_id 
            ]);        

            event(new EmailEvent($emailSet));
            
            $user->update(['paid' => $r->paid]);
            
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){        
    }    

    public function checkIpDetail()
    {
        return $this->ipDetails($this->checkIp());
    }

    // public function checkStock() {
    //     return $this->stock();
    // }
}
