<?php

namespace App\Http\Controllers;

use App\RiskOption;
use Illuminate\Http\Request;

class RiskOptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RiskOption  $riskOption
     * @return \Illuminate\Http\Response
     */
    public function show(RiskOption $riskOption)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RiskOption  $riskOption
     * @return \Illuminate\Http\Response
     */
    public function edit(RiskOption $riskOption)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RiskOption  $riskOption
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RiskOption $riskOption)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RiskOption  $riskOption
     * @return \Illuminate\Http\Response
     */
    public function destroy(RiskOption $riskOption)
    {
        //
    }
}
