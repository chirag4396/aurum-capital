<?php

namespace App\Http\Controllers;

use App\InvestorForum;
use App\Stock;
use App\CurrentRecommendation;
use App\Faq;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Socialite;
use App\Events\LogOutEvent;
use App\Events\ScrapStockData;
use App\Events\DeleteReport;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        return view('users.index');
    }
    public function about(){
        return view('users.about');
    }
    public function blogs(){
        if (!Auth::guest()) {

            if(Auth::user()->paid){
                return view('users.blogs');
            }
        }
        return view('users.services');
    }
    public function contact(){
        return view('users.contact');
    }
    public function currentRecommendation(){
        $stocks = Stock::whereIn('st_id', CurrentRecommendation::get()->pluck(['cr_stock'])->toArray())->get();
        return view('users.current_recommendation')->with(['stocks' => $stocks]);
    }
    public function stockReports($id, $name){
        return view('users.stock_reports')->with(['stock' => Stock::find($id)]);
    }    
    public function faqs(){
        return view('users.faqs')->with(['faqs' => Faq::where(['faq_soft_delete' => 0])->get()]);
    }
    public function investorForum(){
        return view('users.investor_forum')->with(['investor_forums' => InvestorForum::where(['if_approve' => 1])->orderBy('if_created_at', 'desc')->paginate(5)]);
    }    
    public function pastPerformances(){
        return view('users.past_performances');
    }       
    public function services(){
        return view('users.services');
    }
    public function testimonials(){
        return view('users.testimonials');
    }
    public function fb(){
        return view('users.test');
    }
    public function terms(){
        return view('users.terms');
    }
    public function privacy(){
        return view('users.privacy');
    }
    /**
    * Redirect the user to the GitHub authentication page.
    *
    * @return \Illuminate\Http\Response
    */
    public function redirectToProvider()
    {
        return Socialite::driver('facebook')->redirect();
    }

    /**
    * Obtain the user information from GitHub.
    *
    * @return \Illuminate\Http\Response
    */
    public function handleProviderCallback()
    {
        $user = Socialite::driver('facebook')->user();
        return $user;
    }

    public function browser(){
        return view('users.browser');
    }    

    public function checkScrap() {
        return event(new ScrapStockData());
    }
    
    public function checkLogout() {
        return event(new LogOutEvent());
    }

    public function checkPdfEvent() {
        return event(new DeleteReport());
    }
}
