<?php

namespace App\Http\Controllers;

use App\Faq;
use Illuminate\Http\Request;
use App\Http\Traits\GetData;
use Illuminate\Database\QueryException;

class FaqController extends Controller
{
    use GetData;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.view_faqs')->with(['faqs' => Faq::where(['faq_soft_delete' => 0])->get()]);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.create_faq');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {
        try {
            $faq = $this->changeKeys('faq_',$r->all());

            $this->res['msg'] = Faq::create($faq) ? 'success' : 'error';

        } catch (QueryException $e) {
            $this->res['msg'] = 'error';
            $this->res['error'] = $e->getMessage();
            // return $e;        
        }
        return $this->res;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Faq  $faq
     * @return \Illuminate\Http\Response
     */
    public function show(Faq $faq)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Faq  $faq
     * @return \Illuminate\Http\Response
     */
    public function edit(Faq $faq)
    {
        return view('admin.edit_faq')->with(['faq' => $faq]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Faq  $faq
     * @return \Illuminate\Http\Response
     */
    public function update(Request $r, Faq $faq)
    {
        try {
            $f = $this->changeKeys('faq_',$r->all());
            $faq->update($f);
            $this->res['msg'] = 'successU';

        } catch (QueryException $e) {
            $this->res['msg'] = 'error';
            // return $e;        
        }
        return $this->res;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Faq  $faq
     * @return \Illuminate\Http\Response
     */
    public function destroy(Faq $faq)
    {
        $faq->update(['faq_soft_delete' => 1]);
        return redirect()->back();
    }
}
