<?php

namespace App\Http\Controllers;

use App\Kyc;
use App\User;
use App\RejectReason;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\QueryException;
use App\Http\Traits\GetData;
use App\Events\EmailEvent;

class KycController extends Controller
{
    use GetData;

    protected $path = 'img/pan-cards/';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function allKyc($id = null, $type = null){
        $kycs = Kyc::where(function ($kycs) use($id, $type) {            
            if(!is_null($id)) {
                $kycs = $kycs->where('kyc_user', $id);
            }
            if(!is_null($type)) {
                if($type == 'new'){
                    $kycs = $kycs->where(['kyc_status' => 0, 'kyc_reject' => 0]);
                }
            }
        })->orderBy('kyc_created_at', 'desc')->get();
        
        return $kycs;
    }

    public function allKycs($id = null) {
        $kycs = $this->allKyc($id);        
        if($kycs->count()) {
            return view('admin.view_kycs')->with(['kycs' => $kycs, 'user' => User::find($id)]);
        }
        return redirect()->back();
    }

    public function allKycsNew(){        
        $kycs = $this->allKyc(null, 'new');
        if($kycs->count()) {
            return view('admin.view_kycs')->with(['kycs' => $kycs]);
        }
        return redirect()->back();
    }

    public function index(){
        return view('admin.view_kycs')->with(['kycs' => $this->allKyc()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.create_kyc');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r) {        
        $user = Auth::user();
        try{

            $emailSet['to'] = ['email' => $user->email,'name' => $user->name];
            $emailSet['blade'] = 'normal';
            
            $kyc = $this->changeKeys('kyc_',$r->all());
            
            if ($r->hasFile('image')) {
                if(substr($r->file('image')->getMimeType(), 0, 5) == 'image') {
                    $image_title = substr($user->email, 0, strpos($user->email, "@")).'-'.$kyc['kyc_number'].'-'.$user->mobile;
                    list($regular) = $this->uploadFiles($r, $image_title, 'image', [$this->path]);
                    $kyc['kyc_image'] = $regular;
                }else{
                    $this->res['msg'] = 'image';
                    return $this->res;
                }
            }
            $kyc['kyc_user'] = $user->id;
            $kyc['kyc_number'] = strtoupper($kyc['kyc_number']);

            $user->update(['kyc' => 1]);
            
            $this->res['msg'] = Kyc::create($kyc) ? 'success' : 'error';
            $this->res['location'] = route('risk-upload', ['id' => $user->id]);
            
            $emailSet['data'] = [
                'msg' => __('email.kyc_recieved')
            ];
            
            $emailSet['subject'] = __('email.kyc_recieved_subject');

        } catch (QueryException $e) {
            $user->update(['kyc' => 0]);
            return $this->exception($e);
        }
        
        event(new EmailEvent($emailSet));
        
        return $this->res;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Kyc  $kyc
     * @return \Illuminate\Http\Response
     */
    public function show(Kyc $kyc) {
        return view('admin.forms.view_kyc')->with(['kyc' => $kyc]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Kyc  $kyc
     * @return \Illuminate\Http\Response
     */
    
    public function upload($id){
        $user = User::find($id);
        if(Auth::id() == $id) {
            if(Auth::user()->type == 102){                
                if(!Auth::user()->paid) {
                    return redirect()->to('services');
                }
                return view('users.kyc');
            }
            return redirect()->to('/')->with(['page_error' => 'You are not allowed to upload kyc!!']);
        }
        return redirect()->to('/')->with(['page_error' => 'Please login with '.$user->name.'!!']);

    }

    public function edit(Kyc $kyc)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Kyc  $kyc
     * @return \Illuminate\Http\Response
     */
    public function update(Request $r, Kyc $kyc) {
        // return $r->all();
        if(isset($r->status)) {
            $user = User::find($kyc->kyc_user);
            
            $emailSet['to'] = ['email' => $user->email,'name' => $user->name];
            $emailSet['blade'] = 'normal';

            
            if(isset($r->approve)){
                // return 'app';
                $emailSet['data'] = [
                    'msg' => __('email.kyc_approve'),
                    'route' => route('risk-upload', $user->risk ? ['id' => $user->id,'edit' => 'edit'] : ['id' => $user->id]) 
                ];

                $emailSet['subject'] = __('email.kyc_approve_subject');

                $kyc->update(['kyc_status' => 1, 'kyc_reject' => 0]);
                $user->update(['kyc' => $r->status]);
            }
            if(isset($r->reject)){                
                $reasons = $r->reason;
                if(!is_null($r->new_reason)){
                    $rr = RejectReason::create([
                        'rr_title' => $r->new_reason
                    ]);

                    $reasons[] = $rr->rr_id;
                }                
                $note = implode(' and ', RejectReason::whereIn('rr_id', $reasons)->get()->pluck('rr_title')->toArray());
                $emailSet['data'] = [
                    'msg' => __('email.kyc_reject'),
                    'note' => $note,
                    'route' => route('kyc-upload', ['id' => $user->id])
                ];

                $emailSet['subject'] = __('email.kyc_reject_subject');

                $kyc->update(['kyc_status' => 0, 'kyc_reject' => 1, 'kyc_reasons' => implode(',', $reasons)]);
                $user->update(['kyc' => 0]);
            }
            
            
            
            event(new EmailEvent($emailSet));

            return redirect()->back()->with(['kycs' => $this->allKyc()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Kyc  $kyc
     * @return \Illuminate\Http\Response
     */
    public function destroy(Kyc $kyc)
    {
        //
    }
}
