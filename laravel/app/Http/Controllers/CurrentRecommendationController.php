<?php

namespace App\Http\Controllers;

use App\CurrentRecommendation;
use Illuminate\Http\Request;
use App\Http\Traits\GetData;

class CurrentRecommendationController extends Controller
{
    use GetData;

    protected $res = ['msg' => 'error'];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('admin.create_current_recommendation');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r) {
        
        $crD = $this->changeKeys('cr_' , $r->all());
        
        $crD['cr_range'] = isset($r->ranges) ? (count(array_filter($r->ranges)) != 0 ? implode(',', $r->ranges): '') : '';
        $crD['cr_sell_price'] = isset($r->sell_prices) ? (count(array_filter($r->sell_prices)) != 0 ? implode(',', $r->sell_prices): '') : '';
        
        $cr = CurrentRecommendation::create($crD);
        if ($cr) {
            $this->res['msg'] = 'success';
        }
        return $this->res;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CurrentRecommendation  $currentRecommendation
     * @return \Illuminate\Http\Response
     */
    public function show(CurrentRecommendation $currentRecommendation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CurrentRecommendation  $currentRecommendation
     * @return \Illuminate\Http\Response
     */
    public function edit(CurrentRecommendation $currentRecommendation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CurrentRecommendation  $currentRecommendation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CurrentRecommendation $currentRecommendation)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CurrentRecommendation  $currentRecommendation
     * @return \Illuminate\Http\Response
     */
    public function destroy(CurrentRecommendation $currentRecommendation)
    {
        //
    }
}
