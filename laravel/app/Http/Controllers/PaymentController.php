<?php

namespace App\Http\Controllers;

use App\Payment;
use Illuminate\Http\Request;
use App\Http\Traits\GetData;
use App\Classes\EaseBuzz;
use App\OrderDetail;
use App\SubscriptionPlan;
use App\SubscriptionLog;
use App\Subscription;
use Carbon\Carbon;
use App\Events\EmailEvent;
use Illuminate\Support\Facades\Auth;

class PaymentController extends Controller
{
    use GetData;    

//    protected $MERCHANT_KEY="PKNQPUCXCY";
//    protected $SALT='LJ81OQ0NP5';
//    protected $ENV='test';

    protected $MERCHANT_KEY="NGIYDMVM1L";
    protected $SALT='Z6A4EX3WAZ';
    protected $ENV='prod';


    public function getPay(Request $r){        

        if (Auth::guest()) {
            return redirect()->back()->with(['page_error' => 'You need to first log-in to pay online for our services.']);
        }
        // Check for existing Subscription

        $latestSubscription = Auth::user()->latestSubscription()->first();

        if($latestSubscription){

            $end_date = Carbon::parse($latestSubscription->sub_end_data);
            $now = Carbon::now();

            $diff = $now->diffInDays($end_date)+1;
            if($diff > 0){
                return redirect()->back()->with(['page_error' => 'Your current subscription plan has '.$diff.' days left before renewal.']);
            }
        }

        $re = $r->all();
        $posted = array();

        if(!empty($re)) {
            foreach($re as $key => $value) {
                $posted[$key] = htmlentities($value, ENT_QUOTES);
                $posted[$key] = trim($value);
            }
        }
        $formError = 0;
        if(sizeof($posted) > 0) {            
            $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
            if(
                empty($posted['amount'])
                || empty($posted['firstname'])
                || empty($posted['email'])
                || empty($posted['phone'])
                || empty($posted['productinfo'])
                || empty($posted['surl'])
                || empty($posted['furl'])
            ) {
                $formError = 1;
            }

            $this->addOrderDetail($r);
            $a = new EaseBuzz();
            return $a->easepay_page(array('key' => $this->MERCHANT_KEY,
                'txnid' => $txnid,
                'amount' => $posted['amount'],
                'firstname' => $posted['firstname'],
                'email' => $posted['email'],
                'phone' => $posted['phone'],
                'udf1' => $posted['udf1'],
                'udf2' => $posted['udf2'],
                'udf3' => $posted['udf3'],
                'udf4' => $posted['udf4'],
                'udf5' => $posted['udf5'],
                'productinfo' => $posted['productinfo'],
                'surl' => $posted['surl'],
                'furl' => $posted['furl']), $this->SALT, $this->ENV);
        }

        return view('users.payment')->with([
            'posted' => $posted,      
            'formError' => $formError,
            'plans' => SubscriptionPlan::get()
        ]);
    }

    public function addOrderDetail($r){
        $plan = explode('-', $r->plan)[0];

        $orderDetail = OrderDetail::create([
            'order_d_id' => 'AC' . rand(99999, 4).(OrderDetail::max('order_id')+1),        
            'order_total' => $r->amount,
            'order_name' => $r->firstname,
            'order_plan' => $plan,
            'order_email' => $r->email,
            'order_mobile' => $r->phone,    
        ]);

        $r->session()->put('order_id', $orderDetail->order_id);
        $r->session()->put('plan', $plan);
    }
/**
* Display a listing of the resource.
*
* @return \Illuminate\Http\Response
*/
public function index()
{
    if (Auth::guest()) {
        return redirect()->route('payment.create');            
    }
    return view('admin.view_payments')->with(['payments' => Payment::get()]);
}

/**
* Show the form for creating a new resource.
*
* @return \Illuminate\Http\Response
*/
public function create(Request $r)
{
    list($posted, $this->txnid) = $this->listd($r);

    return view('users.payment')->with(['hash' => $this->hash, 'formError' => $this->formError, 'action' => url('placeorder'), 'MERCHANT_KEY' => $this->MERCHANT_KEY, 'txnid' => $this->txnid, 'data' => $r->all()]);
}

/**
* Store a newly created resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @return \Illuminate\Http\Response
*/
public function store(Request $request)
{
//
}

/**
* Display the specified resource.
*
* @param  \App\Models\Payment  $payment
* @return \Illuminate\Http\Response
*/
public function show(Payment $payment)
{
//
}

/**
* Show the form for editing the specified resource.
*
* @param  \App\Models\Payment  $payment
* @return \Illuminate\Http\Response
*/
public function edit(Payment $payment)
{
//
}

/**
* Update the specified resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @param  \App\Models\Payment  $payment
* @return \Illuminate\Http\Response
*/
public function update(Request $request, Payment $payment)
{
//
}

/**
* Remove the specified resource from storage.
*
* @param  \App\Models\Payment  $payment
* @return \Illuminate\Http\Response
*/
public function destroy(Payment $payment)
{
//
}
public function orderStatus(Request $r){
// $orderId = $r->session()->get('order_id');
    $status = false;
    $orderDetail = OrderDetail::find($r->session()->get('order_id'));
    $ph = Payment::create([
        'ph_order_id' => $orderDetail->order_id,
        'ph_email' => is_null($r->email) ? $orderDetail->order_email : $r->email,
        'ph_phone' => is_null($r->phone) ? $orderDetail->order_mobile : $r->phone,
        'ph_status' => $r->status,
        'ph_txn_id' => $r->txnid,
        'ph_plan' => $r->session()->get('plan'),
        'ph_amount' => $r->amount,
        'ph_fullname' => $r->firstname,
        'ph_bank_ref_num' => $r->bank_ref_num,
        'ph_bankcode' => $r->bankcode,
        'ph_unmapped' => $r->unmappedstatus,
        'ph_added_on' => $r->addedon
    ]);
    $ease = new EaseBuzz();
    $chk = $ease->response($r->all(), $this->SALT);
    if ($r->status == 'success') {
        $msg = 'We recieved Rs. '.number_format($ph->ph_amount,2).' on behalf of Service Plan of '.$orderDetail->plan->sp_title;
        
        $this->newSubscription([
            'id' => Auth::id(),
            'amount' => $ph->ph_amount,
            'year' => explode(' ', $orderDetail->plan->sp_title)[0],
        ]);
//        $this->sendSMS($ph->ph_phone, $msg);

        $status = true;

    }
    $r->session()->forget('order_id');
    $r->session()->forget('plan');
    return redirect()->route('previous', ['id' => $orderDetail->order_id, 'status' => $status]);

}  

public function newSubscription($r) {
    $user = Auth::user();
    $emailSet['to'] = ['email' => $user->email,'name' => $user->name];
    $emailSet['blade'] = 'normal';

    $emailSet2['to'] = ['email' => config('app.support_email'), 'name' => config('app.name')];
    $emailSet2['blade'] = 'normal';

    $sub = Subscription::create([
        'sub_user' => $user->id,
        'sub_amount' => $r['amount'],         
        'sub_updated_by' => $user->id,
        'sub_year' => $r['year'],
        'sub_end_data' => Carbon::now()->addYear($r['year'])
    ]);

    $emailSet['data'] = [
        'msg' => __('email.confirmation_success', ['amount' => number_format($sub->sub_amount)]), 
        'route' => route('kyc-upload',['id' => $user->id])
    ];

    $emailSet['subject'] = __('email.confirmation_subject');

    $emailSet2['data'] = [
        'msg' => __('email.new_subscription_admin', [
            'amount' => number_format($sub->sub_amount),
            'user' => $user->name,
            'mobile' => $user->mobile,
            'email' => $user->email,
        ])
    ];

    $emailSet2['subject'] = __('email.new_subscription_admin_subject');
    
    SubscriptionLog::create([
        'sl_user' => $user->id,
        'sl_added_by' => $user->id,
        'sl_status' => 1,
        'sl_sub' => $sub->sub_id 
    ]);        

    event(new EmailEvent($emailSet));
    event(new EmailEvent($emailSet2));
    
    Auth::user()->update(['paid' => 1]);


}

public function listd(Request $r)
{

    $posted = array();
    if (!empty($r)) {
        foreach ($r->all() as $key => $value) {
            $posted[$key] = $value;

        }
    }

    if (empty($posted['txnid'])) {
        $this->txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
    } else {
        $this->txnid = $posted['txnid'];
    }

    return [$posted, $this->txnid];
}

public function previous($id, $status = false, Request $r){        
    $order = OrderDetail::find($id);
    return view('users.payment_status')->with(['order' => $order, 'status' => $status]);
}
}
