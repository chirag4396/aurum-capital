<?php

namespace App\Http\Controllers;

use App\ForumAnswer;
use App\InvestorForum;
use Illuminate\Http\Request;
use App\Http\Traits\GetData;

class ForumAnswerController extends Controller
{
    use GetData;

    protected $res = ['msg' => 'error'];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function allAnswers(Request $r){
        return view('admin.forms.all_answers')->with(['if' => InvestorForum::find($r->id)]);
    }
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {
        $fas = $this->changeKeys('fa_', $r->all());
        if(isset($r->admin)) {
            $fas['fa_approve'] = 1;
        }

        $fa = ForumAnswer::create($fas);

        if($fa){
            $this->res['msg'] = 'success';
        }

        return $this->res;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ForumAnswer  $forumAnswer
     * @return \Illuminate\Http\Response
     */
    public function show(ForumAnswer $forumAnswer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ForumAnswer  $forumAnswer
     * @return \Illuminate\Http\Response
     */
    public function edit(ForumAnswer $forumAnswer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ForumAnswer  $forumAnswer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $r, ForumAnswer $forumAnswer) {
        $nfa = $this->changeKeys('fa_', $r->all());
        $forumAnswer->update($nfa);

//        if(isset($r->approve)){
        return redirect()->back();
//        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ForumAnswer  $forumAnswer
     * @return \Illuminate\Http\Response
     */
    public function destroy(ForumAnswer $forumAnswer)
    {
        //
    }
}
