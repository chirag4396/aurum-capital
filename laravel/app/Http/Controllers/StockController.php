<?php

namespace App\Http\Controllers;

use App\Stock;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Traits\GetData;
use App\Http\Traits\Scrap;
use Yajra\Datatables\Datatables;

class StockController extends Controller
{
    use GetData, Scrap;

    protected $res = ['msg' => 'error'];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
//        return Stock::get();
        return view('admin.view_stocks');
    }

    public function allStocks(Request $r){        
        $stocks = Stock::get();

        return Datatables::of($stocks)->addColumn('symbol', function ($stock) {
            return empty($stock->st_symbol) ? '-' : $stock->st_symbol;
        })->addColumn('title', function ($stock) {
            return empty($stock->st_title) ? '-' : $stock->st_title;
        })->addColumn('type', function ($stock) {
            return $stock->type->stt_title;
        })->addColumn('action', function ($stock) {
            return '<form method = "post" action = "'.route('admin.stock.update', ['id' => $stock->st_id]).'">'
            .csrf_field()
            .'<input type="hidden" name="_method" value = "PATCH">'
            .'<input type="hidden" name="active" value = "'.($stock->st_active ? 0 : 1).'">'
            .'<button class="btn btn-'.($stock->st_active ? 'danger' : 'success').' btn-sm">'.($stock->st_active ? 'Dis-Approve it' : 'Approve it').'</button>'
            .'<a href = "'.route('admin.stock.edit',['id' => $stock->st_id]).'" class="btn btn-success btn-sm">Edit</a>'
            .'</form>';
        })->make(true);        
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r) {

        $st = $this->changeKeys('st_' , $r->all());
        $stock = Stock::create($st);
        if ($stock) {
            $this->res = ['msg' => 'success', 'd' => $this->removePrefix($stock->toArray())];
        }

        return $this->res;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Stock  $stock
     * @return \Illuminate\Http\Response
     */
    public function show(Stock $stock)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Stock  $stock
     * @return \Illuminate\Http\Response
     */
    public function edit(Stock $stock)
    {
        return view('admin.edit_stock')->with(['stock' => $stock]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Stock  $stock
     * @return \Illuminate\Http\Response
     */
    public function update(Request $r, Stock $stock)
    {
        if (isset($r->active)) {
            $stock->update(['st_active' => $r->active]);
            return redirect()->back();
        }

        $st = $this->changeKeys('st_' , $r->all());
        $stock->update($st);

        $this->res['msg'] = 'successU';

        return $this->res;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Stock  $stock
     * @return \Illuminate\Http\Response
     */
    public function destroy(Stock $stock)
    {
        //
    }

    // public function getStock() {

    //     $now = Carbon::now();
    //     $bseDate = $now->format('dmy');
    //     $nseDate = strtoupper($now->format('dMY'));

    //     $bseZip = 'EQ'.$bseDate.'_CSV.ZIP';
    //     $bseCsv = 'EQ'.$bseDate.'.CSV';
    //     $nseZip = strtoupper($now->format('Y/M')).'/cm'.$nseDate.'bhav.csv.zip';
    //     $nseCsv = 'cm'.$nseDate.'bhav.csv';

    //     $this->downloadZip($bseZip, 'BSE');
    //     $this->importStockData($bseCsv);

    //     $this->downloadZip($nseZip, 'NSE');
    //     $this->importStockData($nseCsv);
    // }

    public function approveForm(Request $r) {
        return view('admin.forms.approve_form')->with(['stock' => Stock::find($r->id)]);
    }
}
