<?php 
namespace App\Http\Traits;
use App\CandidateDetail;
use App\Designation;
use App\User;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Intervention\Image\ImageManager;
use setasign\Fpdi;
use Fpdf;

trait GetData{

	protected $res = ['msg' => 'error'];	

	public function resizeImage($image, $quality = 75){
		$sourceImage = $image;
		$targetImage = $image;

		list($maxWidth, $maxHeight, $type, $attr) = getimagesize($image);
		
		if (!$image = @imagecreatefromjpeg($sourceImage)){
			return false;
		}

		list($origWidth, $origHeight) = getimagesize($sourceImage);

		if ($maxWidth == 0){
			$maxWidth  = $origWidth;
		}

		if ($maxHeight == 0){
			$maxHeight = $origHeight;
		}

		$widthRatio = $maxWidth / $origWidth;
		$heightRatio = $maxHeight / $origHeight;

		$ratio = min($widthRatio, $heightRatio);

		$dataewWidth  = (int)$origWidth  * $ratio;
		$dataewHeight = (int)$origHeight * $ratio;

		$dataewImage = imagecreatetruecolor($dataewWidth, $dataewHeight);
		imagecopyresampled($dataewImage, $image, 0, 0, 0, 0, $dataewWidth, $dataewHeight, $origWidth, $origHeight);
		imageinterlace($image, 1);
		imagejpeg($dataewImage, $targetImage, $quality);

		imagedestroy($image);
		imagedestroy($dataewImage);
	}

	public function changeKeys($rep, $arr = []){
		$dataew = [];
		foreach ($arr as $key => $value) {			
			$dataew[$rep.$key] = $value;
		}		
		return $dataew;
	}

	public function dateParser($date){
		return Carbon::parse($date)->format('Y-m-d h:i:s');
	}

	public function removePrefix($data, $prefix = null){
		foreach ($data as $k => $v) {
			if(is_array($v)){
				$data[$k] = $this->removePrefix($v);
			}
			if(is_null($prefix)){
				$e = explode('_', $k);
				if(count($e) > 1){
					unset($e[0]);				
					$d = implode('_', $e);
				}
			}else{
				$d = str_replace($prefix, '',$k);
			}
			$data[$d] = $v;
			unset($data[$k]);

		}
		return $data;        
	}		

	public function removeFile($path){
		if(File::exists($path)){
			unlink($path);
		}
	}

	public function uploadFiles($data, $title, $file, $paths = [], $remove = []){
		// return $paths;
		$postfix = ['big','thumbnail'];
		$da = [];

		$title = strtolower(str_replace(' ','-',trim($title))).'-'.rand(9999,4);
		$exe = $data->file($file)->getClientOriginalExtension();
		if(class_exists('ImageManager')){
			$manager = new ImageManager();
			$photo = $data->file($file)->getRealPath();

			foreach ($paths as $key => $value) {
				if(count($remove) > 0){
					$this->removeFile($remove[$key]);
				}
				$filename = $title.'-'.$postfix[$key].'.'.$exe;
				$full = $paths[$key].$filename;            

				$manager->make($photo)->save($full);
			// ->resize($width, null, function ($constraint) {
			// 	$constraint->aspectRatio();
			// })

				$this->resizeImage($full);
				$da[] = $full;
			}
		}else{						    
			$photo = $data->file($file)->getRealPath();			
			
			foreach ($paths as $key => $value) {
				
				if(count($remove) > 0){
					$this->removeFile($remove[$key]);
				}

				$filename = $title.'-'.$postfix[$key].'.'.$exe;
				$full = $paths[$key].$filename;				
				$data->file($file)->move($paths[$key],$filename);
				// $this->resizeImage($full);
				$da[] = $full;
			}

		}
		return $da;
	}

	public function sendSMS($mobile,$msg){

		$url = 'http://trans.smsfresh.co/api/sendmsg.php';

		$fields = array(
			'user' => 'magarsham', 
			'pass' => 'festivito5555', 
			'sender' => 'FESTVT' ,
			'phone' => $mobile, 
			'text' => $msg, 
			'priority' => 'ndnd', 
			'stype' => 'normal'        
		);


		$ch = curl_init();


		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POST, count($fields));
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($fields));


		$res = curl_exec($ch);


		curl_setopt($ch, CURLOPT_TIMEOUT, 0);
		curl_close($ch);

		return $res;
	}

	public function sendEmail($email, $name, $data, $blade, $sub = 'Order confirmation'){		
		$mail = Mail::send('admin.emails.'.$blade, $data, function ($message) use ($email, $name, $sub) {
			$message->to($email, $name);
			$message->from(config('app.email'), config('app.name'));
			$message->sender(config('app.email'), config('app.name'));
			$message->subject(config('app.name').' | '.$sub);
			$message->priority(1);				   
		});

		return $mail ? true : false;
	}

	public function exception($e){		
		$this->res['error'] = $e->getMessage();
		return $this->res;
	}	

	public function checkIp(){
		$ipaddress = '';
		if (getenv('HTTP_CLIENT_IP'))
			$ipaddress = getenv('HTTP_CLIENT_IP');
		else if(getenv('HTTP_X_FORWARDED_FOR'))
			$ipaddress = getenv('HTTP_X_FORWARDED_FOR');
		else if(getenv('HTTP_X_FORWARDED'))
			$ipaddress = getenv('HTTP_X_FORWARDED');
		else if(getenv('HTTP_FORWARDED_FOR'))
			$ipaddress = getenv('HTTP_FORWARDED_FOR');
		else if(getenv('HTTP_FORWARDED'))
			$ipaddress = getenv('HTTP_FORWARDED');
		else if(getenv('REMOTE_ADDR'))
			$ipaddress = getenv('REMOTE_ADDR');
		else
			$ipaddress = 'UNKNOWN';

		return $ipaddress;
	}
	public function context() {
		return $context = stream_context_create(
			array(
				"http" => array(
					"header" => "User-Agent: ".$_SERVER['HTTP_USER_AGENT']
				)
			)
		);
	}
	public function ipDetails($ip) {		
		
		$url = file_get_contents("https://whatismyipaddress.com/ip/$ip", false, $this->context());

		preg_match_all('/<th>(.*?)<\/th><td>(.*?)<\/td>/s', $url, $output, PREG_SET_ORDER);

		return [
			'ip' => $output[0][2],
			'isp' => $output[4][2],
			'city' => $output[13][2],
			'state' => $output[12][2],
			'zip_code' => $output[16][2],
			'country' => trim(explode('<', $output[11][2])[0]),
			'continent' => $output[10][2]
		];
	}	
	
}