<?php
namespace App\Http\Traits;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Excel;
use App\Stock;
use App\ClosingPrice;
use Carbon\Carbon;

trait Scrap {
	
	protected $path = 'stocks/';

	protected $stockType;

	protected $stockId;

	public function bseData() {
        $now = Carbon::now();
        $bseDate = $now->format('dmy');

        $bseZip = 'EQ'.$bseDate.'_CSV.ZIP';
        $bseCsv = 'EQ'.$bseDate.'.CSV';

        $this->downloadZip($bseZip, 'BSE');
        $this->importStockData($bseCsv);
    }

    public function nseData() {
        $now = Carbon::now();
        $nseDate = strtoupper($now->format('dMY'));

        $nseZip = strtoupper($now->format('Y/M')).'/cm'.$nseDate.'bhav.csv.zip';
        $nseCsv = 'cm'.$nseDate.'bhav.csv';

        $this->downloadZip($nseZip, 'NSE');
        $this->importStockData($nseCsv);
    }

	public function getNSE($url) {

		$guzzle = new Client();

		$response = $guzzle->get($url);

		$destination = public_path('../../public_html/'.$this->path."NSE.zip");

		$file = fopen($destination, "w+");

		fputs($file, $response->getBody());

		fclose($file);
	}

	public function getBSE($url) {
		file_put_contents(public_path('../../public_html/'.$this->path."BSE.zip"), fopen($url, 'r'), false);
	}

	public function downloadZip($file, $stock) {

		$urls = [
			"BSE" => "https://www.bseindia.com/download/BhavCopy/Equity/",
			"NSE" => "https://www.nseindia.com/content/historical/EQUITIES/"
		];
		
		$url = $urls[$stock].$file;

		$this->stockType = $stock;

		switch ($stock) {			
			case 'BSE':
			$this->getBSE($url);			
			break;		
			
			case 'NSE':
			$this->getNSE($url);
			break;
		}

		$this->extractZip("{$stock}.zip");
	}

	public function extractZip($zip) {

		\Zipper::make(public_path('../../public_html/'.$this->path.$zip))->extractTo(public_path('../../public_html/'.$this->path));

	}

	public function importStockData($file) {

		$path = public_path('../../public_html/'.$this->path.$file);

		$data = Excel::load($path, function($reader) {})->get();

		if(!empty($data) && $data->count()){

			foreach ($data as $key => $value) {
                if($value->series == 'EQ' || $this->stockType == 'BSE') {
                    $this->generateStockData($value);
                    $this->generateCosingPriceData($value);
                }
			}
		}
		exit;
	}

	public function generateStockData($value) {

		switch ($this->stockType) {
			case 'BSE':
			$this->createStock([
				'st_title' => $value->sc_name,
				'st_symbol' => $value->sc_code,
				'st_type' => 1
			]);
			break;
			
			case 'NSE':
                $this->createStock([
                    'st_symbol' => $value->symbol,
                    'st_type' => 2
                ]);
            break;
		}		
	}

	public function createStock($data) {

		$stock = Stock::where($data)->first();
		
		if (!$stock) {
            $stock = Stock::create($data);
        }

        $this->stockId = $stock->st_id;
	}

	public function generateCosingPriceData($value) {

		ClosingPrice::create([
			'cp_value' => $value->close,
			'cp_stock' => $this->stockId
		]);
		
	}
}