<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class PaidUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        // return Auth::guard($guard)->check();
        $pageTitle = ucwords(str_replace('-', ' ', $request->route()->getName()));

        if(!Auth::guest()){
            $user = Auth::user();
            if (!$user->paid) {
                return redirect('/')->with(['page_error' => 'You are not allowed to view '.$pageTitle.'.']);
            }else{
                $newKyc = $user->kycDetail()->orderBy('kyc_created_at', 'desc')->first();
                if ($newKyc) {                    
                    if (!$newKyc->kyc_status){
                        return redirect('/')->with(['page_error' => 'You are not allowed to view '.$pageTitle.'.']);
                    }
                }
                if(!$user->risk) {
                    return redirect('/')->with(['page_error' => 'You are not allowed to view '.$pageTitle.'.']);
                }
            }
        }else{
            return redirect('/')->with(['page_error' => 'Please login to view '.$pageTitle.'.']);
        }
        return $next($request);
    }
}
