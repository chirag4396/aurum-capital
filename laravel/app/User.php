<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'type', 'soft_delete', 'kyc', 'mobile', 'risk', 'paid', 'block'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    public function subscriptions() {
        return $this->hasMany(\App\Subscription::class, 'sub_user');
    }
    
    public function latestSubscription() {
        return $this->hasMany(\App\Subscription::class, 'sub_user')->orderBy('sub_created_at', 'desc');
    }

    public function lastSubscriptionLog() {
        return $this->hasMany(\App\SubscriptionLog::class, 'sl_user')->orderBy('sl_created_at', 'desc');
    }

    public function kycDetail(){
        return $this->hasMany(\App\Kyc::class, 'kyc_user');
    }

    public function riskProfile() {
        return $this->hasMany(\App\RiskProfile::class, 'rp_user');
    }
            
    public function recentRiskProfile() {
        return $this->hasOne(\App\RiskProfile::class, 'rp_user')->orderBy('rp_created_at', 'desc');
    }

    public function loginLogs() {
        return $this->hasMany(\App\LoginLog::class, 'll_user');
    }
}
