<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ForumAnswer extends Model
{
    public $primaryKey = 'fa_id';

    protected $fillable = [
        'fa_answer', 'fa_question', 'fa_user', 'fa_answer_on', 'fa_admin', 'fa_approve'
    ];

    CONST CREATED_AT = 'fa_created_at';

    CONST UPDATED_AT = null;

    public function question(){
        return $this->belongsTo(\App\ForumAnswer::class, 'fa_question');
    }

    public function replies(){
        return $this->hasMany(\App\ForumAnswer::class, 'fa_answer_on');
    }

    // public function replyAnswer(){
    //     return $this->belongsTo(\App\ForumAnswer::class, 'fa_answer_on');
    // }

    public function user(){
        return $this->belongsTo(\App\User::class, 'fa_user');
    }
}
