<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    protected $primaryKey = 'order_id';

    protected $fillable = ['order_d_id', 'order_plan', 'order_name', 'order_email', 'order_mobile', 'order_total'];

    CONST CREATED_AT = 'order_created_at';
    
    CONST UPDATED_AT = null;

    public function payment() {
    	return $this->hasOne(\App\Payment::class, 'ph_order_id');
    }

    public function plan() {
    	return $this->belongsTo(\App\SubscriptionPlan::class, 'order_plan');
    }
}
