<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CurrentRecommendation extends Model
{
    public $primaryKey = 'cr_id';

    protected $fillable = ['cr_stock', 'cr_to_do', 'cr_percent_allocation', 'cr_date', 'cr_range', 'cr_cmp', 'cr_sell_price', 'cr_dividend', 'cr_closing_price'];

    CONST CREATED_AT = 'cr_created_at';

    CONST UPDATED_AT = null;

    public function action() {
		return $this->belongsTo(\App\Action::class, 'cr_to_do');
	}
}
