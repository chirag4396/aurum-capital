<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Action extends Model
{
	protected $primaryKey = 'ac_id';

	protected $fillable = ['ac_title'];

	public $timestamps = false;

	public function recommendations() {
		return $this->hasMany(\App\CurrentRecommendation::class, 'cr_to_do');
	}
}
