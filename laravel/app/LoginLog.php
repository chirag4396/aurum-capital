<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoginLog extends Model
{
    public $primaryKey = 'll_id';
    
    protected $fillable = [
    	'll_user', 'll_ip', 'll_status', 'll_created_at'
    ];
    
    CONST CREATED_AT = 'll_created_at';

    CONST UPDATED_AT = null;

    public function user(){
    	return $this->belongsTo(\App\User::class, 'll_user');
    }
}
