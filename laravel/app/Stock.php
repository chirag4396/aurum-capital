<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Stock extends Model
{
	protected $primaryKey = 'st_id';

	protected $fillable = ['st_title', 'st_type', 'st_symbol', 'st_active'];

	CONST CREATED_AT = 'st_created_at';
	
	CONST UPDATED_AT = null;

	public function recommendations() {
		return $this->hasMany(\App\CurrentRecommendation::class, 'cr_stock');
	}

	public function latestRecommendation() {
		return $this->hasMany(\App\CurrentRecommendation::class, 'cr_stock')->orderBy('cr_created_at', 'desc')->first();
	}

	public function reports() {
		return $this->hasMany(\App\StockReport::class, 'sr_stock');
	}

	public function type() {
		return $this->belongsTO(\App\StockType::class, 'st_type');
	}
	
	public function latestClosingPrice() {
		return $this->hasMany(\App\ClosingPrice::class, 'cp_stock')->where(function($stocks){
			$date;
			$dt = Carbon::now();
			$dayOfWeek = $dt->dayOfWeek;
			switch ($dayOfWeek) {
				case 0:
				$date = $dt->subDays(2);       
				break;
				case 6:
				$date = $dt->subDays(1);
				break;
				default:
				$date = $dt->subDays(1);
				break;
			}
			return $stocks->whereRaw('date(cp_created_at) <="'.$date->format('Y-m-d').'"');
		});
	}	
}
