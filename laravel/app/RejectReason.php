<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RejectReason extends Model
{
    public $primaryKey = 'rr_id';

	protected $fillable = ['rr_title', 'rr_default'];

	public $timestamps = false;	
}
