<?php
namespace App\Pdf;

use App\Pdf\PDF_Rotate;

class PDF extends PDF_Rotate {
    public $_tplIdx;
    
    public $fullPathToFile;    

    // public function Header() {
    //     // error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
    // //Put the watermark
    //     $this->Image('http://chart.googleapis.com/chart?cht=p3&chd=t:60,40&chs=250x100&chl=Hello|World', 40, 100, 100, 0, 'PNG');
    //     $this->SetFont('arial', 'B', 50);        
    //     $this->SetTextColor(255, 192, 203);
    //     $this->RotatedText(20, 230, 'Raddyx Technologies Pvt. Ltd.', 45);

    //     if (is_null($this->_tplIdx)) {

    //     // THIS IS WHERE YOU GET THE NUMBER OF PAGES
    //         // $this->numPages = $this->setSourceFile($this->fullPathToFile);
    //         $this->_tplIdx = $this->importPage(1);
    //     }
    //     $this->useTemplate($this->_tplIdx, 0, 0, 200);


    // }

    public function RotatedText($x, $y, $txt, $angle) {
    //Text rotated around its origin
        $this->Rotate($angle, $x, $y);
        $this->Text($x, $y, $txt);
        $this->Rotate(0);
    }

}