<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        'App\Console\Commands\RemoveLoginLog',
        'App\Console\Commands\ScrapNseData',
        'App\Console\Commands\ScrapBseData',
        'App\Console\Commands\RemoveReports'
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('aurum:remove-login-logs')
        ->cron('* * * * *')
        ->sendOutputTo('cronlog.log');

        $schedule->command('aurum:scrap-bse-data')
        ->cron('0 23 * * *')
        ->sendOutputTo('cronlog.log');

        $schedule->command('aurum:scrap-nse-data')
            ->cron('0 23 * * *')
            ->sendOutputTo('cronlog.log');

        $schedule->command('aurum:remove-reports')
        ->cron('* * * * *')
        ->sendOutputTo('cronlog.log');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
