<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class SubscriptionEndNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'aurum:end-subscription';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check user subscription ending date and forward mail to them.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
    }
}
