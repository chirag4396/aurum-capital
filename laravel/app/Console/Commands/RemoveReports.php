<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Events\DeleteReport;
use App\Http\Traits\GetData;

class RemoveReports extends Command
{
    use GetData;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'aurum:remove-reports';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove Generated Reports after some time.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        event(new DeleteReport());

        $this->info('Reports deleted Successfully.');
    }
}
