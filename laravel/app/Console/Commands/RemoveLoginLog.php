<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Events\LogOutEvent;
use App\Events\EmailEvent;

class RemoveLoginLog extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'aurum:remove-login-logs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check user time out and than log them out.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
//        $emailSet['to'] = ['email' => 'chirag.sungare@gmail.com', 'name' => 'Chirag'];
//        $emailSet['data'] = [
//            'msg' => 'Remove logs Successful'
//        ];
//
//        $emailSet['subject'] = 'Testing Remove logs';
//        $emailSet['blade'] = 'normal';
        event(new LogOutEvent());
//        event(new EmailEvent($emailSet));
        $this->info("Remove Log Successful!");
    }
}
