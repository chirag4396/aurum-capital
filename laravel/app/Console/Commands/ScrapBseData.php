<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Events\ScrapBseStockData;

class ScrapBseData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'aurum:scrap-bse-data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Scrap BSE stock data from bhav copy.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        event(new ScrapBseStockData());
        $this->info("BSE data successful scrapped!");
    }
}
