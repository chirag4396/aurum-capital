<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Events\ScrapNseStockData;

class ScrapNseData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'aurum:scrap-nse-data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Scrap NSE stock data from bhav copy';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        event(new ScrapNseStockData());
        $this->info("NSE data successful scrapped!");
    }
}
