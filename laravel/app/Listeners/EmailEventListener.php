<?php

namespace App\Listeners;

use App\Events\EmailEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class EmailEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  EmailEvent  $event
     * @return void
     */
    public function handle(EmailEvent $event) {
        $para = $event->data;
        
        Mail::send('admin.emails.'.$para['blade'], $para['data'], function ($message) use ($para) {
            $message->to($para['to']['email'], $para['to']['name']);
            $message->from(config('app.email'), config('app.name'));
            $message->sender(config('app.email'), config('app.name'));
            $message->subject(config('app.name').' | '.$para['subject']);
            $message->priority(1);                 
        });

        // return $mail ? true : false;
    }
}
