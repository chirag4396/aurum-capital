<?php

namespace App\Listeners;

use App\Events\LogOutEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\LoginLog;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class LogOutEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  LogOutEvent  $event
     * @return void
     */
    public function handle(LogOutEvent $event)
    {

        $date = Carbon::now()->subMinutes(config('app.log_session'));
//        if(Auth::check()) {
//            Auth::user()->loginLogs()->where(['ll_status' => 1])->first()->update(['ll_created_at' => Carbon::now()]);
//        }else{
            LoginLog::where(['ll_status' => 1])->where('ll_created_at', '<=', $date)->update(['ll_status' => 0]);
//        }
    }
}
