<?php

namespace App\Listeners;

use App\Events\ScrapNseStockData;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Http\Traits\Scrap;

class ScrapNseStockDataListener
{
    use Scrap;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ScrapNseStockData  $event
     * @return void
     */
    public function handle(ScrapNseStockData $event)
    {
        $this->nseData();
    }
}
