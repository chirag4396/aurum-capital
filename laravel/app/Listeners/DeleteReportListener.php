<?php

namespace App\Listeners;

use App\Events\DeleteReport;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\GeneratedPdf;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use App\Http\Traits\GetData;

class DeleteReportListener
{
    use GetData;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  DeleteReport  $event
     * @return void
     */
    public function handle(DeleteReport $event)
    {
        $expire = Carbon::now()->subMinutes(config('app.log_session'));
        $pdfs = GeneratedPdf::where('gp_created_at', '<=', $expire)->get();

        foreach ($pdfs as $key => $value) {
            GeneratedPdf::find($value->gp_id)->update(['gp_delete' => 1]);
            $this->removeFile('../../'.$value->gp_path);
        }
        array_map('unlink', glob('pdfs/images/*'));
    }
}
