<?php

namespace App\Listeners;

use Illuminate\Auth\Events\Logout;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Auth;
use App\LoginLog;

class LogSuccessfulLogout
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Logout  $event
     * @return void
     */
    public function handle(Logout $event)
    {
        $pre = $this->findLoginLog(Auth::id());
        if($pre){
            $pre->update(['ll_status' => 0]);
        }
    }

    protected function findLoginLog($id) {
        return LoginLog::where(['ll_status' => 1, 'll_user' => $id])->first();
    }
}
