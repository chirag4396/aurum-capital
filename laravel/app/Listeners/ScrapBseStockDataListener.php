<?php

namespace App\Listeners;

use App\Events\ScrapBseStockData;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Http\Traits\Scrap;

class ScrapBseStockDataListener
{
    use Scrap;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ScrapBseStockData  $event
     * @return void
     */
    public function handle(ScrapBseStockData $event)
    {
        $this->bseData();
    }
}
