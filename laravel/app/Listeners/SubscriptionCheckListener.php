<?php

namespace App\Listeners;

use Illuminate\Auth\Events\SubscriptionCheck;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SubscriptionCheckListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SubscriptionCheck  $event
     * @return void
     */
    public function handle(SubscriptionCheck $event)
    {
        //
    }
}
