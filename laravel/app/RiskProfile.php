<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RiskProfile extends Model
{    
	public $primaryKey = 'rp_id';

	protected $fillable = ['rp_user', 'rp_score', 'rp_dob'];

	CONST CREATED_AT = 'rp_created_at';
	
	CONST UPDATED_AT = null;
	
	public function user() {
		return $this->belongsTo(\App\User::class, 'rp_user');
	}

	public function answers() {
		return $this->hasMany(\App\RiskProfileAnswer::class, 'rpa_rp');
	}
}
