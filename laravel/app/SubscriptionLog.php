<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubscriptionLog extends Model
{
	public $primaryKey = 'sl_id';

	protected $fillable = ['sl_added_by', 'sl_status', 'sl_sub', 'sl_user'];
	
	CONST CREATED_AT = 'sl_created_at';
	
	CONST UPDATED_AT = null;
    
    public function subscription(){
    	return $this->belongsTo(\App\Subscription::class, 'sl_sub');
    }
}
