<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StockReport extends Model
{
	protected $primaryKey = 'sr_id';

	protected $fillable = ['sr_title', 'sr_stock', 'sr_pdf_path', 'sr_soft_delete'];

	CONST CREATED_AT = 'sr_created_at';

	CONST UPDATED_AT = null;

	public function stock() {
		return $this->belongsTo(\App\Stock::class, 'sr_stock');
	}	
}
