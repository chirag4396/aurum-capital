<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kyc extends Model
{
	public $primaryKey = 'kyc_id';
	
	protected $fillable = [
		'kyc_number', 'kyc_image', 'kyc_user', 'kyc_status', 'kyc_reject', 'kyc_reasons'
	];
	
	CONST CREATED_AT = 'kyc_created_at';

	CONST UPDATED_AT = null;

	public function user(){
	    return $this->belongsTo(\App\User::class, 'kyc_user');
	}
}
