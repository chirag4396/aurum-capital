<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RiskOption extends Model
{
	public $primaryKey = 'ro_id';

	protected $fillable = ['ro_option', 'ro_points', 'ro_rq'];

	public $timestamps = false;

	public function question(){
		return $this->belongsTo(\App\RiskQuestion::class, 'ro_rq');
	}
}
