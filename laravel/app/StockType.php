<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StockType extends Model
{
	protected $primaryKey = 'stt_id';

	protected $fillable = ['stt_title', 'stt_link'];

	public $timestamps = false;
}
