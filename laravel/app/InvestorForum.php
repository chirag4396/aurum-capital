<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvestorForum extends Model
{
    public $primaryKey = 'if_id';

    protected $fillable = [
        'if_user', 'if_question', 'if_anonymous', 'if_approve'
    ];

    CONST CREATED_AT = 'if_created_at';

    CONST UPDATED_AT = null;

    public function answers(){
        return $this->hasMany(\App\ForumAnswer::class, 'fa_question');
    }

    public function user(){
        return $this->belongsTo(\App\User::class, 'if_user');
    }
}
