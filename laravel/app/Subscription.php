<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{	
	public $primaryKey = 'sub_id';

	protected $fillable = ['sub_user', 'sub_amount', 'sub_note', 'sub_updated_by', 'sub_cancel', 'sub_year', 'sub_end_data'];
	
	CONST CREATED_AT = 'sub_created_at';
	
	CONST UPDATED_AT = 'sub_updated_at';
}
