<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RiskProfileAnswer extends Model {

	public $primaryKey = 'rpa_id';

	protected $fillable = ['rpa_qus', 'rpa_ans', 'rpa_point', 'rpa_rp'];

	public $timestamps = false;

	public function profile() {
		return $this->belongsTo(\App\RiskProfile::class, 'rpa_rp');
	}
}
