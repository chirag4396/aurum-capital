<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RiskQuestion extends Model
{
    public $primaryKey = 'rq_id';

    protected $fillable = ['rq_qus'];

    public $timestamps = false;

    public function options(){
    	return $this->hasMany(\App\RiskOption::class, 'ro_rq');
    }
}
