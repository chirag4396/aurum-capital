<?php
Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/browser', 'HomeController@browser')->name('browser');
Route::get('/fb', 'HomeController@fb')->name('fb');
Route::get('/about', 'HomeController@about')->name('about');
Route::get('/blogs', 'HomeController@blogs')->name('blogs');
Route::get('/contact', 'HomeController@contact')->name('contact');
Route::get('/faqs', 'HomeController@faqs')->name('faqs');
Route::get('/services', 'HomeController@services')->name('services');
Route::get('/check-scrap', 'HomeController@checkScrap');
Route::get('/check-logout', 'HomeController@checkLogout');
Route::get('/check-date', 'HomeController@checkDate');
Route::get('/check-pdf-event', 'HomeController@checkPdfEvent');

Route::get('/past-performances', 'HomeController@pastPerformances')->name('past-performances');
Route::get('/services', 'HomeController@services')->name('services');
Route::get('/testimonials', 'HomeController@testimonials')->name('testimonials');
Route::get('/risk-profile', 'HomeController@riskProfile')->name('risk-profile');
Route::get('/terms', 'HomeController@terms')->name('terms');
Route::get('/privacy', 'HomeController@privacy')->name('privacy');
Route::get('login/facebook', 'HomeController@redirectToProvider');
Route::get('login/facebook/callback', 'HomeController@handleProviderCallback');
Route::get('ip', 'UserController@checkIpDetail');
Route::get('stock', 'UserController@checkStock');
Route::get('api-call', function(){
	return AlphaVantage\Api::stock()->daily('NSE:KEC');
});

Route::group(['middleware' => 'paid'], function() {
    Route::get('/current-recommendation', 'HomeController@currentRecommendation')->name('current-recommendation');
    Route::get('/investors-forum', 'HomeController@investorForum')->name('investor');
	Route::get('/reports/{id}/{name}', 'HomeController@stockReports')->name('stock-reports');
	Route::get('/view-report/{id}/{name}', 'StockReportController@viewReport')->name('view-report');
    Route::get('/download-report/{id}/{name}', 'StockReportController@downloadReport')->name('download-report');
});
Route::get('/admin/login', function() {
	return view('admin.login');
});

Route::resource('enquiry', 'EnquiryController');
Route::resource('investor-forum', 'InvestorForumController');
Route::get('pdf', 'UserController@pdf');
Route::get('watermark', 'UserController@watermark');

Route::get('client/{id}/kyc', 'KycController@upload')->name('kyc-upload');
Route::get('client/{id}/risk-profile/{edit?}', 'RiskProfileController@upload')->name('risk-upload');
Route::get('client/{id}/risk-profile-result', 'RiskProfileController@result')->name('risk-result');
Route::get('check', 'UserController@checkPDF');
Route::resource('forum-answer', 'ForumAnswerController');

Route::get('/pay','PaymentController@getPay')->name('get-pay');

Route::post('/paydemo','PaymentController@getPay')->name('pay');

Route::post('/placeorder','PaymentController@getPay')->name('placeorder');

Route::post('/paystatus','PaymentController@orderStatus')->name('orderStatus');

Route::resource('payment', 'PaymentController');

Route::get('/booked/order/{id}/{status?}', 'PaymentController@previous')->name('previous');

Route::group(['prefix' => 'admin', 'middleware' => 'admin', 'as' => 'admin.'], function(){	
	Route::get('/', function() {
		return view('admin.dashboard');
	})->name('home');
	
	Route::get('/check', function() {
		return view('admin.emails.normal');
	});

	Route::get('/download-kyc', 'UserController@downloadKyc')->name('download-kyc');
	Route::get('/download-stock', 'StockController@getStock')->name('download-stock');

	Route::resource('kyc', 'KycController');
	Route::resource('faq', 'FaqController');
	Route::resource('risk-profile', 'RiskProfileController');
	Route::resource('user', 'UserController');
	
	Route::get('clients/{type}/{tname}', 'UserController@getClients')->name('clients');
	
	Route::get('risk-profiles/{id}', 'RiskProfileController@allRiskProfile')->name('risk-profiles');
	Route::get('kycs/{id}/{type?}', 'KycController@allKycs')->name('kycs');
	Route::get('kycs-new', 'KycController@allKycsNew')->name('kycs-new');
	Route::get('all-user', 'UserController@allUser')->name('all-user');
	Route::get('all-stocks', 'StockController@allStocks')->name('all-stocks');	
	Route::get('all-logs', 'LoginLogController@allLogs')->name('all-logs');
	Route::get('all-logs-users', 'LoginLogController@allLogsUsers')->name('all-logs-users');
	Route::get('logs-users', 'LoginLogController@viewLogUser')->name('logs-users');	
	Route::get('all-forums', 'InvestorForumController@allForums')->name('all-forums');
	Route::get('logs/{id}', 'LoginLogController@logs')->name('logs');
	Route::post('payment-form', 'UserController@paymentForm')->name('payment-form');
	Route::post('block-form', 'LoginLogController@blockForm')->name('block-form');    
	Route::post('answer-form', 'InvestorForumController@answerForm')->name('answer-form');
	Route::post('approve-form', 'StockController@approveForm')->name('approve-form');	
	Route::post('all-answers', 'ForumAnswerController@allAnswers')->name('all-answers');

	Route::resource('investor-forum', 'InvestorForumController');
	Route::resource('current-recommendation', 'CurrentRecommendationController');
	Route::resource('stock', 'StockController');
	Route::resource('login-log', 'LoginLogController');
	Route::resource('stock-report', 'StockReportController');
	Route::resource('forum-answer', 'ForumAnswerController');
});
